package main

import (
	"context"
	"github.com/kelseyhightower/envconfig"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-edge-deployment/workflow-logs-storage/adapters"
	"gitlab.com/cyverse/cacao-edge-deployment/workflow-logs-storage/adapters/wfstan"
	"gitlab.com/cyverse/cacao-edge-deployment/workflow-logs-storage/domain"
	"gitlab.com/cyverse/cacao-edge-deployment/workflow-logs-storage/ports"
	"gitlab.com/cyverse/cacao-edge-deployment/workflow-logs-storage/types"
)

var conf types.Config

func init() {
	err := envconfig.Process("", &conf)
	if err != nil {
		log.WithError(err).Fatal("fail to parse config from environment variables")
	}
	level, err := log.ParseLevel(conf.LogLevel)
	if err != nil {
		log.WithError(err).Fatal("fail to parse log level from config")
	}
	log.SetLevel(level)

	log.SetReportCaller(true)
}

func main() {
	var logsStorage ports.LogsStorage
	var err error
	switch c := conf.GetStorageConfig(); c.(type) {
	case types.IRODSConfig:
		log.Info("use irods storage")
		logsStorage, err = adapters.NewIRODSLogsStorageAdapter(conf.IRODSConfig)
	case types.SwiftConfig:
		log.Info("use swift storage")
		logsStorage, err = adapters.NewSwiftAdapter(conf.SwiftConfig)
	case types.S3Config:
		log.Info("use s3 storage")
		logsStorage, err = adapters.NewS3StorageAdapter(conf.S3Config)
	case types.FileStorageConfig:
		log.Info("use filesystem storage")
		logsStorage, err = adapters.NewFileStorageAdapter(conf.FileStorageConfig)
	default:
		log.Panicf("BUG, bad config type, %+v", c)
	}
	if err != nil {
		log.WithError(err).Fatal("fail to init storage adapter")
		return
	}

	argoClient, err := adapters.NewArgoWorkflowLogsClient(conf.ArgoConfig)
	if err != nil {
		log.WithError(err).Fatal("fail to init argo client")
		return
	}
	wfEventSrc, err := wfstan.NewStanEventSrc(conf.StanConfig)
	if err != nil {
		log.WithError(err).Fatal("fail to init STAN event source")
		return
	}
	argoListSrc := adapters.NewArgoListMonitor(argoClient)
	defer wfEventSrc.Close()
	svc := domain.NewService([]ports.WorkflowMonitor{wfEventSrc, &argoListSrc}, argoClient, logsStorage)
	serviceCtx, cancelServiceCtx := context.WithCancel(context.Background())
	common.CancelWhenSignaled(cancelServiceCtx)
	err = svc.Start(serviceCtx, cancelServiceCtx)
	if err != nil {
		log.WithError(err).Panic("fail to start service")
	}
}
