package types

import (
	cloudevents "github.com/cloudevents/sdk-go/v2"
	"github.com/pkg/errors"
	"gitlab.com/cyverse/cacao-common/common"
	"time"
)

// Event is a common interface for Events
type Event interface {
	EventType() common.EventType
	ToCloudEvent() (*cloudevents.Event, error)
}

// WorkflowObserver is a callback function to notify the completion of a workflow.
type WorkflowObserver func(workflowName, status string)

// WorkflowLogs is the logs of a workflow.
// This is a struct because we may change its structure. Since workflow has multiple pods.
type WorkflowLogs struct {
	Logs []byte
	// this is populated when retrieving logs from storage, use this for cache control
	LastModified time.Time
}

// WorkflowLogsSavedEvent ...
const WorkflowLogsSavedEvent common.EventType = common.EventTypePrefix + "WorkflowLogsSaved"

// WorkflowLogsSaved ...
type WorkflowLogsSaved struct {
	Provider     common.ID `json:"provider,omitempty"`
	WorkflowName string    `json:"workflow_name"`
}

// LogsNotFound is an error that indicate the logs for a workflow is not found.
//
// Either the workflow is not found, or the logs for the workflow is not in storage (not ready or deleted or never there)
var LogsNotFound = errors.New("logs is not found for the workflow")
