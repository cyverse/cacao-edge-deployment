package types

import (
	"gitlab.com/cyverse/cacao-common/messaging2"
)

// Config contains all config for the workflow logs storage service
type Config struct {
	StanConfig
	IRODSConfig
	SwiftConfig
	ArgoConfig
	S3Config
	FileStorageConfig
	LogLevel string `envconfig:"LOG_LEVEL" default:"debug"`
}

// GetStorageConfig returns appropriate config for the storage solution.
// This will return IRODSConfig if iRODS should be used, return SwiftConfig if OpenStack Swift should be used.
func (c Config) GetStorageConfig() interface{} {
	if c.FileStorageConfig.BasePath != "" {
		return c.FileStorageConfig
	}
	if c.IRODSConfig.Username != "" && c.IRODSConfig.Password != "" && c.IRODSConfig.BasePath != "" {
		return c.IRODSConfig
	}
	if c.SwiftConfig.Region != "" && c.SwiftConfig.ContainerName != "" && c.SwiftConfig.AppCredID != "" && c.SwiftConfig.AppCredSecret != "" {
		return c.SwiftConfig
	}
	return c.S3Config
}

// StanConfig ...
type StanConfig = messaging2.NatsStanMsgConfig

// IRODSConfig is config for the iRODS adapter
type IRODSConfig struct {
	BasePath string `envconfig:"IRODS_BASE_PATH"`
	Username string `envconfig:"IRODS_USERNAME"`
	Password string `envconfig:"IRODS_PASSWORD"`
}

// SwiftConfig is config for OpenStack Swift adapter.
type SwiftConfig struct {
	// OpenStack region
	Region string `envconfig:"SWIFT_REGION"`
	// name of the container, container is equivalent to S3 bucket
	ContainerName string `envconfig:"SWIFT_CONTAINER_NAME"`
	//AppCredID     string `envconfig:"SWIFT_APP_CRED_ID"`
	//AppCredSecret string `envconfig:"SWIFT_APP_CRED_SECRET"`
	AppCredID     string `envconfig:"OS_APPLICATION_CREDENTIAL_ID"`
	AppCredSecret string `envconfig:"OS_APPLICATION_CREDENTIAL_SECRET"`
}

// ArgoConfig is config for argo workflow
type ArgoConfig struct {
	ServerURL string `envconfig:"ARGO_URL"`
	HTTPS     bool   `envconfig:"ARGO_HTTPS"`
	SSLVerify bool   `envconfig:"ARGO_SSL_VERIFY"`
	Namespace string `envconfig:"ARGO_NAMESPACE"`
	Token     string `envconfig:"ARGO_TOKEN"`
}

// S3Config is config for s3 storage adapter.
type S3Config struct {
	// if true, use s3-compatible endpoint for JetStream 2 OpenStack Swift. This takes priority over S3Endpoint
	JetStream2Swift bool `envconfig:"S3_JS2_SWIFT"`
	// if using AWS S3, leaves this empty
	S3Endpoint string `envconfig:"S3_ENDPOINT"`
	S3Region   string `envconfig:"S3_REGION"`

	BucketName  string `envconfig:"S3_BUCKET"`
	AccessKeyID string `envconfig:"S3_ACCESS_KEY_ID"`
	SecretKeyID string `envconfig:"S3_SECRET_KEY_ID"`
}

// FileStorageConfig is config for file storage adapter.
type FileStorageConfig struct {
	BasePath string `envconfig:"FILE_BASE_PATH"`
}

// iRODS configuration specific to CyVerse Data Store.
// https://learning.cyverse.org/ds/icommands/#icommands-first-time-configuration
const (
	CyVerseIRODSHost string = "data.cyverse.org"
	CyVerseIRODSPort int    = 1247
	CyVerseIRODSZone string = "iplant"
)
