package jwttoken

import (
	"errors"
	"github.com/golang-jwt/jwt/v5"
	"github.com/rs/xid"
	"time"
)

// JWTParse parse a JWT token from string and validate it with the given key.
// This will also validate the claim in the JWT.
func JWTParse(key []byte, token string) error {
	if len(key) <= 256/8 {
		return errors.New("invalid key")
	}
	parsed, err := jwt.Parse(token, func(token *jwt.Token) (interface{}, error) {
		return key, nil
	}, jwt.WithValidMethods([]string{jwt.SigningMethodHS256.Alg()}))
	if err != nil {
		return err
	}
	exp, err := parsed.Claims.GetExpirationTime()
	if err != nil {
		return err
	}
	if exp == nil {
		return errors.New("token is invalid, missing exp")
	}
	if time.Since(exp.Time) >= 0 {
		return errors.New("token expired")
	}
	nbf, err := parsed.Claims.GetNotBefore()
	if err != nil {
		return err
	}
	if nbf == nil {
		return errors.New("token is invalid, missing nbf")
	}
	if time.Since(nbf.Time) <= 0 {
		return errors.New("token is not yet valid")
	}
	return nil
}

// GenerateNewJWT generates a new JWT using HS256 algorithm. This JWT will be
// valid from now, with a lifetime of specified duration.
func GenerateNewJWT(key []byte, duration time.Duration) (string, error) {
	if len(key) <= 256/8 {
		return "", errors.New("invalid key")
	}
	now := time.Now()

	t := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.RegisteredClaims{
		Issuer:    "",
		Subject:   "",
		Audience:  nil,
		ExpiresAt: jwt.NewNumericDate(now.Add(duration)),
		NotBefore: jwt.NewNumericDate(now),
		IssuedAt:  jwt.NewNumericDate(now),
		ID:        xid.New().String(),
	})
	return t.SignedString(key)
}
