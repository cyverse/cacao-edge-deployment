// This is a helper program that is meant to be in the same container image as httplogs, so that we can generate new JWT.
package main

import (
	"encoding/base64"
	"fmt"
	"github.com/kelseyhightower/envconfig"
	"gitlab.com/cyverse/cacao-edge-deployment/workflow-logs-storage/httplogs/jwttoken"
	"os"
	"time"
)

// Config allow us to read config from environment variables
type Config struct {
	// this should use the same env var as the one used by httplogs, this way we don't have to worry about passing key when using it in container.
	JWTKeyBase64 string `envconfig:"JWT_KEY"`
	jwtKey       []byte `envconfig:"-"`
}

func main() {
	var conf Config
	err := envconfig.Process("", &conf)
	if err != nil {
		_, _ = fmt.Fprint(os.Stderr, err.Error())
		return
	}
	conf.jwtKey, err = base64.StdEncoding.DecodeString(conf.JWTKeyBase64)
	if err != nil {
		_, _ = fmt.Fprint(os.Stderr, err.Error())
		return
	}
	jwt, err := jwttoken.GenerateNewJWT(conf.jwtKey, time.Minute*10)
	if err != nil {
		_, _ = fmt.Fprint(os.Stderr, err.Error())
		return
	}
	fmt.Println(jwt)
}
