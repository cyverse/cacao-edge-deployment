package main

import (
	"context"
	"crypto/md5"
	"encoding/base32"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"net"
	"net/http"
	"strings"
	"sync"
	"time"

	"github.com/gorilla/mux"
	"github.com/kelseyhightower/envconfig"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-edge-deployment/workflow-logs-storage/adapters"
	"gitlab.com/cyverse/cacao-edge-deployment/workflow-logs-storage/httplogs/jwttoken"
	"gitlab.com/cyverse/cacao-edge-deployment/workflow-logs-storage/ports"
	"gitlab.com/cyverse/cacao-edge-deployment/workflow-logs-storage/types"
)

// Config is configuration for the httplogs service
type Config struct {
	Port              uint16 `envconfig:"HTTP_PORT" default:"8080"`
	RequestTimeoutSec uint   `envconfig:"REQUEST_TIMEOUT" default:"10"`
	JWTKeyBase64      string `envconfig:"JWT_KEY"`
	jwtKey            []byte `envconfig:"-"`
	SwiftConfig       types.SwiftConfig
	IRODSConfig       types.IRODSConfig
	S3Config          types.S3Config
	FileStorageConfig types.FileStorageConfig
}

// GetStorageConfig returns appropriate config for the storage solution.
// This will return IRODSConfig if iRODS should be used, return SwiftConfig if OpenStack Swift should be used.
func (c Config) GetStorageConfig() interface{} {
	if c.FileStorageConfig.BasePath != "" {
		return c.FileStorageConfig
	}
	if c.IRODSConfig.Username != "" && c.IRODSConfig.Password != "" && c.IRODSConfig.BasePath != "" {
		return c.IRODSConfig
	}
	if c.SwiftConfig.Region != "" && c.SwiftConfig.ContainerName != "" && c.SwiftConfig.AppCredID != "" && c.SwiftConfig.AppCredSecret != "" {
		return c.SwiftConfig
	}
	return c.S3Config
}

func main() {
	var conf Config
	err := envconfig.Process("", &conf)
	if err != nil {
		log.WithError(err).Panic("fail to load config from env var")
		return
	}
	var logsStorage ports.LogsReader
	switch c := conf.GetStorageConfig(); c.(type) {
	case types.IRODSConfig:
		log.Info("use irods storage")
		logsStorage, err = adapters.NewIRODSLogsStorageAdapter(conf.IRODSConfig)
	case types.SwiftConfig:
		log.Info("use swift storage")
		logsStorage, err = adapters.NewSwiftAdapter(conf.SwiftConfig)
	case types.S3Config:
		log.Info("use s3 storage")
		logsStorage, err = adapters.NewS3StorageAdapter(conf.S3Config)
	case types.FileStorageConfig:
		log.Info("use filesystem storage")
		logsStorage, err = adapters.NewFileStorageAdapter(conf.FileStorageConfig)
	default:
		log.Panicf("BUG, bad config type, %+v", c)
	}
	if err != nil {
		log.WithError(err).Fatal("fail to init storage adapter")
		return
	}

	serviceCtx, cancelServiceCtx := context.WithCancel(context.Background())
	common.CancelWhenSignaled(cancelServiceCtx)

	conf.jwtKey, err = base64.StdEncoding.DecodeString(conf.JWTKeyBase64)
	if err != nil {
		log.WithError(err).Panic("fail to decode key as base64")
		return
	}

	router := mux.NewRouter()
	router.HandleFunc("/v1/logs/{workflow_name}", handler(conf, conf.jwtKey, logsStorage)).Methods("GET")
	router.HandleFunc("/v1/ping", pingHandler(logsStorage)).Methods("GET")

	var server = http.Server{
		Addr:                         fmt.Sprintf("0.0.0.0:%d", conf.Port),
		Handler:                      router,
		DisableGeneralOptionsHandler: true,
		ReadTimeout:                  time.Second * 2,
		ReadHeaderTimeout:            time.Second,
		WriteTimeout:                 time.Second * 8,
		IdleTimeout:                  time.Second * 30,
		MaxHeaderBytes:               8 << 10,
		BaseContext: func(net.Listener) context.Context {
			return serviceCtx
		},
	}
	var serverWg sync.WaitGroup
	serverWg.Add(1)
	go shutdownServerWhenCanceled(serviceCtx, &serverWg, &server)
	log.Infof("starting http server, %s", server.Addr)
	err = server.ListenAndServe()
	if err != nil {
		if errors.Is(err, http.ErrServerClosed) {
			log.Info("server shutdown")
		} else {
			log.WithError(err).Panic("fail to start http server")
		}
	}
	serverWg.Wait()
}

func shutdownServerWhenCanceled(ctx context.Context, wg *sync.WaitGroup, server *http.Server) {
	defer wg.Done()
	<-ctx.Done()
	timeout, cancel := context.WithTimeout(context.Background(), time.Second*10)
	err := server.Shutdown(timeout)
	cancel()
	if err != nil {
		log.WithError(err).Error("fail to shutdown http server")
	}
}

func jwtAuth(req *http.Request, key []byte) error {
	authHeader := req.Header.Get("Authorization")
	if len(authHeader) == 0 {
		return errors.New("auth header missing")
	}
	// handle the case where header could be "Bearer XXXX" instead of just "XXXX"
	splits := strings.SplitN(authHeader, " ", 2)
	if len(splits) == 1 {
		return jwttoken.JWTParse(key, authHeader)
	}
	return jwttoken.JWTParse(key, splits[1])
}

func jsonError(w http.ResponseWriter, statusCode int, err error) {
	if err == nil {
		statusCode = http.StatusInternalServerError
		err = errors.New("unknown error, error is nil")
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	_ = json.NewEncoder(w).Encode(map[string]string{
		"error": err.Error(),
	})
}

func handler(conf Config, jwtKey []byte, logsStorage ports.LogsReader) http.HandlerFunc {
	return func(writer http.ResponseWriter, request *http.Request) {
		logger := log.WithFields(log.Fields{})
		logger.Info("request to fetch logs")

		var err error
		ctx, cancel := context.WithTimeout(request.Context(), time.Duration(conf.RequestTimeoutSec)*time.Second)
		defer cancel()
		err = jwtAuth(request, jwtKey)
		if err != nil {
			logger.WithError(err).Error("auth failed")
			jsonError(writer, 400, err)
			return
		}

		workflowName := mux.Vars(request)["workflow_name"]
		if strings.Contains(workflowName, "/") {
			logger.Error("bad URL path")
			jsonError(writer, 400, errors.New("bad URL path"))
			return
		}
		logger = logger.WithField("workflow_name", workflowName)

		var wfLogs types.WorkflowLogs
		wfLogs, err = logsStorage.Read(ctx, workflowName)
		if err != nil {
			logger.WithError(err).Error("failed to read logs from storage")
			if errors.Is(err, types.LogsNotFound) {
				jsonError(writer, 404, err)
				return
			}
			jsonError(writer, 400, err)
			return
		}
		writer.Header().Set("Content-Type", "text/plain")
		setCacheHeaderIfApplicable(writer, wfLogs)
		n, err := writer.Write(wfLogs.Logs)
		if err != nil {
			logger.WithError(err).Error("fail to write logs to response")
			return
		}
		logger.WithField("len", n).Info("written logs to response body")
	}
}

func setCacheHeaderIfApplicable(writer http.ResponseWriter, wfLogs types.WorkflowLogs) {
	if wfLogs.LastModified.IsZero() {
		return
	}
	const oneDayMaxAge = "max-age=86400"
	writer.Header().Set("Cache-Control", "private")
	writer.Header().Add("Cache-Control", oneDayMaxAge)
	writer.Header().Set("Last-Modified", wfLogs.LastModified.Format(time.RFC1123))
	hash := md5.New()
	hash.Write(wfLogs.Logs)
	writer.Header().Set("ETag", base32.StdEncoding.EncodeToString(hash.Sum(nil)))
}

func pingHandler(logsStorage ports.LogsReader) http.HandlerFunc {
	var lock sync.Mutex
	var lastPing time.Time
	var lastError error
	return func(writer http.ResponseWriter, request *http.Request) {
		lock.Lock()
		defer lock.Unlock()
		log.Debug("pinged")

		if time.Since(lastPing) > 10*time.Second {
			lastPing = time.Now()
			err := logsStorage.Ping(request.Context())
			if err != nil {
				log.WithError(err).Error("ping failed")
				lastError = err
				writer.WriteHeader(500)
				_, _ = writer.Write([]byte(`{"status": "error", error": "storage errored"}`))
				return
			}
		}
		if lastError != nil {
			_, _ = writer.Write([]byte(`{"status": "error", error": "storage errored"}`))
		} else {
			_, _ = writer.Write([]byte(`{"status": "healthy"}`))
		}
	}
}
