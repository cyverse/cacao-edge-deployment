# workflow-logs-storage

This service fetches workflow logs upon its completion.

This service monitor the events emitted by the exit handler of workflow to determine when a workflow is finished (succeed or failed).
- wait for workflow result event
- fetch workflow logs for the workflow specified in the event
- store workflow logs in persistent storage

# Logs fetching
Use Argo workflow api to fetch logs of each workflow pods.

# Persistent logs storage
For now we use iRODS (irods-go-client) as the persistent backend, we can always implement adapters for other persistent backends.

## iRODS
Folder structure
```
<base_path>/
|__ logs/
		|__ <workflow_name>.log
		|__ <workflow_name>.log
```

## OpenStack Swift
```
<container>/
        |__ logs/
                |__ <workflow_name>.log
                |__ <workflow_name>.log
```

# Configuration

| environment variable  | description                                                   | default          |
|-----------------------|---------------------------------------------------------------|------------------|
| LOG_LEVEL             | log level (trace/debug/info)                                  | debug            |
| NATS_URL              | -                                                             | nats://nats:4222 |
| NATS_CLIENT_ID        | -                                                             | -                |
| NATS_CLUSTER_ID       | -                                                             | cacao-cluster    |
| ARGO_URL              | URL for the argo server                                       | -                |
| ARGO_HTTPS            | use HTTPS if `true`                                           | -                |
| ARGO_SSL_VERIFY       | skip verify SSL if `false`                                    | -                |
| ARGO_TOKEN            | token for authenticate with argo server                       | -                |
| IRODS_BASE_PATH       | base path for storing logs in iRODS                           | -                |
| IRODS_USERNAME        | iRODS username                                                | -                |
| IRODS_PASSWORD        | iRODS password                                                | -                |
| SWIFT_REGION          | OpenStack Swift region                                        |
| SWIFT_CONTAINER_NAME  | OpenStack Swift container name (equivalent to S3 bucket name) |
| SWIFT_APP_CRED_ID     | OpenStack Swift application credential ID                     |
| SWIFT_APP_CRED_SECRET | OpenStack Swift application credential secret                 |
