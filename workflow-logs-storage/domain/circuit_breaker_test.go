package domain

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"sync"
	"testing"
	"time"
)

func TestCircuitBreaker(t *testing.T) {
	t.Run("broken", func(t *testing.T) {
		cb := newCircuitBreaker(2, time.Second, time.Millisecond*200)
		var err = fmt.Errorf("bad")
		cb.AddError(err)
		cb.AddError(err)
		assert.False(t, cb.Broken())
		cb.AddError(err)
		assert.True(t, cb.Broken())
	})
	t.Run("sleep-then-more-error", func(t *testing.T) {
		cb := newCircuitBreaker(2, time.Second, time.Millisecond*200)
		var err = fmt.Errorf("bad")
		cb.AddError(err)
		cb.AddError(err)
		assert.False(t, cb.Broken())

		time.Sleep(time.Second)
		cb.AddError(err)
		assert.False(t, cb.Broken())
	})
	t.Run("sleep-to-clear-error", func(t *testing.T) {
		cb := newCircuitBreaker(2, time.Second, time.Millisecond*200)
		var err = fmt.Errorf("bad")
		cb.AddError(err)
		cb.AddError(err)
		cb.AddError(err)
		cb.AddError(err)
		assert.True(t, cb.Broken())
		time.Sleep(time.Second)
		assert.False(t, cb.Broken())
	})
	t.Run("concurrent", func(t *testing.T) {
		const maxErrorCount = 2
		cb := newCircuitBreaker(maxErrorCount, time.Second, time.Millisecond*200)
		var err = fmt.Errorf("bad")
		const N = 1000
		var wg sync.WaitGroup
		wg.Add(1)
		go func() {
			defer wg.Done()
			for i := 0; i < N; i++ {
				cb.AddError(err)
			}
		}()
		wg.Wait()
		assert.True(t, cb.Broken())
		assert.Less(t, len(cb.errors), N)
		assert.Less(t, len(cb.errors), maxErrorCount*2)
	})
}
