package domain

import (
	"context"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-edge-deployment/workflow-logs-storage/ports"
)

// Domain is entrypoint object for the service
type Domain struct {
	cancelFunc            context.CancelFunc
	workflowMonitorList   []ports.WorkflowMonitor
	logsRepo              ports.WorkflowLogsRepository
	logsStorage           ports.LogsStorage
	storageCircuitBreaker *circuitBreaker
}

// NewService creates a new Domain
func NewService(
	workflowMonitorList []ports.WorkflowMonitor,
	logsRepo ports.WorkflowLogsRepository,
	logsStorage ports.LogsStorage,
) *Domain {
	return &Domain{
		workflowMonitorList:   workflowMonitorList,
		logsRepo:              logsRepo,
		logsStorage:           logsStorage,
		storageCircuitBreaker: newCircuitBreakerWithDefault(),
	}
}

// Start the service
func (svc *Domain) Start(ctx context.Context, cancelFunc context.CancelFunc) error {
	svc.cancelFunc = cancelFunc

	if len(svc.workflowMonitorList) == 0 {
		return fmt.Errorf("no workflow monitor is passed to Domain")
	}

	defer svc.close()
	for i := range svc.workflowMonitorList {
		err := svc.workflowMonitorList[i].RegisterObserver(svc.watchWorkflow)
		if err != nil {
			log.WithError(err).Fatal("fail to register workflow")
			return err
		}
		err = svc.workflowMonitorList[i].Start(ctx)
		if err != nil {
			log.WithError(err).Fatal("fail to start monitoring workflows")
			return err
		}
	}
	<-ctx.Done()
	log.Warning("domain exited")
	return nil
}

func (svc *Domain) watchWorkflow(workflowName, status string) {
	logger := log.WithFields(log.Fields{
		"workflow": workflowName,
	})
	logs, err := svc.logsRepo.FetchLogs(workflowName)
	if err != nil {
		logger.WithError(err).Error("fail to fetch logs")
		return
	}
	if svc.storageCircuitBreaker.Broken() {
		logger.Error("logs storage has errored too many times, circuit breaker is triggered")
		svc.close()
		svc.cancelFunc()
		return
	}
	const retryCount = 3
	for i := 0; i < retryCount; i++ {
		err = svc.logsStorage.Store(context.Background(), workflowName, logs)
		if err != nil {
			svc.storageCircuitBreaker.AddError(err)
			logger.WithError(err).Errorf("fail to store logs, retry %d", i)
		} else {
			break
		}
	}
}

func (svc *Domain) close() {
	for i := range svc.workflowMonitorList {
		err := svc.workflowMonitorList[i].Close()
		if err != nil {
			log.WithError(err).Error("fail to close workflow monitor")
		}
	}
}
