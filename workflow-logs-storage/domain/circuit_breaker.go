package domain

import (
	"sync"
	"time"
)

type circuitBreaker struct {
	lock          sync.Mutex
	maxErrorCount uint16
	duration      time.Duration
	errors        []struct {
		err error
		t   time.Time
	}
	minErrorIntervalAfterBroken time.Duration
}

func newCircuitBreaker(maxErrorCount uint16, duration time.Duration, minErrorIntervalAfterBroken time.Duration) *circuitBreaker {
	return &circuitBreaker{
		lock:                        sync.Mutex{},
		maxErrorCount:               maxErrorCount,
		duration:                    duration,
		errors:                      nil,
		minErrorIntervalAfterBroken: minErrorIntervalAfterBroken,
	}
}

func newCircuitBreakerWithDefault() *circuitBreaker {
	return &circuitBreaker{
		lock:                        sync.Mutex{},
		maxErrorCount:               10,
		duration:                    time.Minute * 5,
		errors:                      nil,
		minErrorIntervalAfterBroken: time.Millisecond * 200,
	}
}

// AddError adds a new error. This is safe to be used concurrently.
func (cb *circuitBreaker) AddError(err error) {
	cb.lock.Lock()
	defer cb.lock.Unlock()

	cb.removeOldErrors()
	if len(cb.errors) > int(cb.maxErrorCount) {
		// we do not want to register all errors after it is broken, after it is broken,
		// we only add error that is some interval a part from last error.
		if time.Now().UTC().Sub(cb.errors[len(cb.errors)-1].t) < cb.minErrorIntervalAfterBroken {
			return
		}
	}

	cb.errors = append(cb.errors, struct {
		err error
		t   time.Time
	}{err: err, t: time.Now().UTC()})
}

// Broken return true if circuitBreaker is broken. This is safe to be used concurrently.
func (cb *circuitBreaker) Broken() bool {
	cb.lock.Lock()
	defer cb.lock.Unlock()

	cb.removeOldErrors()

	return len(cb.errors) > int(cb.maxErrorCount)
}

// this needs to be called when locked
func (cb *circuitBreaker) removeOldErrors() {
	now := time.Now()
	var index = -1
	for i := range cb.errors {
		if now.Sub(cb.errors[i].t) > cb.duration {
			index = i
		} else {
			break
		}
	}
	if index == len(cb.errors)-1 {
		cb.errors = nil
	} else if index >= 0 {
		cb.errors = cb.errors[index+1:]
	}
}
