// This is a CLI tool that will scan all completed workflows that currently existed and store their logs.
// This is useful for scanning workflows logs when workflow-logs-storage is not running.
package main

import (
	"context"
	"github.com/kelseyhightower/envconfig"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-edge-deployment/workflow-logs-storage/adapters"
	"gitlab.com/cyverse/cacao-edge-deployment/workflow-logs-storage/ports"
	"gitlab.com/cyverse/cacao-edge-deployment/workflow-logs-storage/types"
)

var conf types.Config

func init() {
	err := envconfig.Process("", &conf)
	if err != nil {
		log.WithError(err).Fatal("fail to parse config from environment variables")
	}
	log.SetLevel(log.InfoLevel)
}

func main() {
	var logsStorage ports.LogsStorage
	var err error
	switch c := conf.GetStorageConfig(); c.(type) {
	case types.IRODSConfig:
		log.Info("use irods storage")
		logsStorage, err = adapters.NewIRODSLogsStorageAdapter(conf.IRODSConfig)
	case types.SwiftConfig:
		log.Info("use swift storage")
		logsStorage, err = adapters.NewSwiftAdapter(conf.SwiftConfig)
	case types.S3Config:
		log.Info("use s3 storage")
		logsStorage, err = adapters.NewS3StorageAdapter(conf.S3Config)
	case types.FileStorageConfig:
		log.Info("use filesystem storage")
		logsStorage, err = adapters.NewFileStorageAdapter(conf.FileStorageConfig)
	default:
		log.Panicf("BUG, bad config type, %+v", c)
	}
	if err != nil {
		log.WithError(err).Fatal("fail to init storage adapter")
		return
	}
	scanAllWorkflows(logsStorage)
}

func scanAllWorkflows(logsStorage ports.LogsStorage) {
	argoClient, err := adapters.NewArgoWorkflowLogsClient(conf.ArgoConfig)
	if err != nil {
		log.WithError(err).Fatal("fail to init argo client")
		return
	}
	workflowNames, err := argoClient.ListWorkflows()
	if err != nil {
		log.WithError(err).Panicf("fail to list workflow")
		return
	}
	for _, wfName := range workflowNames {
		var logs types.WorkflowLogs
		logs, err = argoClient.FetchLogs(wfName)
		if err != nil {
			log.WithError(err).WithField("wfName", wfName).Errorf("fail to fetch logs")
			return
		}
		err = logsStorage.Store(context.TODO(), wfName, logs)
		if err != nil {
			log.WithError(err).WithField("wfName", wfName).Errorf("fail to store logs")
			return
		}
		log.WithField("wfName", wfName).Info("stored logs for workflow")
	}
	log.Info("finished")
}
