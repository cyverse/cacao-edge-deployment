package adapters

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"github.com/gophercloud/gophercloud"
	"github.com/gophercloud/gophercloud/openstack"
	"github.com/gophercloud/gophercloud/openstack/objectstorage/v1/objects"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-edge-deployment/workflow-logs-storage/ports"
	"gitlab.com/cyverse/cacao-edge-deployment/workflow-logs-storage/types"
	"path"
	"strconv"
	"time"
)

// SwiftAdapter is storage adapter for OpenStack Swift (Object Storage for OpenStack)
type SwiftAdapter struct {
	appCredID     string
	appCredSecret string
	region        string
	containerName string
	swift         *gophercloud.ServiceClient
}

var _ ports.LogsStorage = (*SwiftAdapter)(nil)
var _ ports.LogsReader = (*SwiftAdapter)(nil)

// NewSwiftAdapter creates a new SwiftAdapter and initialize it from config
func NewSwiftAdapter(config types.SwiftConfig) (*SwiftAdapter, error) {
	adapter := &SwiftAdapter{
		appCredID:     config.AppCredID,
		appCredSecret: config.AppCredSecret,
		region:        config.Region,
		containerName: config.ContainerName,
		swift:         nil,
	}
	err := adapter.init()
	if err != nil {
		return nil, err
	}
	return adapter, nil
}

func (s *SwiftAdapter) init() error {
	opts := gophercloud.AuthOptions{
		IdentityEndpoint:            "https://js2.jetstream-cloud.org:5000/v3/",
		ApplicationCredentialID:     s.appCredID,
		ApplicationCredentialSecret: s.appCredSecret,
	}

	provider, err := openstack.AuthenticatedClient(opts)
	if err != nil {
		return err
	}
	swift, err := openstack.NewObjectStorageV1(provider, gophercloud.EndpointOpts{
		Region: s.region,
	})
	if err != nil {
		return err
	}
	// use hard-coded timeout instead of swift.Context, since we cannot safely set it concurrently.
	swift.HTTPClient.Timeout = time.Second * 10
	s.swift = swift
	log.Info("initialized swift adapter")
	return nil
}

// TTL for logs stored as Swift Object, logs file(object) will be deleted after TTL expired.
const swiftLogsTTL = time.Hour * 24 * 180

// Store logs for a workflow as object in OpenStack Swift.
func (s *SwiftAdapter) Store(ctx context.Context, workflowName string, logs types.WorkflowLogs) error {
	logger := log.WithFields(log.Fields{
		"workflowName": workflowName,
		"len":          len(logs.Logs),
	})
	now := time.Now().UTC()
	result := objects.Create(s.swift, s.containerName, s.objectName(workflowName), objects.CreateOpts{
		Content: bytes.NewReader(logs.Logs),
		Metadata: map[string]string{
			"workflow_name":   workflowName,
			"created_on":      createdDateStr(now),
			"created_on_unix": strconv.FormatInt(now.Unix(), 10),
		},
		ContentType: "text",
		DeleteAfter: int(now.Add(swiftLogsTTL).Unix()),
	})
	if result.Err != nil {
		logger.WithError(result.Err).Error("fail to store logs")
		return result.Err
	}
	logger.Info("logs stored successfully")
	return nil
}

// Read logs for a workflow from swift
func (s *SwiftAdapter) Read(ctx context.Context, workflowName string) (types.WorkflowLogs, error) {
	logger := log.WithFields(log.Fields{
		"workflowName": workflowName,
	})
	result := objects.Download(s.swift, s.containerName, s.objectName(workflowName), objects.DownloadOpts{
		IfMatch:           "",
		IfModifiedSince:   time.Time{},
		IfNoneMatch:       "",
		IfUnmodifiedSince: time.Time{},
		Newest:            false,
		Range:             "",
		Expires:           "",
		MultipartManifest: "",
		Signature:         "",
	})
	if result.Err != nil {
		if _, ok := result.Err.(gophercloud.Err404er); ok {
			return types.WorkflowLogs{}, errors.Join(types.LogsNotFound, result.Err)
		}
		logger.WithError(result.Err).Error("fail to download logs")
		return types.WorkflowLogs{}, result.Err
	}
	content, err := result.ExtractContent()
	if err != nil {
		logger.WithError(err).Error("fail to extract content from download result")
		return types.WorkflowLogs{}, err
	}
	downloadHeader, err := result.Extract()
	if err != nil {
		return types.WorkflowLogs{}, err
	}
	if !downloadHeader.LastModified.IsZero() {
		return types.WorkflowLogs{
			Logs:         content,
			LastModified: downloadHeader.LastModified,
		}, nil
	}
	return types.WorkflowLogs{
		Logs: content,
	}, nil
}

func (s *SwiftAdapter) objectName(workflowName string) string {
	return path.Join("logs", workflowName+".log")
}

// Ping perform some action to check if we still have connection to OpenStack Swift
func (s *SwiftAdapter) Ping(ctx context.Context) error {
	s.swift.Context = ctx
	list := objects.List(s.swift, s.containerName, objects.ListOpts{
		Limit: 10,
	})
	if list.Err != nil {
		return list.Err
	}
	return nil
}

func createdDateStr(now time.Time) string {
	now = now.UTC()
	return fmt.Sprintf("%d%02d%02d", now.Year(), now.Month(), now.Day())
}

func parseCreatedDateStr(date string) (time.Time, error) {
	if len(date) != 8 {
		return time.Time{}, fmt.Errorf("bad date format, expect YYYYMMDD")
	}
	yearStr := date[:len(date)-4]
	monthStr := date[len(date)-4 : len(date)-2]
	dayStr := date[len(date)-2:]
	year, err := strconv.ParseInt(yearStr, 10, 32)
	if err != nil {
		return time.Time{}, err
	}
	month, err := strconv.ParseInt(monthStr, 10, 8)
	if err != nil {
		return time.Time{}, err
	}
	day, err := strconv.ParseInt(dayStr, 10, 8)
	if err != nil {
		return time.Time{}, err
	}
	return time.Date(int(year), time.Month(month), int(day), 0, 0, 0, 0, time.UTC), nil
}
