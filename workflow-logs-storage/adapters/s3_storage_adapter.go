package adapters

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"path"
	"strconv"
	"strings"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-edge-deployment/workflow-logs-storage/ports"
	"gitlab.com/cyverse/cacao-edge-deployment/workflow-logs-storage/types"
)

// S3StorageAdapter is a storage adapter for AWS S3 or S3-compatible service.
type S3StorageAdapter struct {
	s3Region string
	// if js2Swift is true, then this is the container name for OpenStack Swift
	s3BucketName string
	sess         *session.Session
	uploader     *s3manager.Uploader
	downloader   *s3manager.Downloader
}

var _ ports.LogsStorage = (*S3StorageAdapter)(nil)
var _ ports.LogsReader = (*S3StorageAdapter)(nil)

const jetstream2SwiftS3Endpoint = "https://js2.jetstream-cloud.org:8001/"

// NewS3StorageAdapter ...
func NewS3StorageAdapter(
	config types.S3Config,
) (S3StorageAdapter, error) {
	var sess *session.Session
	var err error
	if config.JetStream2Swift {
		sess, err = newS3Session(config.S3Region, jetstream2SwiftS3Endpoint, config.AccessKeyID, config.SecretKeyID)
	} else {
		sess, err = newS3Session(config.S3Region, config.S3Endpoint, config.AccessKeyID, config.SecretKeyID)
	}
	if err != nil {
		return S3StorageAdapter{}, nil
	}
	err = createS3PingFile(sess, config.BucketName)
	if err != nil {
		return S3StorageAdapter{}, err
	}

	return S3StorageAdapter{
		s3Region:     config.S3Region,
		s3BucketName: config.BucketName,
		sess:         sess,
		uploader:     s3manager.NewUploader(sess),
		downloader:   s3manager.NewDownloader(sess),
	}, nil
}

func createS3PingFile(sess *session.Session, bucketName string) error {
	ctx, cancelFunc := context.WithTimeout(context.Background(), time.Second*3)
	defer cancelFunc()
	client := s3.New(sess)
	_, err := client.GetObjectWithContext(ctx, &s3.GetObjectInput{
		Bucket: aws.String(bucketName),
		Key:    aws.String(s3PingObjectKey),
	})
	if err != nil && !strings.Contains(err.Error(), s3.ErrCodeNoSuchKey) {
		return err
	}
	uploader := s3manager.NewUploader(sess)
	_, err = uploader.UploadWithContext(ctx, &s3manager.UploadInput{
		Body:        strings.NewReader("ping"),
		Bucket:      aws.String(bucketName),
		ContentType: aws.String("text/plain"),
		Key:         aws.String(s3PingObjectKey),
	})
	return err
}

const s3LogsTTL = time.Hour * 24 * 180

// Store ...
func (s S3StorageAdapter) Store(ctx context.Context, workflowName string, logs types.WorkflowLogs) error {
	now := time.Now().UTC()
	output, err := s.uploader.UploadWithContext(ctx, &s3manager.UploadInput{
		ACL:                 nil,
		Body:                bytes.NewReader(logs.Logs),
		Bucket:              aws.String(s.s3BucketName),
		CacheControl:        nil,
		ContentDisposition:  nil,
		ContentEncoding:     nil,
		ContentLanguage:     nil,
		ContentMD5:          nil,
		ContentType:         aws.String("text/plain"),
		ExpectedBucketOwner: nil,
		Expires:             aws.Time(now.Add(s3LogsTTL)),
		Key:                 aws.String(s.s3Path(workflowName)),
		Metadata: map[string]*string{
			"workflow_name":   aws.String(workflowName),
			"created_on":      aws.String(createdDateStr(now)),
			"created_on_unix": aws.String(strconv.FormatInt(now.Unix(), 10)),
		},
	})
	if err != nil {
		return err
	}
	log.WithFields(log.Fields{
		"uploadID": output.UploadID,
		"location": output.Location,
	}).Info("uploaded to s3")
	return nil
}

// Read ...
func (s S3StorageAdapter) Read(ctx context.Context, workflowName string) (types.WorkflowLogs, error) {
	client := s3.New(s.sess)
	obj, err := client.GetObjectWithContext(ctx, &s3.GetObjectInput{
		Bucket: aws.String(s.s3BucketName),
		Key:    aws.String(s.s3Path(workflowName)),
	})
	if err != nil {
		if awsErr, ok := err.(awserr.Error); ok {
			switch awsErr.Code() {
			case s3.ErrCodeNoSuchKey:
				fallthrough
			case s3.ErrCodeNoSuchBucket:
				fallthrough
			case s3.ErrCodeInvalidObjectState:
				return types.WorkflowLogs{}, errors.Join(types.LogsNotFound, err)
			default:
			}
		}
		return types.WorkflowLogs{}, err
	}

	var buf aws.WriteAtBuffer
	n, err := s.downloader.DownloadWithContext(ctx, &buf, &s3.GetObjectInput{
		Bucket: aws.String(s.s3BucketName),
		Key:    aws.String(s.s3Path(workflowName)),
	})
	if err != nil {
		return types.WorkflowLogs{}, fmt.Errorf("failed to download file, %v", err)
	}
	log.Infof("file downloaded, %d bytes", n)
	if obj.LastModified != nil {
		return types.WorkflowLogs{Logs: buf.Bytes(), LastModified: *obj.LastModified}, nil
	}
	return types.WorkflowLogs{Logs: buf.Bytes()}, nil
}

const s3PingObjectKey = "ping"

// Ping ...
func (s S3StorageAdapter) Ping(ctx context.Context) error {
	client := s3.New(s.sess)
	object, err := client.GetObjectWithContext(ctx, &s3.GetObjectInput{
		Bucket: aws.String(s.s3BucketName),
		Key:    aws.String(s3PingObjectKey),
	})
	if err != nil {
		return err
	}
	log.Tracef("pinged S3 by get a special ping object, %d", object.ContentLength)
	return nil
}

func (s S3StorageAdapter) s3Path(workflowName string) string {
	return path.Join("logs", workflowName+".log")
}

// leave endpoint empty if using AWS S3
func newS3Session(region, endpoint string, AccessKeyID, SecretKeyID string) (*session.Session, error) {
	if region == "" {
		return nil, errors.New("s3 region cannot be empty")
	}
	conf := &aws.Config{
		CredentialsChainVerboseErrors: nil,
		Credentials: credentials.NewCredentials(s3CredentialProvider{
			AccessKeyID: AccessKeyID,
			SecretKeyID: SecretKeyID,
		}),
		Endpoint:                nil,
		EndpointResolver:        nil,
		EnforceShouldRetryCheck: nil,
		Region:                  aws.String(region),
		DisableSSL:              nil,
		HTTPClient:              nil,
		LogLevel:                aws.LogLevel(aws.LogDebug),
		Logger: aws.LoggerFunc(func(i ...interface{}) {
			log.WithField("s3", "").Debug(i...)
		}),
		MaxRetries:                        aws.Int(3),
		Retryer:                           nil,
		DisableParamValidation:            nil,
		DisableComputeChecksums:           nil,
		S3ForcePathStyle:                  aws.Bool(true),
		S3Disable100Continue:              nil,
		S3UseAccelerate:                   nil,
		S3DisableContentMD5Validation:     nil,
		S3UseARNRegion:                    nil,
		LowerCaseHeaderMaps:               nil,
		EC2MetadataDisableTimeoutOverride: nil,
		UseDualStack:                      nil,
		SleepDelay:                        nil,
		DisableRestProtocolURICleaning:    nil,
		EnableEndpointDiscovery:           nil,
		DisableEndpointHostPrefix:         nil,
		STSRegionalEndpoint:               0,
		S3UsEast1RegionalEndpoint:         0,
	}
	if endpoint != "" {
		conf.Endpoint = aws.String(endpoint)
	}
	return session.NewSession(conf)
}

// s3CredentialProvider implements credentials.Provider
type s3CredentialProvider struct {
	AccessKeyID string
	SecretKeyID string
}

var _ credentials.Provider = (*s3CredentialProvider)(nil)

// Retrieve implements credentials.Provider
func (s s3CredentialProvider) Retrieve() (credentials.Value, error) {
	return credentials.Value{
		AccessKeyID:     s.AccessKeyID,
		SecretAccessKey: s.SecretKeyID,
		SessionToken:    "",
		ProviderName:    "",
	}, nil
}

// IsExpired implements credentials.Provider
func (s s3CredentialProvider) IsExpired() bool {
	return false
}
