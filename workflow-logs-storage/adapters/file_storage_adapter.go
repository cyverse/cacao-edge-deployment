package adapters

import (
	"context"
	"errors"
	"fmt"
	"io"
	"os"
	"path"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-edge-deployment/workflow-logs-storage/types"
)

// FileStorageAdapter ...
type FileStorageAdapter struct {
	basePath string
}

const fileStorageLogsDir = "logs"

// NewFileStorageAdapter ...
func NewFileStorageAdapter(config types.FileStorageConfig) (FileStorageAdapter, error) {
	if config.BasePath == "" {
		return FileStorageAdapter{}, errors.New("base path cannot be empty for file storage adapter")
	}
	config.BasePath = path.Clean(config.BasePath)
	stat, err := os.Stat(config.BasePath)
	if err != nil {
		return FileStorageAdapter{}, fmt.Errorf("cannot get stat on the base path, %w", err)
	}
	if !stat.IsDir() {
		return FileStorageAdapter{}, fmt.Errorf("base path for file storage adapter is not a directory")
	}

	err = os.MkdirAll(path.Join(config.BasePath, fileStorageLogsDir), 0700)
	if err != nil {
		return FileStorageAdapter{}, fmt.Errorf("cannot create logs directory, %w", err)
	}

	return FileStorageAdapter{
		basePath: config.BasePath,
	}, nil
}

// Store workflow logs as a file
func (fa FileStorageAdapter) Store(ctx context.Context, workflowName string, logs types.WorkflowLogs) error {
	f, err := os.OpenFile(fa.logsPath(workflowName), os.O_RDWR|os.O_CREATE, 0600)
	if err != nil {
		return err
	}
	defer f.Close()
	written, err := f.Write(logs.Logs)
	if err != nil {
		return err
	}
	log.WithField("len", written).Info("written logs to file")
	return nil
}

// Read workflow logs from a file
func (fa FileStorageAdapter) Read(ctx context.Context, workflowName string) (types.WorkflowLogs, error) {
	f, err := os.Open(fa.logsPath(workflowName))
	if err != nil {
		if errors.Is(err, os.ErrNotExist) {
			return types.WorkflowLogs{}, errors.Join(types.LogsNotFound, err)
		}
		return types.WorkflowLogs{}, err
	}
	defer f.Close()
	fileStat, err := f.Stat()
	if err != nil {
		return types.WorkflowLogs{}, err
	}

	all, err := io.ReadAll(f)
	if err != nil {
		return types.WorkflowLogs{}, err
	}

	return types.WorkflowLogs{Logs: all, LastModified: fileStat.ModTime()}, nil
}

// Ping ...
func (fa FileStorageAdapter) Ping(ctx context.Context) error {
	_, err := os.Stat(fa.basePath)
	if err != nil {
		return err
	}
	return nil
}

func (fa FileStorageAdapter) logsPath(workflowName string) string {
	return path.Join(fa.basePath, fileStorageLogsDir, workflowName)
}
