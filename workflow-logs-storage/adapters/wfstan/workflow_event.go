package wfstan

import "gitlab.com/cyverse/cacao-common/common"

// WorkflowSucceededEvent is the event type of WorkflowSucceeded
const WorkflowSucceededEvent common.EventType = common.EventTypePrefix + "WorkflowSucceeded"

// WorkflowFailedEvent is the event type of WorkflowFailed
const WorkflowFailedEvent common.EventType = common.EventTypePrefix + "WorkflowFailed"

// WorkflowSucceeded is an event emitted when workflow succeeded
type WorkflowSucceeded struct {
	// provider that the workflow operates on
	Provider common.ID `json:"provider,omitempty"`
	// name of the workflow
	WorkflowName string `json:"workflow_name"`
	// status of the Workflow
	Status WorkflowStatus `json:"status"`
	// assume workflow output is a json object
	WfOutputs map[string]interface{} `json:"workflow_outputs"`
	Metadata  map[string]interface{} `json:"metadata"`
}

// WorkflowFailed is an event emitted when workflow failed or errored
type WorkflowFailed struct {
	// provider that the workflow operates on
	Provider common.ID `json:"provider,omitempty"`
	// name of the workflow
	WorkflowName string `json:"workflow_name"`
	// status of the Workflow
	Status WorkflowStatus `json:"status"`
	// status of the failed node
	NodeStatus map[string]NodeStatus  `json:"node_status"`
	Metadata   map[string]interface{} `json:"metadata"`
}

// WorkflowStatus is the status of workflow
type WorkflowStatus string

// Workflow statuses
const (
	WfPending   WorkflowStatus = "Pending"
	WfRunning   WorkflowStatus = "Running"
	WfSucceeded WorkflowStatus = "Succeeded"
	WfSkipped   WorkflowStatus = "Skipped"
	WfFailed    WorkflowStatus = "Failed"
	WfError     WorkflowStatus = "Error"
	WfOmitted   WorkflowStatus = "Omitted"
)

// NodeStatus is status of a node in workflow
type NodeStatus string

// Node statuses
const (
	NodePending   NodeStatus = "Pending"
	NodeRunning   NodeStatus = "Running"
	NodeSucceeded NodeStatus = "Succeeded"
	NodeSkipped   NodeStatus = "Skipped"
	NodeFailed    NodeStatus = "Failed"
	NodeError     NodeStatus = "Error"
	NodeOmitted   NodeStatus = "Omitted"
)
