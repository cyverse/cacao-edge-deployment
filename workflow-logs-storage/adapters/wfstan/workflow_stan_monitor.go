package wfstan

import (
	"context"
	"encoding/json"
	"errors"
	cloudevents "github.com/cloudevents/sdk-go/v2"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao-edge-deployment/workflow-logs-storage/types"
	"sync"
)

// StanEventSrc monitor workflow by subscribe to events emitted by workflow's exit handler
type StanEventSrc struct {
	conf                types.StanConfig
	eventConn           messaging2.EventConnection
	eventConnCtx        context.Context
	eventConnCancelFunc context.CancelFunc
	eventConnWaitGroup  *sync.WaitGroup
	obs                 []types.WorkflowObserver
}

// NewStanEventSrc ...
func NewStanEventSrc(config types.StanConfig) (*StanEventSrc, error) {
	config.DurableName = "workflow-logs-storage"
	config.QueueGroup = "workflow-logs-storage"
	return &StanEventSrc{
		conf:                config,
		eventConn:           nil,
		eventConnCtx:        nil,
		eventConnCancelFunc: nil,
		eventConnWaitGroup:  &sync.WaitGroup{},
		obs:                 nil,
	}, nil
}

// RegisterObserver register an observer that will be notified when a workflow is finished
func (src *StanEventSrc) RegisterObserver(observer types.WorkflowObserver) error {
	src.obs = append(src.obs, observer)
	return nil
}

// Start starts monitoring STAN events emitted by workflow exit handler.
// Note that this is a blocking call.
func (src *StanEventSrc) Start(ctx context.Context) error {
	src.eventConnCtx, src.eventConnCancelFunc = context.WithCancel(ctx)
	conn, err := messaging2.NewStanConnectionFromConfig(src.conf)
	if err != nil {
		return err
	}
	src.eventConn = &conn

	src.eventConnWaitGroup.Add(1)
	err = conn.Listen(src.eventConnCtx, map[common.EventType]messaging2.EventHandlerFunc{
		WorkflowSucceededEvent: src.handleCe,
		WorkflowFailedEvent:    src.handleCe,
	}, src.eventConnWaitGroup)
	if err != nil {
		return err
	}
	return nil
}

func (src *StanEventSrc) handleCe(_ context.Context, ce cloudevents.Event, _ messaging2.EventResponseWriter) error {
	wfResult, err := workflowResultFromCloudevent(ce)
	if errors.Is(err, errBadEventType) {
		// ignore bad event type
		return nil
	}
	if err != nil {
		log.WithError(err).Errorf("fail to convert cloudevent")
		return nil
	}
	src.notifyObservers(wfResult)
	return nil
}

func (src *StanEventSrc) notifyObservers(wfResult workflowResult) {
	if len(src.obs) == 0 {
		log.Warn("no observer is registered")
		return
	}
	for _, ob := range src.obs {
		ob(wfResult.WorkflowName, wfResult.Status)
	}
}

// Close ...
func (src *StanEventSrc) Close() error {
	if src.eventConnCancelFunc != nil {
		src.eventConnCancelFunc()
	}
	log.Debug("waiting for event connection to shutdown")
	src.eventConnWaitGroup.Wait()
	return nil
}

var errBadEventType error = errors.New("bad event type")

func workflowResultFromCloudevent(ce cloudevents.Event) (workflowResult, error) {
	switch common.EventType(ce.Type()) {
	case WorkflowSucceededEvent:
		var event WorkflowSucceeded
		err := json.Unmarshal(ce.Data(), &event)
		if err != nil {
			return workflowResult{}, err
		}
		return workflowResult{
			Username:     usernameFromWorkflowEventMetadata(event.Metadata),
			WorkflowName: event.WorkflowName,
			Status:       string(event.Status),
		}, nil
	case WorkflowFailedEvent:
		var event WorkflowFailed
		err := json.Unmarshal(ce.Data(), &event)
		if err != nil {
			return workflowResult{}, err
		}
		return workflowResult{
			Username:     usernameFromWorkflowEventMetadata(event.Metadata),
			WorkflowName: event.WorkflowName,
			Status:       string(event.Status),
		}, nil
	default:
		return workflowResult{}, errBadEventType
	}
}

func usernameFromWorkflowEventMetadata(metadata map[string]interface{}) string {
	usernameRaw, ok := metadata["username"]
	if !ok {
		return ""
	}
	username, ok := usernameRaw.(string)
	if !ok {
		return ""
	}
	return username
}

type workflowResult struct {
	Username     string
	WorkflowName string
	Status       string
}
