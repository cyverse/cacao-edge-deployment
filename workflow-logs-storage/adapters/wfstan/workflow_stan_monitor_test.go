package wfstan

import (
	cloudevents "github.com/cloudevents/sdk-go/v2"
	"gitlab.com/cyverse/cacao-common/messaging"
	"reflect"
	"testing"
)

func Test_workflowResultFromCloudevent(t *testing.T) {
	type args struct {
		ce cloudevents.Event
	}
	tests := []struct {
		name    string
		args    args
		want    workflowResult
		wantErr bool
	}{
		{
			name: "workflow succeeded",
			args: args{
				ce: func() cloudevents.Event {
					ce, err := messaging.CreateCloudEvent(map[string]interface{}{
						"workflow_name": "wfname_123",
						"status":        "Succeeded",
						"metadata":      map[string]string{"username": "testuser123"},
					}, string(WorkflowSucceededEvent), "src")
					if err != nil {
						panic(err)
					}
					return ce
				}(),
			},
			want: workflowResult{
				Username:     "testuser123",
				WorkflowName: "wfname_123",
				Status:       "Succeeded",
			},
			wantErr: false,
		},
		{
			name: "workflow failed",
			args: args{
				ce: func() cloudevents.Event {
					ce, err := messaging.CreateCloudEvent(map[string]interface{}{
						"workflow_name": "wfname_123",
						"status":        "Failed",
						"metadata":      map[string]string{"username": "testuser123"},
					}, string(WorkflowFailedEvent), "src")
					if err != nil {
						panic(err)
					}
					return ce
				}(),
			},
			want: workflowResult{
				Username:     "testuser123",
				WorkflowName: "wfname_123",
				Status:       "Failed",
			},
			wantErr: false,
		},
		{
			name: "bad data",
			args: args{
				ce: func() cloudevents.Event {
					ce, err := messaging.CreateCloudEvent("bad data", string(WorkflowSucceededEvent), "src")
					if err != nil {
						panic(err)
					}
					return ce
				}(),
			},
			want:    workflowResult{},
			wantErr: true,
		},
		{
			name: "bad event type",
			args: args{
				ce: func() cloudevents.Event {
					ce, err := messaging.CreateCloudEvent("", "errBadEventType", "src")
					if err != nil {
						panic(err)
					}
					return ce
				}(),
			},
			want:    workflowResult{},
			wantErr: true,
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := workflowResultFromCloudevent(tt.args.ce)
			if (err != nil) != tt.wantErr {
				t.Errorf("workflowResultFromCloudevent() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("workflowResultFromCloudevent() got = %v, want %v", got, tt.want)
			}
		})
	}
}
