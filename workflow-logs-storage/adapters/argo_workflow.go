package adapters

import (
	"bytes"
	"context"
	"io"
	"k8s.io/apimachinery/pkg/labels"
	"sort"
	"strings"
	"time"

	"github.com/argoproj/argo-workflows/v3/cmd/argo/commands/client"
	"github.com/argoproj/argo-workflows/v3/pkg/apiclient"
	"github.com/argoproj/argo-workflows/v3/pkg/apiclient/workflow"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-edge-deployment/workflow-logs-storage/types"
	core "k8s.io/api/core/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
)

// ArgoWorkflowLogsClient is a client to fetches workflow logs from Argo Workflow
type ArgoWorkflowLogsClient struct {
	namespace string
	clientCtx context.Context
	apiClient apiclient.Client
	clientSet *kubernetes.Clientset
}

// NewArgoWorkflowLogsClient ...
func NewArgoWorkflowLogsClient(config types.ArgoConfig) (*ArgoWorkflowLogsClient, error) {
	opts, err := configToArgoClientOpts(config)
	if err != nil {
		return nil, err
	}
	ctx, apiClient, err := apiclient.NewClientFromOpts(opts)
	if err != nil {
		return nil, err
	}
	err = checkConnectionWithArgo(ctx, apiClient, config.Namespace)
	if err != nil {
		return nil, err
	}
	clientSet, err := getK8SClientSet(config)
	if err != nil {
		return nil, err
	}
	return &ArgoWorkflowLogsClient{
		namespace: config.Namespace,
		clientCtx: ctx,
		apiClient: apiClient,
		clientSet: clientSet,
	}, nil
}

func getK8SClientSet(config types.ArgoConfig) (*kubernetes.Clientset, error) {
	inClusterConfig, err := rest.InClusterConfig()
	if err != nil {
		return nil, err
	}
	if config.Token != "" {
		inClusterConfig.BearerToken = config.Token
	}
	clientSet, err := kubernetes.NewForConfig(inClusterConfig)
	if err != nil {
		return nil, err
	}
	return clientSet, nil
}

func configToArgoClientOpts(config types.ArgoConfig) (apiclient.Opts, error) {
	inClusterConfig, err := rest.InClusterConfig()
	if err != nil {
		return apiclient.Opts{}, err
	}
	token := config.Token
	if token == "" {
		token = inClusterConfig.BearerToken
	}
	return apiclient.Opts{
		ArgoServerOpts: apiclient.ArgoServerOpts{
			URL:                config.ServerURL,
			Path:               "",
			Secure:             config.HTTPS,
			InsecureSkipVerify: !config.SSLVerify,
			HTTP1:              false,
		},
		InstanceID:   "",
		AuthSupplier: func() string { return "Bearer " + token },
		Offline:      false,
	}, nil
}

// ListWorkflows return a list of workflow names.
func (c ArgoWorkflowLogsClient) ListWorkflows() ([]string, error) {
	ctx, apiClient := client.NewAPIClient(context.Background())
	ctx, cancel := context.WithTimeout(ctx, time.Second*30)
	defer cancel()
	var req = workflow.WorkflowListRequest{
		Namespace:   c.namespace,
		ListOptions: nil,
		Fields:      "items.metadata",
	}
	wfList, err := apiClient.NewWorkflowServiceClient().ListWorkflows(ctx, &req)
	if err != nil {
		return nil, err
	}
	var result = make([]string, 0, wfList.Items.Len())
	for _, item := range wfList.Items {
		result = append(result, item.Name)
	}
	return result, nil
}

// FetchLogs fetches logs for a workflow
func (c ArgoWorkflowLogsClient) FetchLogs(workflowName string) (types.WorkflowLogs, error) {
	podNames, err := c.listPods(workflowName)
	if err != nil {
		return types.WorkflowLogs{}, err
	}
	var dest bytes.Buffer
	for _, podName := range podNames {
		err = c.fetchLogs(workflowName, podName, &dest)
		if err != nil {
			return types.WorkflowLogs{}, err
		}
	}
	return types.WorkflowLogs{Logs: dest.Bytes()}, nil
}

func (c ArgoWorkflowLogsClient) fetchLogs(workflowName, podName string, dest io.Writer) error {
	podLogsLimitBytes := int64(5 << 20) // limit logs to 5MB per pod
	ctx, apiClient := client.NewAPIClient(context.Background())
	ctx, cancel := context.WithTimeout(ctx, time.Minute*3)
	defer cancel()
	var req = workflow.WorkflowLogRequest{
		Name:      workflowName,
		Namespace: c.namespace,
		PodName:   podName,
		LogOptions: &core.PodLogOptions{
			// workflow pod will have 2 container, `main` and `init`, main is the one
			// specified in the workflow, init is a helper container created by argo. we are
			// only interested in logs of `main` container.
			Container:  "main",
			LimitBytes: &podLogsLimitBytes,
		},
		Grep: "",
	}
	stream, err := apiClient.NewWorkflowServiceClient().WorkflowLogs(ctx, &req)
	if err != nil {
		return err
	}
	for {
		var entry *workflow.LogEntry
		entry, err = stream.Recv()
		if err == io.EOF {
			// new line at the end of pod logs
			_, err = dest.Write([]byte("\n"))
			return err
		} else if err != nil {
			return err
		}
		_, err = io.WriteString(dest, entry.Content)
		if err != nil {
			return err
		}
		if len(entry.Content) == 0 || entry.Content[len(entry.Content)-1] != '\n' {
			// append \n at the end of line if absent
			_, err = dest.Write([]byte("\n"))
			if err != nil {
				return err
			}
		}
	}
}

// listPods returns a list of podName for the workflow.
//
// - get a list of workflow nodes by getting the workflow.
// - sort the workflow nodes by its finish timestamp (this is a simpler way to get the order of nodes than traverse the graph of execution).
// - filter pods in the same namespace by a special workflow name label.
// - lookup pod with its node ID annotation.
func (c ArgoWorkflowLogsClient) listPods(workflowName string) (podNames []string, err error) {
	ctx, apiClient := client.NewAPIClient(context.Background())
	var req = workflow.WorkflowGetRequest{
		Name:      workflowName,
		Namespace: c.namespace,
		Fields:    "status",
	}
	wf, err := apiClient.NewWorkflowServiceClient().GetWorkflow(ctx, &req)
	if err != nil {
		return nil, err
	}
	nodes := make(argoSortNodes, 0)
	for nodeName := range wf.Status.Nodes {
		nodes = append(nodes, sortArgoNodeElem{
			NodeName: nodeName,
			NodeID:   wf.Status.Nodes[nodeName].ID,
			FinishAt: wf.Status.Nodes[nodeName].FinishedAt.Time,
		})
	}
	sort.Sort(nodes)

	labelSelector := metaV1.LabelSelector{
		MatchLabels: map[string]string{
			"workflows.argoproj.io/workflow": workflowName,
		},
		MatchExpressions: nil,
	}
	labelMap, err := metaV1.LabelSelectorAsMap(&labelSelector)
	if err != nil {
		return nil, err
	}
	podList, err := c.clientSet.CoreV1().Pods(c.namespace).List(c.clientCtx, metaV1.ListOptions{
		LabelSelector: labels.SelectorFromSet(labelMap).String(),
	})
	if err != nil {
		return nil, err
	}
	nodeIDToPodNameMapping := make(map[string]string)
	for i := range podList.Items {
		nodeID, ok := podList.Items[i].GetAnnotations()["workflows.argoproj.io/node-id"]
		if !ok {
			continue
		}
		nodeIDToPodNameMapping[nodeID] = podList.Items[i].ObjectMeta.Name
	}

	podNames = make([]string, 0, len(nodes))
	for i := range nodes {
		podName, ok := nodeIDToPodNameMapping[nodes[i].NodeID]
		if !ok {
			continue
		}
		podNames = append(podNames, podName)
	}
	// TODO consider filter out some pods, e.g. do we need to include logs for cloning git repo if it succeeded.
	return podNames, nil
}

type argoSortNodes []sortArgoNodeElem

var _ sort.Interface = (*argoSortNodes)(nil)

type sortArgoNodeElem struct {
	// note, in ArgoWorkflow 3.5+, NodeName is no longer the PodName
	NodeName string
	NodeID   string
	FinishAt time.Time
}

// Len ...
func (nodes argoSortNodes) Len() int {
	return len(nodes)
}

// Less ...
func (nodes argoSortNodes) Less(i, j int) bool {
	if nodes[i].FinishAt.Unix() == nodes[j].FinishAt.Unix() {
		// use node name as tiebreaker
		return strings.Compare(nodes[i].NodeName, nodes[j].NodeName) < 0
	}
	return nodes[i].FinishAt.Before(nodes[j].FinishAt)
}

// Swap ...
func (nodes argoSortNodes) Swap(i, j int) {
	tmp := nodes[i]
	nodes[i] = nodes[j]
	nodes[j] = tmp
}

func checkConnectionWithArgo(ctx context.Context, apiClient apiclient.Client, namespace string) error {
	var req = workflow.WorkflowListRequest{
		Namespace: namespace,
		ListOptions: &metaV1.ListOptions{
			Limit: 10,
		},
		Fields: "metadata.name",
	}
	_, err := apiClient.NewWorkflowServiceClient().ListWorkflows(ctx, &req)
	return err
}

// ArgoList implements ports.WorkflowMonitor, this list all current completed
// workflows and feed them to observers.
//
// This only run once upon Start(), the purpose is to catch up on any workflows
// that might have been missed (e.g. when the service is down, or when storage is
// down for some period of time).
type ArgoList struct {
	client *ArgoWorkflowLogsClient
	obs    []types.WorkflowObserver
}

// NewArgoListMonitor ...
func NewArgoListMonitor(client *ArgoWorkflowLogsClient) ArgoList {
	return ArgoList{
		client: client,
		obs:    nil,
	}
}

// RegisterObserver ...
func (a *ArgoList) RegisterObserver(observer types.WorkflowObserver) error {
	a.obs = append(a.obs, observer)
	return nil
}

// Start ...
func (a *ArgoList) Start(ctx context.Context) error {
	ctxListWorkflow, cancel := context.WithTimeout(a.client.clientCtx, time.Second*30)
	var req = workflow.WorkflowListRequest{
		Namespace:   a.client.namespace,
		ListOptions: nil,
		Fields:      "items.metadata,items.status.phase",
	}
	workflows, err := a.client.apiClient.NewWorkflowServiceClient().ListWorkflows(ctxListWorkflow, &req)
	cancel()
	if err != nil {
		return err
	}
	// spawn a go routine, so that this won't block for too long
	go func() {
		select {
		case <-ctx.Done():
			log.Warn("ctx canceled, skip processing existing workflows")
			return
		case <-time.After(time.Second * 3): // delay slightly in case we are in a crash loop
		}
		for _, wf := range workflows.Items {
			if wf.Status.Phase.Completed() {
				for i := range a.obs {
					a.obs[i](wf.Name, string(wf.Status.Phase))
				}
				select {
				case <-ctx.Done():
					log.Warn("ctx canceled, stop processing existing workflows in halfway")
					return
				default:
				}
			}
		}
		log.Info("done processing existing completed workflows")
	}()
	return nil
}

// Close ...
func (a *ArgoList) Close() error {
	return nil
}
