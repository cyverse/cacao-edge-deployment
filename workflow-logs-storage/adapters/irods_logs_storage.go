package adapters

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	irodsfs "github.com/cyverse/go-irodsclient/fs"
	irodstypes "github.com/cyverse/go-irodsclient/irods/types"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-edge-deployment/workflow-logs-storage/ports"
	"gitlab.com/cyverse/cacao-edge-deployment/workflow-logs-storage/types"
	"io"
	"path"
	"unicode"
)

// IRODSLogsStorageAdapter ...
type IRODSLogsStorageAdapter struct {
	basePath   string
	filesystem *irodsfs.FileSystem
}

var _ ports.LogsStorage = (*IRODSLogsStorageAdapter)(nil)
var _ ports.LogsReader = (*IRODSLogsStorageAdapter)(nil)

// NewIRODSLogsStorageAdapter creates a new LogsStorage.
// Note that this will fail if no connectivity to iRODS.
func NewIRODSLogsStorageAdapter(config types.IRODSConfig) (*IRODSLogsStorageAdapter, error) {
	filesystem, err := irodsConfigToFilesystem(config)
	if err != nil {
		return nil, err
	}
	return &IRODSLogsStorageAdapter{
		basePath:   path.Clean(config.BasePath),
		filesystem: filesystem,
	}, nil
}

// Store logs in iRODS
func (s *IRODSLogsStorageAdapter) Store(ctx context.Context, workflowName string, logs types.WorkflowLogs) error {
	logger := log.WithFields(log.Fields{
		"workflow": workflowName,
	})
	filepath, err := irodsLogsPath(s.basePath, workflowName)
	if err != nil {
		logger.WithError(err).Error("fail to generate a path for the logs in irods")
		return err
	}
	err = s.storeLogs(filepath, bytes.NewReader(logs.Logs))
	if err != nil {
		logger.WithError(err).Error("fail to upload logs to irods")
		return err
	}
	logger.Info("workflow logs saved to iRODS")
	return nil
}

func (s *IRODSLogsStorageAdapter) storeLogs(filepath string, logData io.Reader) error {
	fileHandle, err := s.filesystem.CreateFile(filepath, "", "w")
	if err != nil {
		return err
	}
	defer closeIRODSFile(fileHandle)
	written, err := io.Copy(fileHandle, logData)
	if err != nil {
		return err
	}
	log.WithFields(log.Fields{
		"bytesWritten": written,
		"path":         filepath,
	}).Debug("written logs to a irods file")
	return nil
}

// Read logs for a workflow from iRODS
func (s *IRODSLogsStorageAdapter) Read(ctx context.Context, workflowName string) (types.WorkflowLogs, error) {
	logger := log.WithFields(log.Fields{
		"workflow": workflowName,
	})
	filepath, err := irodsLogsPath(s.basePath, workflowName)
	if err != nil {
		logger.WithError(err).Error("fail to generate a path for the logs in irods")
		return types.WorkflowLogs{}, err
	}
	fileHandle, err := s.filesystem.OpenFile(filepath, "", "r")
	if err != nil {
		if errors.Is(err, &irodstypes.FileNotFoundError{}) {
			return types.WorkflowLogs{}, errors.Join(types.LogsNotFound, err)
		}
		return types.WorkflowLogs{}, err
	}
	all, err := io.ReadAll(fileHandle)
	if err != nil {
		return types.WorkflowLogs{}, err
	}
	return types.WorkflowLogs{Logs: all, LastModified: fileHandle.GetEntry().ModifyTime}, nil
}

// Ping perform some operation to check if we still have connection to iRODS.
func (s *IRODSLogsStorageAdapter) Ping(ctx context.Context) error {
	_, err := s.filesystem.List(s.basePath)
	if err != nil {
		return err
	}
	return nil
}

// Close the adapter, clean up any resources
func (s *IRODSLogsStorageAdapter) Close() error {
	s.filesystem.Release()
	return nil
}

func closeIRODSFile(handle *irodsfs.FileHandle) {
	err := handle.Close()
	if err != nil {
		log.WithError(err).Error("fail to close irods file")
	}
}

func irodsLogsPath(basePath, workflowName string) (string, error) {
	err := checkWorkflowNameForIRODSPath(workflowName)
	if err != nil {
		return "", err
	}
	return path.Join(basePath, "logs", workflowName+".log"), nil
}

func checkWorkflowNameForIRODSPath(workflowName string) error {
	for _, r := range workflowName {
		if r > unicode.MaxASCII {
			return fmt.Errorf("workflow name contains non-ASCII characters")
		}
		if unicode.IsLetter(r) {
			continue
		}
		if unicode.IsDigit(r) {
			continue
		}
		if r != '-' {
			return fmt.Errorf("username can only contain alphanumeric character or '-'")
		}
	}
	return nil
}

// use config for CyVerse iRODS
func irodsConfigToFilesystem(config types.IRODSConfig) (*irodsfs.FileSystem, error) {
	account, err := irodstypes.CreateIRODSAccount(
		types.CyVerseIRODSHost,
		types.CyVerseIRODSPort,
		config.Username,
		types.CyVerseIRODSZone,
		irodstypes.AuthSchemeNative,
		config.Password,
		"",
	)
	if err != nil {
		return nil, err
	}

	appName := "cacao_workflow_logs_storage_service"
	filesystem, err := irodsfs.NewFileSystemWithDefault(account, appName)
	if err != nil {
		return nil, err
	}
	return filesystem, nil
}
