package adapters

import (
	"context"
	"errors"
	"os"
	"reflect"
	"testing"
	"time"

	"github.com/kelseyhightower/envconfig"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-edge-deployment/workflow-logs-storage/types"
)

func getS3ConfigForIntegrationTest(t *testing.T) (config types.S3Config, skipTest bool) {
	val, ok := os.LookupEnv("S3_INTEGRATION_TEST")
	if !ok || val != "true" {
		t.Skipf("skipped, S3_INTEGRATION_TEST not set")
		return config, true
	}
	err := envconfig.Process("", &config)
	if err != nil {
		return types.S3Config{}, false
	}
	return config, false
}

func TestS3StorageAdapter_Integration(t *testing.T) {
	s3Conf, skipTest := getS3ConfigForIntegrationTest(t)
	if skipTest {
		return
	}

	s3Adapter, err := NewS3StorageAdapter(s3Conf)
	if !assert.NoError(t, err) {
		return
	}
	ctx, cancelFunc := context.WithTimeout(context.Background(), time.Minute)
	defer cancelFunc()

	testLogs := []byte("logs-foobar")
	const wfName = "foobar"
	err = s3Adapter.Store(ctx, wfName, types.WorkflowLogs{Logs: testLogs})
	if !assert.NoError(t, err) {
		return
	}
	fetchedLogs, err := s3Adapter.Read(ctx, wfName)
	if !assert.NoError(t, err) {
		return
	}
	assert.Equal(t, testLogs, fetchedLogs.Logs)

	// read workflow that does not exist
	_, err = s3Adapter.Read(ctx, "do-not-exist")
	if !assert.Errorf(t, err, "should error if workflow logs does not exist") {
		return
	}
	assert.Truef(t, errors.Is(err, types.LogsNotFound), "errors.Is() determine err is not types.LogsNotFound, %s", reflect.TypeOf(err).String())

	// overwrite an existing workflow logs
	newTestLogs := []byte("logs-foobar-new")
	err = s3Adapter.Store(ctx, wfName, types.WorkflowLogs{Logs: newTestLogs})
	if !assert.NoError(t, err) {
		return
	}
	fetchedLogs, err = s3Adapter.Read(ctx, wfName)
	if !assert.NoError(t, err) {
		return
	}
	assert.Equal(t, newTestLogs, fetchedLogs.Logs)
}
