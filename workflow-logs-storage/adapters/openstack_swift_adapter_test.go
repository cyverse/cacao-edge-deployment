package adapters

import (
	"github.com/stretchr/testify/assert"
	"reflect"
	"testing"
	"time"
)

func Test_createdDateStr(t *testing.T) {
	type args struct {
		now time.Time
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "",
			args: args{
				now: time.Date(2023, 9, 5, 1, 2, 3, 4, time.UTC),
			},
			want: "20230905",
		},
		{
			name: "",
			args: args{
				now: time.Date(2023, 10, 23, 1, 2, 3, 4, time.UTC),
			},
			want: "20231023",
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := createdDateStr(tt.args.now); got != tt.want {
				t.Errorf("createdDateStr() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_parseCreatedDateStr(t *testing.T) {
	now := time.Now()
	dateStr := createdDateStr(now)
	parsed, err := parseCreatedDateStr(dateStr)
	if !assert.NoError(t, err) {
		return
	}
	assert.Equal(t, now.Year(), parsed.Year())
	assert.Equal(t, now.Month(), parsed.Month())
	assert.Equal(t, now.Day(), parsed.Day())

	type args struct {
		date string
	}
	tests := []struct {
		name    string
		args    args
		want    time.Time
		wantErr bool
	}{
		{
			name: "",
			args: args{
				date: "20230905",
			},
			want:    time.Date(2023, 9, 5, 0, 0, 0, 0, time.UTC),
			wantErr: false,
		},
		{
			name: "",
			args: args{
				date: "20231023",
			},
			want:    time.Date(2023, 10, 23, 0, 0, 0, 0, time.UTC),
			wantErr: false,
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := parseCreatedDateStr(tt.args.date)
			if (err != nil) != tt.wantErr {
				t.Errorf("parseCreatedDateStr() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("parseCreatedDateStr() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestSwiftAdapter_objectName(t *testing.T) {
	type args struct {
		workflowName string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "basic",
			args: args{
				workflowName: "wf-123",
			},
			want: "logs/wf-123.log",
		},
		{
			name: "same path as S3 adapter",
			args: args{
				workflowName: "wf-123",
			},
			want: S3StorageAdapter{}.s3Path("wf-123"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &SwiftAdapter{}
			assert.Equalf(t, tt.want, s.objectName(tt.args.workflowName), "objectName(%v)", tt.args.workflowName)
		})
	}
}
