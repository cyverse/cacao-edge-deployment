package ports

import (
	"context"
	"gitlab.com/cyverse/cacao-edge-deployment/workflow-logs-storage/types"
)

// EventSink is a sink to publish event into
type EventSink interface {
	Init(types.StanConfig) error
	Publish(types.Event) error
}

// WorkflowMonitor monitors workflow
type WorkflowMonitor interface {
	RegisterObserver(observer types.WorkflowObserver) error
	Start(ctx context.Context) error
	Close() error
}

// WorkflowLogsRepository is abstraction over how to fetch logs
type WorkflowLogsRepository interface {
	FetchLogs(workflowName string) (types.WorkflowLogs, error)
}

// LogsStorage is a persistence storage that stores logs
type LogsStorage interface {
	Store(ctx context.Context, workflowName string, logs types.WorkflowLogs) error
}

// LogsReader is the reading side of the persistence storage that stores logs.
type LogsReader interface {
	Read(ctx context.Context, workflowName string) (types.WorkflowLogs, error)
	//Reader(ctx context.Context, workflowName string) (io.ReadCloser, error)
	Ping(ctx context.Context) error
}
