#!/bin/bash

if [ $# -lt 1 ] || [[ "$1" != "local"  && "$1" != "prod" ]]; then
    echo "Usage: $0 [local|prod]"
	exit 1
fi

CACAO_DEPLOY_ENVIRONMENT="$1"

CACAO_CONFIG_FILE="config.$CACAO_DEPLOY_ENVIRONMENT.yaml"
printf "Deploying to $CACAO_DEPLOY_ENVIRONMENT...\n"

if [ ! -f "hosts.$CACAO_DEPLOY_ENVIRONMENT.yaml" ] ; then
    cp "example_hosts.$CACAO_DEPLOY_ENVIRONMENT.yaml" "hosts.$CACAO_DEPLOY_ENVIRONMENT.yaml"
fi
if [ -L hosts.yaml ]; then
    rm hosts.yaml
fi
ln -s "hosts.$CACAO_DEPLOY_ENVIRONMENT.yaml" hosts.yaml

if [ ! -f "$CACAO_CONFIG_FILE" ] ; then
    cp "example_config.yaml" $CACAO_CONFIG_FILE
fi

if [ -L config.yaml ]; then
    rm config.yaml
fi

ln -s $CACAO_CONFIG_FILE config.yaml

ansible-playbook -i hosts.yaml playbook.yaml
