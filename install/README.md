# cacao-edge-deployment
- for local dev only
- support k3s and k3d
- need a cluster ready and `kubectl` configured to use the clusterconfig

## argo-workflow-mediator

- assume you are in `argo-workflow-mediator/install` directory
### prepare && deploy

```bash
./deploy.sh [local|prod]
```

> Note: if you have existing `config.yaml` or `hosts.yaml`, copy them to `./install/`

### local development

> Note: by default `$AWM_NAMESPACE` (where Argo workflow mediator is deployed) is `awm`, `$ARGO_NAMESPACE` (where Argo Workflow is deployed) is `argo`

```bash
skaffold dev -n $AWM_NAMESPACE
```
There is a different Dockerfile `DockerfileLocal` that can be used for local dev in skaffold.yaml, `DockerfileLocal` copys the executable `argo-workflow-mediator` from the host rather than build it in during image build.

### local testing
see `cli/README.md` for publish messages

Either `cli pub` or `stan-pub` from [nats-box image](https://hub.docker.com/r/synadia/nats-box) can be used for publish NATS Streaming messages.
> Note: since the dev nats manifest uses a hostPort to publish the ports, one can connect to it on the host machine rather than only in a pod.

### cleanup
- delete `$ARGO_NAMESPACE` and `$AWM_NAMESPACE` namespace
```bash
kubectl delete ns $ARGO_NAMESPACE
kubectl delete ns $AWM_NAMESPACE
```

- remove other artifacts
```bash
rm -rf deploy/
rm -rf workflow-def/
rm -f workflow-def.tar.gz
rm -f workflow-def.tar.gz.base64
rm -f awm_config.yaml
rm -f config.local.yaml
rm -f config.ci.yaml
rm -f config.prod.yaml
rm -f config.yaml
rm -f localhost.yaml
rm -f hosts.yaml
```
