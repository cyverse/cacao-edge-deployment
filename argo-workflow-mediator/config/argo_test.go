package config

import (
	"reflect"
	"testing"
)

func TestArgoConfigFromProviderConfig(t *testing.T) {
	type args struct {
		conf ProviderConfig
	}
	tests := []struct {
		name string
		args args
		want ArgoConfig
	}{
		{"normal", args{ProviderConfig{"host", 1234, false, false, "namespace", "svc-account", "token", "abcdABCD", "https://localhost:6443", false}}, ArgoConfig{"host", 1234, false, false, "namespace", "svc-account", "token"}},
		{"ssl", args{ProviderConfig{"host", 1234, true, true, "namespace", "svc-account", "token", "abcdABCD", "https://localhost:6443", false}}, ArgoConfig{"host", 1234, true, true, "namespace", "svc-account", "token"}},
		{"missing host", args{ProviderConfig{"", 1234, true, false, "namespace", "svc-account", "token", "abcdABCD", "https://localhost:6443", false}}, ArgoConfig{"", 1234, true, false, "namespace", "svc-account", "token"}},
		{"missing namespace", args{ProviderConfig{"host", 1234, true, false, "", "svc-account", "token", "abcdABCD", "https://localhost:6443", false}}, ArgoConfig{"host", 1234, true, false, "", "svc-account", "token"}},
		{"missing svc account", args{ProviderConfig{"host", 1234, true, false, "namespace", "", "token", "abcdABCD", "https://localhost:6443", false}}, ArgoConfig{"host", 1234, true, false, "namespace", "", "token"}},
		{"missing token", args{ProviderConfig{"host", 1234, true, false, "namespace", "svc-account", "", "abcdABCD", "https://localhost:6443", false}}, ArgoConfig{"host", 1234, true, false, "namespace", "svc-account", ""}},
		{"missing cert auth data", args{ProviderConfig{"host", 1234, true, false, "namespace", "svc-account", "token", "", "https://localhost:6443", false}}, ArgoConfig{"host", 1234, true, false, "namespace", "svc-account", "token"}},
		{"missing k8s api url", args{ProviderConfig{"host", 1234, true, false, "namespace", "svc-account", "token", "abcdABCD", "", false}}, ArgoConfig{"host", 1234, true, false, "namespace", "svc-account", "token"}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ArgoConfigFromProviderConfig(tt.args.conf); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ArgoConfigFromProviderConfig() = %v, want %v", got, tt.want)
			}
		})
	}
}
