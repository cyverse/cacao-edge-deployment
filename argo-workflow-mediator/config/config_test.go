package config

import (
	"github.com/stretchr/testify/assert"
	"testing"

	"github.com/rs/xid"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/types"
)

const testConfStr = `
provider:
  api_host: 127.0.0.1
  api_port: 2746
  cert_auth_data: abcdABCD
  k8s_api_url: https://localhost:6443
  namespace: argo
  service_account: argo-svc-account
  ssl: true
  ssl_verify: false
  token: svc-acc-token
tf_backend:
  tf_backend_type: http
  http_base_url: http://terraform-http-backend.default.svc
  http_jwt_secret_key: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa1234567890
workflow_base_dir: /opt/argo-wf-def
`

func TestParseConfigFromYAML(t *testing.T) {
	type args struct {
		dat []byte
	}

	providerConf := ProviderConfig{
		Host:            "127.0.0.1",
		Port:            2746,
		SSL:             true,
		SSLVerify:       false,
		Namespace:       "argo",
		ServiceAccount:  "argo-svc-account",
		Token:           "svc-acc-token",
		CertAuthData:    "abcdABCD",
		K8SAPIServerURL: "https://localhost:6443",
		K8SInCluster:    false,
	}
	tfBackendConfig := TFBackendConfig{
		TFBackendType: "http",
		//TFPostgresBackendConfig: TFPostgresBackendConfig{
		//	Host:                     "localhost",
		//	Port:                     5432,
		//	AdminUser:                "postgres",
		//	AdminPassword:            "postgres-password",
		//	UserCredentialDB:         "credential_database",
		//	UserCredentialTable:      "user_credential_table",
		//	TFStateDB:                "terraform_state_database",
		//	TFStateSchemaNamePostfix: "_tf_state",
		//	TFWorkspaceNamePostfix:   "_template",
		//},
		TFHTTPBackendConfig: TFHTTPBackendConfig{
			HTTPBaseURL:      "http://terraform-http-backend.default.svc",
			HTTPJWTSecretKey: "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa1234567890",
			HTTPJWTLifetime:  "",
		},
	}

	tests := []struct {
		name    string
		args    args
		want    *Config
		wantErr bool
	}{
		{
			name: "normal",
			args: args{[]byte(testConfStr)},
			want: &Config{
				ProviderConf: providerConf,
				ProviderList: nil,
				TFBackend:    tfBackendConfig,
			},
			wantErr: false,
		},
		{"empty", args{[]byte("")}, nil, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ParseConfigFromYAML(tt.args.dat)
			if (err != nil) != tt.wantErr {
				t.Errorf("ParseConfigFromYAML() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			assert.Equalf(t, tt.want, got, "ParseConfigFromYAML() = %v, want %v", got, tt.want)
		})
	}
}

func TestValidateProviderConfig(t *testing.T) {
	testProvider := types.Provider("provider-" + xid.New().String())
	type args struct {
		provider types.Provider
		conf     *ProviderConfig
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{"normal", args{testProvider, &ProviderConfig{"host", 1234, false, false, "namespace", "svc-account", "token", "abcdABCD", "https://localhost:6443", false}}, false},
		{"ssl", args{testProvider, &ProviderConfig{"host", 1234, true, true, "namespace", "svc-account", "token", "abcdABCD", "https://localhost:6443", false}}, false},
		{"missing host", args{testProvider, &ProviderConfig{"", 1234, true, false, "namespace", "svc-account", "token", "abcdABCD", "https://localhost:6443", false}}, true},
		{"missing namespace", args{testProvider, &ProviderConfig{"host", 1234, true, false, "", "svc-account", "token", "abcdABCD", "https://localhost:6443", false}}, true},
		{"missing svc account", args{testProvider, &ProviderConfig{"host", 1234, true, false, "namespace", "", "token", "abcdABCD", "https://localhost:6443", false}}, true},
		{"missing token", args{testProvider, &ProviderConfig{"host", 1234, true, false, "namespace", "svc-account", "", "abcdABCD", "https://localhost:6443", false}}, true},
		{"missing cert auth data", args{testProvider, &ProviderConfig{"host", 1234, true, false, "namespace", "svc-account", "token", "", "https://localhost:6443", false}}, true},
		{"missing k8s api url", args{testProvider, &ProviderConfig{"host", 1234, true, false, "namespace", "svc-account", "token", "abcdABCD", "", false}}, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := validateProviderConfig(tt.args.provider, tt.args.conf); (err != nil) != tt.wantErr {
				t.Errorf("validateProviderConfig() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestValidateConfig(t *testing.T) {
	type args struct {
		conf *Config
	}

	providerConf := ProviderConfig{"host", 1234, false, false, "namespace", "svc-account", "token", "abcdABCD", "https://localhost:6443", false}
	emptyProviderConf := ProviderConfig{}
	tfBackendConfig := TFBackendConfig{
		"pg",
		TFPostgresBackendConfig{"host", 1234, "admin_user", "admin_password", "user_cred_db", "user_cred_table", "tf_state_db", "tf_state_schema_postfix", "tf_workspace_name_postfix"},
		TFHTTPBackendConfig{},
	}

	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "normal",
			args: args{
				conf: &Config{
					ProviderConf: providerConf,
					ProviderList: nil,
					TFBackend:    tfBackendConfig,
				},
			},
			wantErr: false,
		},
		{
			name: "bad TF config",
			args: args{conf: &Config{
				ProviderConf: providerConf,
				ProviderList: nil,
				TFBackend:    TFBackendConfig{},
			},
			},
			wantErr: true,
		},
		{
			name: "bad provider config",
			args: args{conf: &Config{
				ProviderConf: emptyProviderConf,
				ProviderList: nil,
				TFBackend:    tfBackendConfig,
			},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := validateConfig(tt.args.conf); (err != nil) != tt.wantErr {
				t.Errorf("validateConfig() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
