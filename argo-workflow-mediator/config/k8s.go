package config

import (
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/utils"
)

// K8SConfig is configuration for Argo Workflow
type K8SConfig struct {
	Namespace string
	// if nil then uses in-cluster config
	Kubeconfig []byte
}

// K8SConfigFromProviderConfig builds K8SConfig from provider config
func K8SConfigFromProviderConfig(conf ProviderConfig) (*K8SConfig, error) {
	// use in cluster config
	if conf.K8SInCluster {
		return &K8SConfig{Namespace: conf.Namespace, Kubeconfig: nil}, nil
	}

	param := utils.SvcAccountKubeConfParam{
		CertAuthData:    conf.CertAuthData,
		ServerURL:       conf.K8SAPIServerURL,
		SvcAccount:      conf.ServiceAccount,
		SvcAccountToken: conf.Token,
		Namespace:       conf.Namespace,
	}

	kubeconf, err := utils.BuildKubeconfigFromSvcAccount(param)
	if err != nil {
		return nil, err
	}

	return &K8SConfig{
		Namespace:  conf.Namespace,
		Kubeconfig: kubeconf,
	}, nil
}
