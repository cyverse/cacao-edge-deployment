package domain

import (
	"errors"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/ports"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/types"
)

// WorkflowTerminateHandler is handler for WorkflowTerminateCmd
type WorkflowTerminateHandler struct {
	argoCli ports.ArgoClient
	// sink to publish the workflow terminated event into
	eventSink ports.OutgoingEvents
}

// NewWorkflowTerminateHandler creates a new WorkflowTerminateCmdHandler
func NewWorkflowTerminateHandler(
	argoCli ports.ArgoClient,
	eventSink ports.OutgoingEvents,
) WorkflowTerminateHandler {
	return WorkflowTerminateHandler{argoCli, eventSink}
}

// Handle is the handler function, same signature as ports.MsgHandler
func (h WorkflowTerminateHandler) Handle(event types.WorkflowTerminate) error {
	var err error
	logger := log.WithFields(log.Fields{
		"package":  "domain",
		"function": "WorkflowTerminateHandler.Handle",
	})

	err = h.validateEvent(event)
	if err != nil {
		return err
	}

	err = h.terminateWorkflow(event.Provider, event.WorkflowName)
	if err != nil {
		h.publishTerminateFailedEvent(event, types.FailToTerminate, err.Error())
		return err
	}

	status, err := h.workflowStatus(event.Provider, event.WorkflowName)
	if err != nil {
		logger.WithFields(log.Fields{"workflow": event.WorkflowName, "status_check_err": err}).Info("workflow terminated")
		h.publishTerminateFailedEvent(event, types.FailToRetrieveStatus, err.Error())
		return err
	}
	logger.WithFields(log.Fields{"workflow": event.WorkflowName, "status": status}).Info("workflow terminated")

	h.publishTerminatedEvent(event, status)
	return nil
}

func (h WorkflowTerminateHandler) validateEvent(event types.WorkflowTerminate) error {
	if event.WorkflowName == "" {
		err := errors.New("workflow name empty")
		h.publishTerminateFailedEvent(event, types.WorkflowNameEmpty, err.Error())
		return err
	}

	return nil
}

func (h WorkflowTerminateHandler) terminateWorkflow(provider types.Provider, wfName string) error {
	err := h.argoCli.TerminateWorkflow(wfName)
	if err != nil {
		return err
	}
	return nil
}

func (h WorkflowTerminateHandler) workflowStatus(provider types.Provider, wfName string) (string, error) {
	status, err := h.argoCli.WorkflowStatus(wfName)
	if err != nil {
		return "", err
	}
	return string(status.Phase), nil
}

// Publish WorkflowTerminated event
func (h WorkflowTerminateHandler) publishTerminatedEvent(reqEvent types.WorkflowTerminate, status string) {
	event := types.WorkflowTerminated{
		RequestID:      reqEvent.EventID,
		TransactionID:  reqEvent.TransactionID,
		Provider:       reqEvent.Provider,
		WorkflowName:   reqEvent.WorkflowName,
		WorkflowStatus: status,
	}
	h.eventSink.WorkflowTerminated(event)
}

// Publish WorkflowTerminateFailed event
func (h WorkflowTerminateHandler) publishTerminateFailedEvent(reqEvent types.WorkflowTerminate, errType types.EventErrorType, errMsg string) {
	event := types.WorkflowTerminateFailed{
		RequestID:     reqEvent.EventID,
		TransactionID: reqEvent.TransactionID,
		Provider:      reqEvent.Provider,
		WorkflowName:  reqEvent.WorkflowName,
		Error:         errType,
		Msg:           errMsg,
	}
	h.eventSink.WorkflowTerminateFailed(event)
}
