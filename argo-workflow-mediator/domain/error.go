package domain

import (
	"fmt"

	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/types"
)

// UnsupportedProvider indicates the provider is unsupported
type UnsupportedProvider struct {
	Provider types.Provider
	reason   string
}

func (err UnsupportedProvider) Error() string {
	if err.reason != "" {
		return fmt.Sprintf("Unsupported provider %s, %s", err.Provider, err.reason)
	}
	return fmt.Sprintf("Unsupported provider %s", err.Provider)
}

// MissingEventID indicates Event is missing EventID
type MissingEventID struct {
	types.EventType
}

func (err MissingEventID) Error() string {
	return fmt.Sprintf("Missing EventID from event, event type :'%s'", err.EventType)
}
