package domain

import (
	"context"
	"fmt"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"reflect"
	"sync"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/config"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/ports"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/types"
)

// Domain is the entrypoint to the domain logic/service
type Domain struct {
	// Number of event worker to spawn
	EventWorkerCount uint
	// Buffer capacity of the go channel for events
	EventChannelBufferCapacity uint
	// Event adapter, incoming
	EventSrc ports.EventSrc
	// Argo clients for all providers
	ArgoCli ports.ArgoClient
	// Source for workflow definition for all providers
	WfSrc ports.WorkflowDefSrc
	// Terraform backend
	TfBackend ports.TFBackend
	// K8S Credential Secret Store for all providers
	CredStore ports.K8SCredSecretStore
	// config for outbound NATS
	natsOut messaging2.NatsStanMsgConfig

	providerList []types.Provider
}

// NewDomain creates new Domain object
func NewDomain() Domain {
	domain := Domain{}
	return domain
}

// Init initialize domain.
func (domain *Domain) Init(conf *config.Config, envConfig config.EnvConfig) error {

	if domain.EventWorkerCount == 0 {
		return fmt.Errorf("EventWorkerCount is 0")
	}

	// Init argo clients for all provider
	domain.ArgoCli.Init(config.ArgoConfigFromProviderConfig(conf.ProviderConf))

	// Init workflow definition source for all provider
	err := domain.WfSrc.Init(config.WorkflowConfig{WorkflowBaseDir: envConfig.WorkflowBaseDir})
	if err != nil {
		return err
	}

	// Init k8s secret adapters for all provider
	k8sConf, err := config.K8SConfigFromProviderConfig(conf.ProviderConf)
	if err != nil {
		return err
	}
	err = domain.CredStore.Init(*k8sConf)
	if err != nil {
		return err
	}

	// Init terraform backend
	err = domain.TfBackend.Init(conf.TFBackend)
	if err != nil {
		return err
	}

	// Init event source
	domain.EventSrc.SetHandlers(domain)

	// store the outbound NATS config
	domain.natsOut = envConfig.NatsStanMsgConfig

	domain.providerList = conf.ProviderList
	return nil
}

// Start the domain service
func (domain *Domain) Start(ctx context.Context) {

	var wg sync.WaitGroup

	wg.Add(1)
	err := domain.EventSrc.Start(ctx, &wg)
	if err != nil {
		log.Fatal(err)
	}
	wg.Wait()
}

var _ ports.EventHandlers = (*Domain)(nil)

// WorkflowCreateHandler ...
func (domain *Domain) WorkflowCreateHandler(request types.WorkflowCreate, sink ports.OutgoingEvents) {
	logger := log.WithField("event_type", request.EventType())
	handler := NewWorkflowCreateHandler(domain, sink)
	err := handler.Handle(request)
	if err != nil {
		logger.WithField("error_type", reflect.TypeOf(err)).Error(err)
	}
}

// WorkflowTerminateHandler ...
func (domain *Domain) WorkflowTerminateHandler(request types.WorkflowTerminate, sink ports.OutgoingEvents) {
	logger := log.WithField("event_type", request.EventType())
	handler := NewWorkflowTerminateHandler(domain.ArgoCli, sink)
	err := handler.Handle(request)
	if err != nil {
		logger.WithField("error_type", reflect.TypeOf(err)).Error(err)
	}
}

// WorkflowResubmitHandler ...
func (domain *Domain) WorkflowResubmitHandler(request types.WorkflowResubmit, sink ports.OutgoingEvents) {
	logger := log.WithField("event_type", request.EventType())
	handler := NewWorkflowResubmitHandler(domain.ArgoCli, sink)
	err := handler.Handle(request)
	if err != nil {
		logger.WithField("error_type", reflect.TypeOf(err)).Error(err)
	}
}
