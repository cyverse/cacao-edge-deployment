package domain

import (
	"errors"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/ports"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/types"
)

// WorkflowResubmitHandler is a msg handler for WorkflowResubmit
type WorkflowResubmitHandler struct {
	argoCli   ports.ArgoClient
	eventSink ports.OutgoingEvents
}

// NewWorkflowResubmitHandler creates new WorkflowResubmitHandler
func NewWorkflowResubmitHandler(
	argoCli ports.ArgoClient,
	eventSink ports.OutgoingEvents,
) WorkflowResubmitHandler {
	return WorkflowResubmitHandler{argoCli, eventSink}
}

// Handle is the handler function
func (h WorkflowResubmitHandler) Handle(event types.WorkflowResubmit) error {
	var err error
	logger := log.WithFields(log.Fields{
		"package":  "domain",
		"function": "WorkflowResubmitHandler.Handle",
	})
	err = h.validateEvent(event)
	if err != nil {
		return err
	}

	newWfName, err := h.resubmitWorkflow(event.Provider, event.WorkflowName)
	if err != nil {
		logger.WithFields(log.Fields{"error": err, "event": event}).Error("fail to resubmit workflow")
		h.publishResubmitFailedEvent(event, types.FailToResubmit, err.Error())
		return err
	}
	logger.WithField("new_workflow", newWfName).Info("workflow resubmitted")

	h.publishResubmittedEvent(event, newWfName)

	return nil
}

func (h WorkflowResubmitHandler) validateEvent(event types.WorkflowResubmit) error {
	if event.WorkflowName == "" {
		err := errors.New("workflow name empty")
		h.publishResubmitFailedEvent(event, types.WorkflowNameEmpty, err.Error())
		return err
	}

	return nil
}

func (h WorkflowResubmitHandler) resubmitWorkflow(provider types.Provider, wfName string) (string, error) {
	newWf, err := h.argoCli.ResubmitWorkflow(wfName)
	if err != nil {
		return "", err
	}

	return newWf.Name, nil
}

func (h WorkflowResubmitHandler) publishResubmittedEvent(reqEvent types.WorkflowResubmit, newWfName string) {
	event := types.WorkflowResubmitted{
		RequestID:       reqEvent.EventID,
		TransactionID:   reqEvent.TransactionID,
		Provider:        reqEvent.Provider,
		WorkflowName:    reqEvent.WorkflowName,
		NewWorkflowName: newWfName,
	}
	h.eventSink.WorkflowResubmitted(event)
}

func (h WorkflowResubmitHandler) publishResubmitFailedEvent(reqEvent types.WorkflowResubmit, errType types.EventErrorType, errMsg string) {
	event := types.WorkflowResubmitFailed{
		RequestID:     reqEvent.EventID,
		TransactionID: reqEvent.TransactionID,
		Provider:      reqEvent.Provider,
		WorkflowName:  reqEvent.WorkflowName,
		Error:         errType,
		Msg:           errMsg,
	}
	h.eventSink.WorkflowResubmitFailed(event)

}
