package domain

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/messaging2"
	wf "gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/domain/workflow"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/ports"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/types"
)

// WorkflowCreateHandler is a msg handler for WorkflowCreate
type WorkflowCreateHandler struct {
	argoCli      ports.ArgoClient
	wfSrc        ports.WorkflowDefSrc
	eventSink    ports.OutgoingEvents
	tfBackend    ports.TFBackend
	credStore    ports.K8SCredSecretStore
	natsOut      messaging2.NatsStanMsgConfig
	providerList []types.Provider
}

// NewWorkflowCreateHandler creates a WorkflowCreateCmdHandler
func NewWorkflowCreateHandler(domain *Domain, sink ports.OutgoingEvents) WorkflowCreateHandler {
	return WorkflowCreateHandler{
		argoCli:      domain.ArgoCli,
		wfSrc:        domain.WfSrc,
		eventSink:    sink,
		tfBackend:    domain.TfBackend,
		credStore:    domain.CredStore,
		natsOut:      domain.natsOut,
		providerList: domain.providerList,
	}
}

// Handle handles the types.WorkflowCreate.
func (h WorkflowCreateHandler) Handle(event types.WorkflowCreate) error {
	var err error
	logger := log.WithFields(log.Fields{
		"package":  "domain",
		"function": "WorkflowCreateHandler.Handle",
	})
	if err = h.validateEvent(event); err != nil {
		return err
	}

	wfName, err := h.createWorkflow(event)
	if err != nil {
		h.publishFailedEvent(event, types.FailToCreateWorkflow, err.Error())
		return err
	}

	logger.WithField("workflow", wfName).Info("workflow created")

	h.publishCreatedEvent(event, wfName)
	return nil
}

func (h WorkflowCreateHandler) validateEvent(event types.WorkflowCreate) error {
	err := h.checkProvider(event)
	if err != nil {
		h.publishFailedEvent(event, types.ProviderNotSupported, err.Error())
		return err
	}
	return nil
}

func (h WorkflowCreateHandler) checkProvider(event types.WorkflowCreate) error {
	if len(h.providerList) == 0 {
		return nil
	}
	for _, p := range h.providerList {
		if event.Provider == p {
			return nil
		}
	}
	return UnsupportedProvider{
		Provider: event.Provider,
		reason:   "",
	}
}

func (h WorkflowCreateHandler) createWorkflow(event types.WorkflowCreate) (string, error) {
	fac, err := wf.NewFactory(
		event.TransactionID,
		event.Provider,
		h.argoCli,
		h.wfSrc,
		event.WorkflowFilename,
		h.tfBackend,
		h.credStore,
		h.natsOut,
	)
	if err != nil {
		return "", err
	}
	wfName, err := fac.Create(event.WfDat)
	if err != nil {
		return "", err
	}
	return wfName, nil
}

// Publish WorkflowCreated event
func (h WorkflowCreateHandler) publishCreatedEvent(reqEvent types.WorkflowCreate, wfName string) {
	event := types.WorkflowCreated{
		RequestID:     reqEvent.EventID,
		TransactionID: reqEvent.TransactionID,
		Provider:      reqEvent.Provider,
		WorkflowName:  wfName,
	}
	h.eventSink.WorkflowCreated(event)
}

// Publish WorkflowCreateFailed event
func (h WorkflowCreateHandler) publishFailedEvent(reqEvent types.WorkflowCreate, errType types.EventErrorType, errMsg string) {
	event := types.WorkflowCreateFailed{
		RequestID:     reqEvent.EventID,
		TransactionID: reqEvent.Transaction(),
		Provider:      reqEvent.Provider,
		Error:         errType,
		Msg:           errMsg,
	}
	h.eventSink.WorkflowCreateFailed(event)
}
