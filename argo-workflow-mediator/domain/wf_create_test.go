package domain

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"testing"

	"github.com/rs/xid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/adapters/mock"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/config"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/domain/workflow"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/ports"
	portsmocks "gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/ports/mocks"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/types"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/utils"
)

func openstackCredBase64(key []byte) string {
	cred := types.OpenStackCredential{
		IdentityAPIVersion: "3",
		RegionName:         "region",
		AuthURL:            "https://openstack.org",
		ProjectDomainID:    "project-domain",
		ProjectID:          "project-id-123",
		ProjectName:        "project-name",
		UserDomainName:     "user-domain",
		Username:           "username",
		Password:           "password",
	}
	credJSONStr, err := json.Marshal(cred)
	if err != nil {
		panic(err)
	}

	credCiphertext, err := utils.AESEncrypt(key, credJSONStr)
	if err != nil {
		panic(err)
	}
	return base64.StdEncoding.EncodeToString(credCiphertext)
}

func TestWorkflowCreateHandler_Handle(t *testing.T) {
	provider := types.Provider("provider-" + xid.New().String())
	encryptKey := utils.GenerateAES256Key()

	type fields struct {
		argoClis   ports.ArgoClient
		wfSrcs     ports.WorkflowDefSrc
		eventSink  *portsmocks.OutgoingEvents
		tfBackend  ports.TFBackend
		credStores ports.K8SCredSecretStore
		natsOut    messaging2.NatsStanMsgConfig
	}
	type args struct {
		event types.WorkflowCreate
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "mock",
			fields: fields{
				argoClis: mock.NewMockArgoClient(),
				wfSrcs: func() ports.WorkflowDefSrc {
					wfSrc := mock.NewMockWorkflowDefSrc()
					wfSrc.Register("mock.yaml", &workflow.HelloWorldWorkflow)
					return wfSrc
				}(),
				eventSink: func() *portsmocks.OutgoingEvents {
					sink := &portsmocks.OutgoingEvents{}
					sink.On("WorkflowCreated", types.WorkflowCreated{
						EventID:       "",
						RequestID:     "test-event-id-123",
						TransactionID: "",
						Provider:      provider,
						WorkflowName:  "-0",
					}).Once()
					return sink
				}(),
				tfBackend: &mock.TFMockBackend{},
				credStores: func() ports.K8SCredSecretStore {
					credStore := mock.K8SCredSecretStore{}
					err := credStore.Init(config.K8SConfig{})
					assert.NoError(t, err)
					credStore.SetKey(encryptKey)
					return &credStore
				}(),
				natsOut: messaging2.NatsStanMsgConfig{},
			},
			args: args{
				event: types.WorkflowCreate{
					EventID:          "test-event-id-123",
					Provider:         provider,
					Username:         nil,
					WorkflowFilename: "mock.yml",
					WfDat: map[string]interface{}{
						"module_name":        "openstack-module",
						"username":           "test_user1",
						"credential_secrets": "secret1",
						"ansible_vars":       map[string]string{"key": "value"},
					},
				},
			},
			wantErr: assert.NoError,
		},
		{
			name: "terraform",
			fields: fields{
				argoClis: mock.NewMockArgoClient(),
				wfSrcs: func() ports.WorkflowDefSrc {
					wfSrc := mock.NewMockWorkflowDefSrc()
					wfSrc.Register("terraform.yml", &workflow.HelloWorldWorkflow)
					return wfSrc
				}(),
				eventSink: func() *portsmocks.OutgoingEvents {
					sink := &portsmocks.OutgoingEvents{}
					sink.On("WorkflowCreated", types.WorkflowCreated{
						EventID:       "",
						RequestID:     "test-event-id-123",
						TransactionID: "",
						Provider:      provider,
						WorkflowName:  "-0",
					}).Once()
					return sink
				}(),
				tfBackend: func() *mock.TFMockBackend {
					tfBackend := &mock.TFMockBackend{}
					err := tfBackend.Init(config.TFBackendConfig{})
					assert.NoError(t, err)
					return tfBackend
				}(),
				credStores: func() ports.K8SCredSecretStore {
					credStore := mock.K8SCredSecretStore{}
					err := credStore.Init(config.K8SConfig{})
					assert.NoError(t, err)
					credStore.SetKey(encryptKey)
					return &credStore
				}(),
				natsOut: messaging2.NatsStanMsgConfig{},
			},
			args: args{
				event: types.WorkflowCreate{
					EventID:          "test-event-id-123",
					Provider:         provider,
					Username:         stringPtr("test_user1"),
					WorkflowFilename: "terraform.yml",
					WfDat: map[string]interface{}{
						"deployment_id": "d12345",
						"template_id":   "t12345",
						"tf_state_key":  "tf12345",
						"git_url":       "https://gitlab.com/cyverse/foobar",
						"git_upstream": map[string]string{
							"branch": "master",
						},
						"sub_path":      "openstack-module",
						"username":      "test_user1",
						"cloud_cred_id": "secret1",
						"ansible_vars": map[string]string{
							"key": "value",
						},
						"cloud_cred": openstackCredBase64(encryptKey),
					},
				},
			},
			wantErr: assert.NoError,
		},
		{
			name: "workflow-file-non-exist",
			fields: fields{
				argoClis: mock.NewMockArgoClient(),
				wfSrcs: func() ports.WorkflowDefSrc {
					wfSrc := mock.NewMockWorkflowDefSrc()
					wfSrc.Register("terraform.yml", &workflow.HelloWorldWorkflow)
					return wfSrc
				}(),
				eventSink: func() *portsmocks.OutgoingEvents {
					sink := &portsmocks.OutgoingEvents{}
					sink.On("WorkflowCreateFailed", types.WorkflowCreateFailed{
						EventID:       "",
						RequestID:     "test-event-id-123",
						TransactionID: "",
						Provider:      provider,
						Error:         "FailToCreateWorkflow",
						Msg:           "Unknown Workflow: FooBar.yml",
					}).Once()
					return sink
				}(),
				tfBackend: func() *mock.TFMockBackend {
					tfBackend := &mock.TFMockBackend{}
					err := tfBackend.Init(config.TFBackendConfig{})
					assert.NoError(t, err)
					return tfBackend
				}(),
				credStores: func() ports.K8SCredSecretStore {
					credStore := mock.K8SCredSecretStore{}
					err := credStore.Init(config.K8SConfig{})
					assert.NoError(t, err)
					credStore.SetKey(encryptKey)
					return &credStore
				}(),
				natsOut: messaging2.NatsStanMsgConfig{},
			},
			args: args{
				event: types.WorkflowCreate{
					EventID:          "test-event-id-123",
					Provider:         provider,
					Username:         stringPtr("test_user1"),
					WorkflowFilename: "FooBar.yml", // workflow file does not exist
					WfDat: map[string]interface{}{
						"deployment_id": "d12345",
						"template_id":   "t12345",
						"tf_state_key":  "tf12345",
						"git_url":       "https://gitlab.com/cyverse/foobar",
						"git_upstream": map[string]string{
							"branch": "master",
						},
						"sub_path":      "openstack-module",
						"username":      "test_user1",
						"cloud_cred_id": "secret1",
						"ansible_vars": map[string]string{
							"key": "value",
						},
						"cloud_cred": openstackCredBase64(encryptKey),
					},
				},
			},
			wantErr: assert.Error,
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h := WorkflowCreateHandler{
				argoCli:   tt.fields.argoClis,
				wfSrc:     tt.fields.wfSrcs,
				eventSink: tt.fields.eventSink,
				tfBackend: tt.fields.tfBackend,
				credStore: tt.fields.credStores,
				natsOut:   tt.fields.natsOut,
			}
			tt.wantErr(t, h.Handle(tt.args.event), fmt.Sprintf("Handle(%v)", tt.args.event))
			tt.fields.eventSink.AssertExpectations(t)
		})
	}
}

func stringPtr(s string) *string {
	return &s
}
