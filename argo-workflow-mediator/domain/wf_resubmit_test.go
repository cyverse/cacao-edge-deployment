package domain

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/adapters/mock"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/domain/workflow"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/ports"
	portsmocks "gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/ports/mocks"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/types"
)

func TestWorkflowResubmitHandler_Handle(t *testing.T) {
	type fields struct {
		argoCli   ports.ArgoClient
		eventSink *portsmocks.OutgoingEvents
	}
	type args struct {
		event types.WorkflowResubmit
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "empty event",
			fields: fields{
				argoCli: mock.NewMockArgoClient(),
				eventSink: func() *portsmocks.OutgoingEvents {
					out := &portsmocks.OutgoingEvents{}
					out.On("WorkflowResubmitFailed", types.WorkflowResubmitFailed{
						EventID:       "",
						RequestID:     "",
						TransactionID: "",
						Provider:      "",
						WorkflowName:  "",
						Error:         "WorkflowNameEmpty",
						Msg:           "workflow name empty",
					}).Once()
					return out
				}(),
			},
			args: args{
				event: types.WorkflowResubmit{},
			},
			wantErr: assert.Error,
		},
		{
			name: "resubmit",
			fields: fields{
				argoCli: func() ports.ArgoClient {
					wfName := "workflow-123"
					argoCli := mock.NewMockArgoClient()
					wfDef := workflow.HelloWorldWorkflow.DeepCopy()
					wfDef.Name = "mock-wf-"
					wf, err := argoCli.CreateWorkflowWithName(wfName, wfDef)
					if err != nil {
						panic(err)
					}
					// modify workflow name
					result, _ := argoCli.GetWorkflow(wf.Name)
					result.Name = wfName

					wf.Name = wfName
					return argoCli
				}(),
				eventSink: func() *portsmocks.OutgoingEvents {
					out := &portsmocks.OutgoingEvents{}
					out.On("WorkflowResubmitted", types.WorkflowResubmitted{
						EventID:         "",
						RequestID:       "event-aaaaaaaaaaaaaaaaaaaa",
						TransactionID:   "tid-aaaaaaaaaaaaaaaaaaaa",
						Provider:        "provider-aaaaaaaaaaaaaaaaaaaa",
						WorkflowName:    "workflow-123",
						NewWorkflowName: "workflow-123-1",
					}).Once()
					return out
				}(),
			},
			args: args{
				event: types.WorkflowResubmit{
					EventID:       "event-aaaaaaaaaaaaaaaaaaaa",
					TransactionID: "tid-aaaaaaaaaaaaaaaaaaaa",
					Provider:      "provider-aaaaaaaaaaaaaaaaaaaa",
					WorkflowName:  "workflow-123",
				},
			},
			wantErr: assert.NoError,
		},
		{
			name: "workflow-not-exist",
			fields: fields{
				argoCli: func() ports.ArgoClient {
					argoCli := mock.NewMockArgoClient()
					return argoCli
				}(),
				eventSink: func() *portsmocks.OutgoingEvents {
					out := &portsmocks.OutgoingEvents{}
					out.On("WorkflowResubmitFailed", types.WorkflowResubmitFailed{
						EventID:       "",
						RequestID:     "event-aaaaaaaaaaaaaaaaaaaa",
						TransactionID: "tid-aaaaaaaaaaaaaaaaaaaa",
						Provider:      "provider-aaaaaaaaaaaaaaaaaaaa",
						WorkflowName:  "workflow-that-does-not-exists-123",
						Error:         "FailToResubmit",
						Msg:           "workflow workflow-that-does-not-exists-123 not found",
					}).Once()
					return out
				}(),
			},
			args: args{
				event: types.WorkflowResubmit{
					EventID:       "event-aaaaaaaaaaaaaaaaaaaa",
					TransactionID: "tid-aaaaaaaaaaaaaaaaaaaa",
					Provider:      "provider-aaaaaaaaaaaaaaaaaaaa",
					WorkflowName:  "workflow-that-does-not-exists-123",
				},
			},
			wantErr: assert.Error,
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h := WorkflowResubmitHandler{
				argoCli:   tt.fields.argoCli,
				eventSink: tt.fields.eventSink,
			}
			tt.wantErr(t, h.Handle(tt.args.event), fmt.Sprintf("Handle(%v)", tt.args.event))
			tt.fields.eventSink.AssertExpectations(t)
		})
	}
}
