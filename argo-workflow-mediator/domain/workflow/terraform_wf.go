package workflow

import (
	"crypto/sha256"
	"encoding/base64"
	"encoding/hex"
	"errors"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao-common/service"
	"path"
	"path/filepath"
	"strings"

	wfv1 "github.com/argoproj/argo-workflows/v3/pkg/apis/workflow/v1alpha1"
	"github.com/mitchellh/mapstructure"
	"github.com/rs/xid"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/ports"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/types"
	yaml "gopkg.in/yaml.v3"
)

// name of an empty K8S secret, it is used to populate the secret name when
// a secret is not provided. Argo workflow does not take empty secret (""),
// even if the secret is declared as optional, so a specific constant is
// required for indicating secret is not provided.
const emptySecretName = "empty-secret"

// TerraformFactory is a factory to create Terraform Workflow
type TerraformFactory struct {
	transaction common.TransactionID
	prov        types.Provider
	cli         ports.ArgoClient
	src         ports.WorkflowDefSrc
	tfBackend   ports.TFBackend
	credStore   ports.K8SCredSecretStore
	natsOut     messaging2.NatsStanMsgConfig
}

// NewTerraformFactory creates a TerraformFactory
func NewTerraformFactory(
	transaction common.TransactionID,
	provider types.Provider,
	cli ports.ArgoClient,
	wfSrc ports.WorkflowDefSrc,
	tfBackend ports.TFBackend,
	credStore ports.K8SCredSecretStore,
	natsOut messaging2.NatsStanMsgConfig,
) (*TerraformFactory, error) {
	return &TerraformFactory{
		transaction: transaction,
		prov:        provider,
		cli:         cli,
		src:         wfSrc,
		tfBackend:   tfBackend,
		credStore:   credStore,
		natsOut:     natsOut,
	}, nil
}

// Provider returns the provider that this factory operates on
func (fac TerraformFactory) Provider() types.Provider {
	return fac.prov
}

// The data that goes into the parameter of the workflow definition when submitting it.
type terraformParam struct {
	TransactionID       common.TransactionID
	DeploymentID        common.ID
	TemplateType        service.TemplateTypeName
	TemplateID          common.ID
	TerraformStateKey   string
	GitURL              string
	GitBranch           string
	GitTag              string
	GitCommit           string
	Path                string
	Username            string
	CloudCredSecretName string
	GitCredSecretName   string
	AnsibleVars         map[string]interface{}
	Provider            string
	NatsURL             string
	NatsClusterID       string
	NatsSubject         string
}

// Create Terraform Workflow, the data should match TerraformParam
func (fac TerraformFactory) Create(data map[string]interface{}) (string, error) {

	wfData, err := fac.parseWorkflowData(data)
	if err != nil {
		return "", err
	}

	credSecretName, err := fac.prepK8SSecretForCloudCred(*wfData)
	if err != nil {
		return "", err
	}
	gitSecretName, err := fac.prepK8SSecretForGitCred(*wfData)
	if err != nil {
		return "", err
	}

	wfDef, err := fac.prepWorkflowDef(*wfData, credSecretName, gitSecretName)
	if err != nil {
		return "", err
	}

	wf, err := fac.cli.CreateWorkflow(wfDef)
	if err != nil {
		return "", err
	}
	return wf.Name, nil
}

func (fac TerraformFactory) prepWorkflowDef(wfData types.TerraformParam, credSecretName, gitSecretName string) (*wfv1.Workflow, error) {
	wfDef := fac.src.GetWorkflow("terraform.yml")
	if wfDef == nil {
		return nil, FailToLoadWorkflowDef{WfName: "terraform.yml"}
	}

	var wfParam = terraformParam{
		TransactionID:       fac.transaction,
		DeploymentID:        wfData.Deployment,
		TemplateType:        wfData.TemplateType,
		TemplateID:          wfData.TemplateID,
		TerraformStateKey:   wfData.TerraformStateKey,
		GitURL:              wfData.GitURL,
		GitBranch:           wfData.GitTrackedUpStream.Branch,
		GitTag:              wfData.GitTrackedUpStream.Tag,
		GitCommit:           wfData.GitTrackedUpStream.Commit,
		Path:                wfData.Path,
		Username:            wfData.Username,
		CloudCredSecretName: credSecretName,
		GitCredSecretName:   gitSecretName,
		AnsibleVars:         wfData.AnsibleVars,
		Provider:            fac.prov.String(),
		NatsURL:             fac.natsOut.URL,
		NatsClusterID:       fac.natsOut.ClusterID,
		NatsSubject:         common.EventsSubject,
	}

	err := fac.prepTFBackendBlock(&wfParam)
	if err != nil {
		return nil, err
	}

	wfDef, err = fac.injectParam(wfDef, wfParam)
	if err != nil {
		return nil, err
	}

	if wfDef.ObjectMeta.Labels == nil {
		wfDef.ObjectMeta.Labels = make(map[string]string)
	}
	wfDef.ObjectMeta.Labels["deployment"] = wfData.Deployment.String()
	wfDef.ObjectMeta.Labels["template"] = wfData.TemplateID.String()
	wfDef.ObjectMeta.Labels["template_type"] = string(wfData.TemplateType)
	// Note: We cannot store username as is, because of character limit, label value
	// does not allow special character like '@', so we have to hash it. We cannot
	// fit the entire hex encoded sha256 to label value due to character limit, so we
	// have to cut it short.
	//
	// To get the hash of a username, run the following in a shell, and enter the
	// username (ctrl-D to end input):
	// cat | tr -d '\n' | sha256sum | head -c 32 && echo ""
	usernameHash := sha256.New()
	usernameHash.Write([]byte(wfData.Username))
	usernameHashStr := hex.EncodeToString(usernameHash.Sum(nil))
	wfDef.ObjectMeta.Labels["username"] = "sha2-" + usernameHashStr[0:32]

	return wfDef, nil
}

// prepare the HCL(HashipCorp Config Lang) block for Terraform Backend
func (fac TerraformFactory) prepTFBackendBlock(wfParam *terraformParam) error {
	// setup user in the backend if not found
	if !fac.tfBackend.LookupUser(wfParam.Username) {
		err := fac.tfBackend.SetupUser(wfParam.Username)
		if err != nil {
			return err
		}
	}

	hclBlock, err := fac.tfBackend.GetHCLBackendBlock(wfParam.Username, wfParam.TerraformStateKey)
	if err != nil {
		return err
	}

	wfParam.AnsibleVars["TF_BACKEND"] = hclBlock

	return nil
}

func (fac TerraformFactory) parseWorkflowData(data map[string]interface{}) (*types.TerraformParam, error) {
	var param types.TerraformParam
	err := mapstructure.Decode(data, &param)
	if err != nil {
		return nil, err
	}
	if param.Deployment == "" {
		return nil, DataError{Field: "deployment_id"}
	}
	if param.TemplateID == "" {
		return nil, DataError{Field: "template_id"}
	}
	if param.TerraformStateKey == "" {
		return nil, DataError{Field: "tf_state_key"}
	}
	if param.GitURL == "" {
		return nil, DataError{Field: "git_url"}
	}
	// There must exist 1 among branch/tag/commit
	if param.GitTrackedUpStream.Branch == "" && param.GitTrackedUpStream.Tag == "" && param.GitTrackedUpStream.Commit == "" {
		return nil, DataError{Field: "commit"}
	}
	if pathExceedRoot(param.Path) {
		return nil, DataError{Field: "sub_path"}
	}
	if param.Username == "" {
		return nil, DataError{Field: "username"}
	}
	if len(param.AnsibleVars) == 0 {
		return nil, DataError{Field: "ansible_vars"}
	}
	if param.CloudCredID == "" {
		return nil, DataError{Field: "cloud_cred_id"}
	}
	if param.CloudCredentialBase64 == "" {
		return nil, DataError{Field: "cloud_cred"}
	}
	_, err = base64.StdEncoding.DecodeString(param.CloudCredentialBase64)
	if err != nil {
		return nil, err
	}
	// git credID and encoded git cred should be present or absent together
	if (param.GitCredID == "" && param.GitCredentialBase64 != "") || (param.GitCredID != "" && param.GitCredentialBase64 == "") {
		return nil, DataError{Field: "git_cred"}
	}

	return &param, nil
}

func (fac TerraformFactory) injectParam(wfDef *wfv1.Workflow, param terraformParam) (*wfv1.Workflow, error) {
	InjectWorkflowParameter(wfDef, "transaction", string(param.TransactionID))
	InjectWorkflowParameter(wfDef, "deployment-id", param.DeploymentID.String())
	InjectWorkflowParameter(wfDef, "template-type", string(param.TemplateType))
	InjectWorkflowParameter(wfDef, "template-id", param.TemplateID.String())
	InjectWorkflowParameter(wfDef, "tf-state-key", param.TerraformStateKey)
	InjectWorkflowParameter(wfDef, "git-url", param.GitURL)
	InjectWorkflowParameter(wfDef, "git-branch", param.GitBranch)
	InjectWorkflowParameter(wfDef, "git-tag", param.GitTag)
	InjectWorkflowParameter(wfDef, "git-commit", param.GitCommit)
	InjectWorkflowParameter(wfDef, "sub-path", param.Path)
	InjectWorkflowParameter(wfDef, "username", param.Username)
	InjectWorkflowParameter(wfDef, "k8s-openstack-secrets", param.CloudCredSecretName)
	InjectWorkflowParameter(wfDef, "k8s-git-secrets", param.GitCredSecretName)
	InjectWorkflowParameter(wfDef, "provider", param.Provider)
	InjectWorkflowParameter(wfDef, "NATS_URL", param.NatsURL)
	InjectWorkflowParameter(wfDef, "NATS_CLUSTER_ID", param.NatsClusterID)
	InjectWorkflowParameter(wfDef, "NATS_SUBJECT", param.NatsSubject)
	ansibleVarsYaml, err := yaml.Marshal(param.AnsibleVars)
	if err != nil {
		return nil, err
	}
	InjectWorkflowParameter(wfDef, "ansible-vars-yaml", string(ansibleVarsYaml))

	return wfDef, nil
}

// prepare K8S Secret for cloud credential in the Argo cluster for the workflow creation.
func (fac TerraformFactory) prepK8SSecretForCloudCred(wfData types.TerraformParam) (string, error) {

	metadata := ports.CloudCredMetadata{Provider: fac.prov, Username: wfData.Username, CredID: wfData.CloudCredID}
	cred, err := base64.StdEncoding.DecodeString(wfData.CloudCredentialBase64)
	if err != nil {
		return "", err
	}

	// FIXME need to find a better way to check credential type.
	// Ideally, credential type or provider type should be passed as part of the request to AWM.
	return fac.credStore.StoreEncryptedCred(metadata, cred, func(cred map[string]string) error {
		if isOpenStackCred(cred) {
			return validateOpenStackCred(cred)
		}
		return validateAWSCred(cred)
	})
}

// FIXME need to find a better way to check credential type
func isOpenStackCred(cred map[string]string) bool {
	for key := range cred {
		if strings.HasPrefix(key, "OS_") {
			return true
		}
	}
	return false
}

// prepare K8S Secret for Git credential in the Argo cluster.
func (fac TerraformFactory) prepK8SSecretForGitCred(wfData types.TerraformParam) (string, error) {

	// git cred is optional, provide an secret name that does not exists.
	if wfData.GitCredentialBase64 == "" {
		return emptySecretName, nil
	}

	metadata := ports.CloudCredMetadata{Provider: fac.prov, Username: wfData.Username, CredID: wfData.GitCredID}
	cred, err := base64.StdEncoding.DecodeString(wfData.GitCredentialBase64)
	if err != nil {
		return "", err
	}

	return fac.credStore.StoreEncryptedCred(metadata, cred, validateGitCred)
}

// validate OpenStackCred, this only perform a very basic validation.
func validateOpenStackCred(cred map[string]string) error {
	var openStackCred types.OpenStackCredential
	err := mapstructure.Decode(cred, &openStackCred)
	if err != nil {
		return err
	}
	if openStackCred.RegionName == "" {
		return DataError{Field: "OS_REGION_NAME"}
	}
	if openStackCred.AuthURL == "" {
		return DataError{Field: "OS_AUTH_URL"}
	}

	if openStackCred.AuthType == "v3applicationcredential" {
		if openStackCred.AppCredID == "" && openStackCred.AppCredName == "" {
			return DataError{Field: "OS_APPLICATION_CREDENTIAL_ID"}
		}
		if openStackCred.AppCredSecret == "" {
			return DataError{Field: "OS_APPLICATION_CREDENTIAL_SECRET"}
		}
	} else {
		// username&password auth
		if openStackCred.Username == "" {
			return DataError{Field: "OS_USERNAME"}
		}
		if openStackCred.Password == "" {
			return DataError{Field: "OS_PASSWORD"}
		}
	}

	return nil
}

func validateAWSCred(cred map[string]string) error {
	var awsCred types.AWSCredential
	err := mapstructure.Decode(cred, &awsCred)
	if err != nil {
		return err
	}
	if awsCred.AccessKey == "" {
		return DataError{Field: "AWS_ACCESS_KEY_ID"}
	}
	if awsCred.SecretKey == "" {
		return DataError{Field: "AWS_SECRET_ACCESS_KEY"}
	}
	return nil
}

func validateGitCred(cred map[string]string) error {
	var gitCred types.GitCredential
	err := mapstructure.Decode(cred, &gitCred)
	if err != nil {
		return err
	}
	if gitCred.Username == "" {
		return errors.New("username missing in git credential")
	}
	if gitCred.Password == "" {
		return errors.New("password missing in git credential")
	}
	return nil
}

// check if a path exceeds root
func pathExceedRoot(pathStr string) bool {
	if filepath.IsAbs(pathStr) {
		return true
	}
	prefix := path.Join("/", xid.New().String(), xid.New().String())
	cleaned := path.Clean(path.Join(prefix, pathStr))
	return !strings.HasPrefix(cleaned, prefix)
}
