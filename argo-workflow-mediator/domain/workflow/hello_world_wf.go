package workflow

import (
	wfv1 "github.com/argoproj/argo-workflows/v3/pkg/apis/workflow/v1alpha1"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/ports"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/types"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// HelloWorldWorkflow is a Argo workflow definition for a simple hello world
// workflow
var HelloWorldWorkflow = wfv1.Workflow{
	TypeMeta: metav1.TypeMeta{APIVersion: "argoproj.io/v1alpha1", Kind: "Workflow"},
	ObjectMeta: metav1.ObjectMeta{
		GenerateName: "hello-world-",
	},
	Spec: wfv1.WorkflowSpec{
		ServiceAccountName: "awm-argo",
		Entrypoint:         "whalesay",
		Templates: []wfv1.Template{
			{
				Name: "whalesay",
				Container: &corev1.Container{
					Image:   "docker/whalesay:latest",
					Command: []string{"cowsay"},
					Args:    []string{"hello world"},
				},
			},
		},
	},
}

// HelloWorldWorkflowFactory is used to create hello world workflow, this is
// useful to check if AWM is connected to Argo Workflow.
type HelloWorldWorkflowFactory struct {
	transaction common.TransactionID
	prov        types.Provider
	cli         ports.ArgoClient
}

// NewHelloWorldWorkflowFactory ...
func NewHelloWorldWorkflowFactory(
	transaction common.TransactionID,
	provider types.Provider,
	cli ports.ArgoClient,
	natsOut messaging2.NatsStanMsgConfig,
) (*HelloWorldWorkflowFactory, error) {
	return &HelloWorldWorkflowFactory{
		transaction: transaction,
		prov:        provider,
		cli:         cli,
	}, nil
}

// Provider ...
func (fac HelloWorldWorkflowFactory) Provider() types.Provider {
	return fac.prov
}

// Create ...
func (fac HelloWorldWorkflowFactory) Create(data map[string]interface{}) (string, error) {
	workflow, err := fac.cli.CreateWorkflow(&HelloWorldWorkflow)
	if err != nil {
		return "", err
	}
	return workflow.Name, nil
}
