package workflow

import (
	"encoding/base64"
	"encoding/json"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"testing"

	"github.com/mitchellh/mapstructure"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"

	wfv1 "github.com/argoproj/argo-workflows/v3/pkg/apis/workflow/v1alpha1"
	"github.com/rs/xid"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/adapters"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/adapters/mock"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/config"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/ports"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/types"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/utils"
)

var OpenStackCred = types.OpenStackCredential{
	IdentityAPIVersion: "3",
	RegionName:         "region",
	AuthURL:            "https://openstack.org",
	ProjectDomainID:    "project-domain",
	ProjectID:          "project-id-123",
	ProjectName:        "project-name",
	UserDomainName:     "user-domain",
	Username:           "username",
	Password:           "password",
}

var OpenStackAppCred = types.OpenStackCredential{
	IdentityAPIVersion: "3",
	AuthType:           "v3applicationcredential",
	AuthURL:            "https://openstack.org",
	AppCredID:          "app-cred-id",
	AppCredSecret:      "app-cred-secret",

	RegionName:      "region",
	ProjectDomainID: "123",
	ProjectID:       "123",
	ProjectName:     "project-name",
	UserDomainName:  "user-domain-name",
}

var GitCred = types.GitCredential{
	Username: "username",
	Password: "password",
}

// Test creation of Terraform Workflow with Cloud Credential
func TestCreateTerraformWorkflow(t *testing.T) {
	provider, cli, wfSrc, tfBackend, credStore := setup()
	tid := messaging.NewTransactionID()
	natsConf := messaging2.NatsStanMsgConfig{
		NatsConfig: messaging2.NatsConfig{
			URL:                  "example:4222",
			QueueGroup:           "queue-123",
			WildcardSubject:      "",
			ClientID:             "",
			MaxReconnects:        0,
			ReconnectWait:        0,
			RequestTimeout:       0,
			EventWildcardSubject: "subject-123",
			AckWaitSec:           0,
		},
		StanConfig: messaging2.StanConfig{
			ClusterID:     "cluster-123",
			DurableName:   "durable-123",
			EventsTimeout: 0,
		},
	}
	fac, err := NewTerraformFactory(tid, provider, cli, wfSrc, tfBackend, credStore, natsConf)
	assert.NoError(t, err)

	wfData, wfDataMap := setupWorkflowDataWithCred(credStore.Key)

	wfName, err := fac.Create(wfDataMap)
	assert.NoError(t, err)
	assert.NotEmpty(t, wfName, "workflow name is empty")

	_, err = cli.WorkflowStatus(wfName)
	assert.NoError(t, err)

	params, err := getWorkflowParam(cli, wfName)
	assert.NoError(t, err)

	// check if there is no unknown fields in workflow parameters
	paramFieldNames := wfParamFields()
	for _, param := range params {
		_, ok := paramFieldNames[param.Name]
		assert.Truef(t, ok, "unknown field %s in the workflow parameters", param.Name)
		paramFieldNames[param.Name] = true // mark field as set
	}

	// check if all the required field are set in the workflow parameters
	for field, fieldSet := range paramFieldNames {
		assert.Truef(t, fieldSet, "field %s is missing from workflow parameters", field)
	}

	cloudCredMeta := ports.CloudCredMetadata{Provider: provider, Username: wfDataMap["username"].(string), CredID: wfData.CloudCredID}
	gitCredMeta := ports.CloudCredMetadata{Provider: provider, Username: wfDataMap["username"].(string), CredID: wfData.GitCredID}

	for _, param := range params {
		if !assert.NotNil(t, param.Value) {
			return
		}
		switch param.Name {
		case "transaction":
			assert.Equal(t, string(tid), param.Value.String())
		case "deployment-id":
			assert.Equal(t, wfData.Deployment.String(), param.Value.String())
		case "template-id":
			assert.Equal(t, wfData.TemplateID.String(), param.Value.String())
		case "tf-state-key":
			assert.Equal(t, wfData.TerraformStateKey, param.Value.String())
		case "git-url":
			assert.Equal(t, wfData.GitURL, param.Value.String())
		case "git-branch":
			assert.Equal(t, wfData.GitTrackedUpStream.Branch, param.Value.String())
		case "git-tag":
			assert.Equal(t, wfData.GitTrackedUpStream.Tag, param.Value.String())
		case "git-commit":
			assert.Equal(t, wfData.GitTrackedUpStream.Commit, param.Value.String())
		case "sub-path":
			assert.Equal(t, wfData.Path, param.Value.String())
		case "username":
			assert.Equal(t, wfData.Username, param.Value.String())
		case "k8s-openstack-secrets":
			assert.Equal(t, adapters.CredK8SSecretName(cloudCredMeta), param.Value.String())
		case "k8s-git-secrets":
			assert.Equal(t, adapters.CredK8SSecretName(gitCredMeta), param.Value.String())
		case "ansible-vars-yaml":
			assert.NotEmpty(t, param.Value.String())
		case "provider":
			assert.Equal(t, string(provider), param.Value.String())
		case "NATS_URL":
			assert.Equal(t, natsConf.NatsConfig.URL, param.Value.String())
		case "NATS_CLUSTER_ID":
			assert.Equal(t, natsConf.ClusterID, param.Value.String())
		case "NATS_SUBJECT":
			assert.Equal(t, common.EventsSubject, param.Value.String())
		}
	}

	// 2 secret, 1 cloud cred, 1 git cred
	assert.Equal(t, 2, credStore.Count())
	cloudCredSecretNameMatched := false
	gitCredSecretNameMatched := false
	for _, secretName := range credStore.List() {
		if secretName == adapters.CredK8SSecretName(cloudCredMeta) {
			cloudCredSecretNameMatched = true
			continue
		} else if secretName == adapters.CredK8SSecretName(gitCredMeta) {
			gitCredSecretNameMatched = true
			continue
		} else {
			assert.Fail(t, "secret name not for cloud cred nor for git cred")
		}
	}
	assert.True(t, cloudCredSecretNameMatched)
	assert.True(t, gitCredSecretNameMatched)
}

func TestCreateTerraformWorkflowMissingData(t *testing.T) {

	// fields that are required, must not be empty or nil
	fieldNames := []string{"deployment_id", "template_id", "tf_state_key", "git_url", "username", "cloud_cred_id", "cloud_cred", "ansible_vars"}

	for _, missingDataField := range fieldNames {
		t.Run(missingDataField, func(t *testing.T) {
			provider, cli, wfSrc, tfBackend, credStore := setup()

			fac, err := NewTerraformFactory(messaging.NewTransactionID(), provider, cli, wfSrc, tfBackend, credStore, messaging2.NatsStanMsgConfig{})
			assert.NoError(t, err)

			wfData := setupWorkflowData()
			delete(wfData, missingDataField)

			_, err = fac.Create(wfData)
			assert.Error(t, err, "Should return error")
			e := err.(DataError)
			assert.IsType(t, DataError{}, err, "Wrong error type")
			assert.Equal(t, missingDataField, e.Field, "Inconsistent data field")
		})

	}
}

func setup() (types.Provider, ports.ArgoClient, ports.WorkflowDefSrc, ports.TFBackend, *mock.K8SCredSecretStore) {
	provider := types.Provider("provider-" + xid.New().String())
	cli := mock.NewMockArgoClient()
	wfSrc := mock.NewMockWorkflowDefSrc()
	wfSrc.Register("terraform.yml", &HelloWorldWorkflow)
	tfBackend := &mock.TFMockBackend{}
	tfBackend.Init(config.TFBackendConfig{})
	credStore := mock.K8SCredSecretStore{}
	credStore.Init(config.K8SConfig{})
	return provider, cli, wfSrc, tfBackend, &credStore
}

func setupWorkflowData() map[string]interface{} {
	wfData := types.TerraformParam{
		Username:          "test_user1",
		Deployment:        common.NewID("deployment"),
		TemplateID:        common.NewID("template"),
		TerraformStateKey: xid.New().String(),
		GitURL:            "https://gitlab.com/cyverse/foobar",
		GitTrackedUpStream: struct {
			Branch string `mapstructure:"branch"`
			Tag    string `mapstructure:"tag"`
			Commit string `mapstructure:"commit"`
		}{
			Branch: "master",
			Tag:    "",
			Commit: "",
		},
		Path:                  "modules/foobar",
		CloudCredID:           "secret1",
		AnsibleVars:           map[string]interface{}{"key": "value"},
		CloudCredentialBase64: "AAAAAAAAA",
		GitCredID:             "secret2",
		GitCredentialBase64:   "AAAAAAAAA",
	}
	var wfDataMap map[string]interface{}
	err := mapstructure.Decode(wfData, &wfDataMap)
	if err != nil {
		// must not fail
		panic(err)
	}
	return wfDataMap
}

func setupWorkflowDataWithCred(aesKey []byte) (types.TerraformParam, map[string]interface{}) {
	wfData := types.TerraformParam{
		Username:          "test_user1",
		Deployment:        common.NewID("deployment"),
		TemplateID:        common.NewID("template"),
		TerraformStateKey: xid.New().String(),
		GitURL:            "https://gitlab.com/cyverse/foobar",
		GitTrackedUpStream: struct {
			Branch string `mapstructure:"branch"`
			Tag    string `mapstructure:"tag"`
			Commit string `mapstructure:"commit"`
		}{
			Branch: "master",
			Tag:    "",
			Commit: "",
		},
		Path:                  "modules/foobar",
		CloudCredID:           "secret1",
		AnsibleVars:           map[string]interface{}{"key": "value"},
		CloudCredentialBase64: credToBase64(OpenStackCred, aesKey),
		GitCredID:             "secret2",
		GitCredentialBase64:   credToBase64(GitCred, aesKey),
	}
	var wfDataMap map[string]interface{}
	err := mapstructure.Decode(wfData, &wfDataMap)
	if err != nil {
		// must not fail
		panic(err)
	}
	return wfData, wfDataMap
}

func getWorkflowParam(cli ports.ArgoClient, wfName string) ([]wfv1.Parameter, error) {
	argoCli := cli.(mock.ArgoClient)

	wf, err := argoCli.GetWorkflow(wfName)
	if err != nil {
		return nil, err
	}

	return wf.Spec.Arguments.Parameters, nil
}

// returns all possible fields that can appear in the workflow parameter argument (spec.arguments.parameters)
func wfParamFields() map[string]bool {
	fields := make(map[string]bool)
	fields["transaction"] = false
	fields["deployment-id"] = false
	fields["template-id"] = false
	fields["template-type"] = false
	fields["tf-state-key"] = false
	fields["git-url"] = false
	fields["git-branch"] = false
	fields["git-tag"] = false
	fields["git-commit"] = false
	fields["sub-path"] = false
	fields["username"] = false
	fields["k8s-openstack-secrets"] = false
	fields["k8s-git-secrets"] = false
	fields["ansible-vars-yaml"] = false
	fields["provider"] = false
	fields["NATS_URL"] = false
	fields["NATS_CLUSTER_ID"] = false
	fields["NATS_SUBJECT"] = false
	return fields
}

// Workflow Data without Credential
func TestParseTerraformWorkflowDataWithoutCred(t *testing.T) {
	fac := TerraformFactory{}

	wfData := make(map[string]interface{})
	id := xid.New()
	wfData["template_id"] = id.String()
	wfData["module_name"] = "openstack-module"
	wfData["username"] = "test_user1"
	wfData["cloud_cred_id"] = "secret1"
	ansibleVars := make(map[string]string)
	ansibleVars["key"] = "value"
	wfData["ansible_vars"] = ansibleVars
	wfData["cloud_cred"] = ""

	_, err := fac.parseWorkflowData(wfData)
	assert.Error(t, err, "should return error")

}

func TestParseTerraformWorkflowData(t *testing.T) {
	fac := TerraformFactory{}

	wfData := make(map[string]interface{})
	id := xid.New()
	wfData["deployment_id"] = id.String()
	wfData["template_id"] = id.String()
	wfData["tf_state_key"] = id.String()
	wfData["git_url"] = "https://gitlab.com/cyverse/foobar"
	wfData["git_upstream"] = map[string]string{
		"branch": "master",
	}
	wfData["username"] = "test_user1"
	wfData["cloud_cred_id"] = "secret1"
	ansibleVars := make(map[string]string)
	ansibleVars["key"] = "value"
	wfData["ansible_vars"] = ansibleVars
	wfData["cloud_cred"] = credToBase64(OpenStackCred, utils.GenerateAES256Key())

	_, err := fac.parseWorkflowData(wfData)
	if err != nil {
		assert.NoError(t, err)
	}

}

// Workflow Data with Application Credential
func TestParseTerraformWorkflowDataWithAppCred(t *testing.T) {
	fac := TerraformFactory{}

	wfData := make(map[string]interface{})
	id := xid.New()
	wfData["deployment_id"] = id.String()
	wfData["template_id"] = id.String()
	wfData["tf_state_key"] = id.String()
	wfData["git_url"] = "https://gitlab.com/cyverse/foobar"
	wfData["git_upstream"] = map[string]string{"branch": "master"}
	wfData["username"] = "test_user1"
	wfData["cloud_cred_id"] = "secret1"
	ansibleVars := make(map[string]string)
	ansibleVars["key"] = "value"
	wfData["ansible_vars"] = ansibleVars
	wfData["cloud_cred"] = credToBase64(OpenStackAppCred, utils.GenerateAES256Key())

	_, err := fac.parseWorkflowData(wfData)
	if err != nil {
		assert.NoError(t, err)
	}

}

func credToBase64(cred interface{}, key []byte) string {
	credJSONStr, err := json.Marshal(cred)
	if err != nil {
		panic(err)
	}

	credCiphertext, err := utils.AESEncrypt(key, credJSONStr)
	if err != nil {
		panic(err)
	}
	return base64.StdEncoding.EncodeToString(credCiphertext)
}
