// Package workflow ...
// Factory for Mock Workflow
package workflow

import (
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/ports"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/types"
)

// MockFactory is a mock-up Workflow Factory
type MockFactory struct {
	prov types.Provider
	cli  ports.ArgoClient
	src  ports.WorkflowDefSrc
}

// NewMockFactory creates a MockFactory
func NewMockFactory(provider types.Provider, cli ports.ArgoClient, wfSrc ports.WorkflowDefSrc) (*MockFactory, error) {
	fac := MockFactory{prov: provider, cli: cli, src: wfSrc}
	return &fac, nil
}

// Provider returns the provider that this factory operates on
func (fac MockFactory) Provider() types.Provider {
	return fac.prov
}

// Create creates a workflow with given data, returns the workflow name, and possible error
func (fac MockFactory) Create(data map[string]interface{}) (string, error) {
	wf, err := fac.cli.CreateWorkflow(&HelloWorldWorkflow)
	if err != nil {
		return "", err
	}
	return wf.Name, nil
}
