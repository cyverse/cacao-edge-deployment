package types

import (
	"gitlab.com/cyverse/cacao-common/common"
	cacaosvc "gitlab.com/cyverse/cacao-common/service"
)

// Event is a common interface for Events
type Event interface {
	EventType() EventType
	Transaction() common.TransactionID
}

// QueryOp is a type to represent query operations
type QueryOp string

// EventType is a type to represent event operations
type EventType = common.EventType

// EventID is the ID of events, this is used to track transaction
type EventID = common.EventID

// Provider is the ID of cloud provider
type Provider cacaosvc.AWMProvider

func (p Provider) String() string {
	return string(p)
}

// EventErrorType is the error type that appear in the failure/error event emitted
type EventErrorType string
