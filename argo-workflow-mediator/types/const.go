package types

// CloudEventSourcePrefix is prefix for source field in cloudevent
const CloudEventSourcePrefix string = "https://gitlab.com/cyverse/argo-workflow-mediator/"

// EventTypePrefix is the prefix for EventType
const EventTypePrefix = "org.cyverse.events."

// WorkflowCreateRequestedEvent is the type name of WorkflowCreate request
const WorkflowCreateRequestedEvent EventType = EventTypePrefix + "WorkflowCreateRequested"

// WorkflowCreatedEvent is the cloudevent name of WorkflowCreated defined below
const WorkflowCreatedEvent EventType = EventTypePrefix + "WorkflowCreated"

// WorkflowCreateFailedEvent is the event name of WorkflowCreateFailed
const WorkflowCreateFailedEvent EventType = EventTypePrefix + "WorkflowCreateFailed"

// WorkflowTerminateRequestedEvent is the type name of WorkflowTerminate request
const WorkflowTerminateRequestedEvent EventType = EventTypePrefix + "WorkflowTerminateRequested"

// WorkflowTerminatedEvent is the name of the WorkflowTerminated
const WorkflowTerminatedEvent EventType = EventTypePrefix + "WorkflowTerminated"

// WorkflowTerminateFailedEvent is the name of the WorkflowTerminateFailed
const WorkflowTerminateFailedEvent EventType = EventTypePrefix + "WorkflowTerminateFailed"

// WorkflowResubmitRequestedEvent is the type name of WorkflowResubmit request
const WorkflowResubmitRequestedEvent EventType = EventTypePrefix + "WorkflowResubmitRequested"

// WorkflowResubmittedEvent is the name of the WorkflowResubmitted event
const WorkflowResubmittedEvent EventType = EventTypePrefix + "WorkflowResubmitted"

// WorkflowResubmitFailedEvent is the name of the WorkflowResubmitFailed event
const WorkflowResubmitFailedEvent EventType = EventTypePrefix + "WorkflowResubmitFailed"

// FailToCreateWorkflow is error type that could appear in failure events WorkflowCreateFailedEvent
const FailToCreateWorkflow EventErrorType = "FailToCreateWorkflow"

// WorkflowNameEmpty is error type that could appear in failure events emitted
const WorkflowNameEmpty EventErrorType = "WorkflowNameEmpty"

// ProviderNotSupported is error type that could appear in failure events emitted
const ProviderNotSupported EventErrorType = "ProviderNotSupported"

// FailToResubmit is error type that could appear in failure events emitted relate to WorkflowResubmitFailedEvent
const FailToResubmit EventErrorType = "FailToResubmit"

// FailToTerminate is error type that could appear in failure events WorkflowTerminateFailedEvent
const FailToTerminate EventErrorType = "FailToTerminate"

// FailToRetrieveStatus is error type that could appear in failure events emitted
const FailToRetrieveStatus EventErrorType = "FailToRetrieveStatus"
