package main

import (
	"context"
	"fmt"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"io"

	"github.com/kelseyhightower/envconfig"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/adapters"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/config"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/domain"
)

var envConf config.EnvConfig
var configFile *config.Config

func init() {
	// env var config
	err := envconfig.Process("", &envConf)
	if err != nil {
		log.Fatalf("load env config failed, %v", err)
	}
	envConf.Override()

	logLevel, err := log.ParseLevel(envConf.LogLevel)
	if err != nil {
		log.Fatalf("fail to parse log level, %v", err)
	}
	log.SetLevel(logLevel)

	// config file
	configFile, err = config.LoadConfigFromFile(envConf.ConfigPath)
	if err != nil {
		log.Fatalf("fail to load config file, %v", err)
	}
	log.Info(fmt.Sprintf("Config loaded from %s", envConf.ConfigPath))
}

func main() {
	stanConn, err := messaging2.NewStanConnectionFromConfigWithoutEventSource(envConf.NatsStanMsgConfig)
	if err != nil {
		log.WithError(err).Fatal()
		return
	}
	defer logCloserError(&stanConn)
	svc := createDomain(configFile, &stanConn)

	err = svc.Init(configFile, envConf)
	if err != nil {
		log.WithError(err).Panic("fail to init service")
	}

	serviceCtx, cancelFunc := context.WithCancel(context.Background())
	defer cancelFunc()
	common.CancelWhenSignaled(cancelFunc)
	svc.Start(serviceCtx)
}

func createDomain(conf *config.Config, eventConn messaging2.EventConnection) domain.Domain {
	svc := domain.NewDomain()
	svc.EventWorkerCount = envConf.EventWorkerCount
	svc.EventChannelBufferCapacity = envConf.EventChannelBufferCapacity
	eventAdapter := adapters.NewStanEventAdapter(eventConn)
	svc.EventSrc = eventAdapter
	svc.ArgoCli = adapters.NewArgoClient()
	svc.WfSrc = adapters.NewWorkflowDefFile()
	svc.CredStore = adapters.NewK8SCredSecretStore(envConf)
	switch conf.TFBackend.TFBackendType {
	case "":
		log.Info("use Terraform Postgres backend")
		svc.TfBackend = adapters.NewTFPostgresBackend()
	case config.TFPGBackend:
		log.Info("use Terraform Postgres backend")
		svc.TfBackend = adapters.NewTFPostgresBackend()
	case config.TFHTTPBackend:
		log.Info("use Terraform HTTP backend")
		svc.TfBackend = adapters.NewTFHTTPBackend()
	default:
		log.Fatal("bad terraform backend type")
	}
	return svc
}

func logCloserError(closer io.Closer) {
	err := closer.Close()
	if err != nil {
		log.WithError(err).Error("fail to close")
		return
	}
}
