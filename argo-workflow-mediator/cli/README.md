# Command Line Utility to publish Message to NATS Streaming

## Build
```bash
go build
```

## Generate Encryption key (base64 encoded)
```bash
./cli key-gen
```

## Encrypt credential with random key
```bash
./cli cred encrypt cred.json --random
```

## Inject encrypted credential into `WorkflowCreateRequested` msg file
> Note: only works for `terraform` workflow and OpenStack credential
- create credential json file
```bash
cp example_cred.json cred.json
# edit cred.json
```

- create msg json file `create.json`
```bash
cp example_create.json create.json
# edit create.json
```

- encrypt credential and inject encrypted credential into `create.json`
```bash
# random key
./cli cred inject cred.json create.json --random
# specify key
./cli cred inject cred.json create.json <ENCRYPTION-KEY>
```

## Publish NATS Streaming message
- get ip of nats pod
```bash
kubectl get pod -n $AWM_NAMESPACE nats-0 -o=jsonpath="{.status.hostIP}"
```

- create `config.yml`
```bash
cp example_config.yml config.yml
# edit config.yml
```

- create msg json file from example json
```bash
cp example_resubmit.json resubmit.json
# edit resubmit.json
```
| example msg | description |
| --- | --- |
| `example_create.json` | create a terraform workflow |
| `example_create_destroy.json` | create a terraform workflow that destroy the resources (based on terraform state) |
| `example_resubmit.json` | resubmit a workflow |
| `example_terminate.json` | terminate a workflow |

- publish
```bash
./cli pub resubmit.json
```

## Publish message with `stan-pub` from [nats-box image](https://hub.docker.com/r/synadia/nats-box)
Alternatively one can also use utility from `nats-box` image to interact with NATS/NATS Streaming.

- publish message with `nats-box` (example msg `example_create.json`, `example_resubmit.json`, `example_terminate.json`)
Get IP of NATS pod
```bash
kubectl get pod -n $AWM_NAMESPACE nats-0 -o=jsonpath="{.status.hostIP}"
```
Start nats-box
```bash
kubectl run -n $AWM_NAMESPACE -i --rm --tty nats-box --image=synadia/nats-box --restart=Never
```
Use `stan-pub` inside `nats-box`
```bash
export NATS_URL=<NATS_IP>:4222 # uses IP of NATS pod from above
export SUBJECT=cyverse.awm.Cmd
stan-pub -s $NATS_URL -c stan $SUBJECT '<msg-in-json>'
```
