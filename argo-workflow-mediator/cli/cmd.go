package main

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"time"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	"github.com/kelseyhightower/envconfig"
	"github.com/mitchellh/mapstructure"
	"github.com/nats-io/stan.go"
	"github.com/rs/xid"
	"github.com/spf13/cobra"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/types"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/utils"
	"gopkg.in/yaml.v3"
)

var (
	rootCmd = &cobra.Command{
		Use:   "cli",
		Short: "CLI utility",
		Long:  `CLI utility for interacting with Argo Workflow Mediator`,
	}

	configFilename        string
	useRandomKey          bool
	validateBeforePublish bool

	keyCmd = &cobra.Command{
		Use:   "key-gen",
		Short: "Generate Encryption Key",
		Long:  `Generate AES-256 encryption key encoded in base64`,
		Run:   KeyGeneration,
	}
	helloWorldWorkflowCmd = &cobra.Command{
		Use:     "helloworld",
		Aliases: []string{"hello", "hw"},
		Short:   "Create a hello world workflow",
		Long:    `Create a hello world workflow`,
		RunE:    createHelloWorldWorkflow,
	}
	credCmd = &cobra.Command{
		Use:   "cred",
		Short: "Manipulate Credential",
		Long:  `Manipulate Credential`,
	}
	credEncryptCmd = &cobra.Command{
		Use:   "encrypt [cred-file] [key]",
		Short: "Encrypt Credential and print it out",
		Long:  `Encrypt Credential with given key (base64 encoded) and print out the base64 encoded key and ciphertext`,
		RunE:  EncryptCred,
	}
	credInjectCmd = &cobra.Command{
		Use:   "inject [cred-file] [msg-file] [key]",
		Short: "Encrypt Credential and inject into a WorkflowCreateRequested msg file",
		Long:  `Encrypt Credential with given key (base64 encoded) and inject into a WorkflowCreateRequested msg json file`,
		RunE:  InjectEncryptCred,
	}
	publishCmd = &cobra.Command{
		Use:   "pub [msg-file]",
		Short: "Publish to NATS Streaming",
		Long:  `Read message from a json file and publish to NATS Streaming`,
		RunE:  PublishMsg,
	}
	subscribeCmd = &cobra.Command{
		Use:   "sub",
		Short: "Subscribe to NATS Streaming",
		Long:  `Subscribe to NATS Streaming`,
	}
)

func initCmd() {
	publishCmd.PersistentFlags().StringVar(&configFilename, "config", "", "config yaml file, default to \"config.yml\"")
	publishCmd.PersistentFlags().BoolVar(&validateBeforePublish, "validate", false, "check if message is valid json and cloudevent before publish")
	credEncryptCmd.PersistentFlags().BoolVar(&useRandomKey, "random", false, "encrypt with random key")
	credInjectCmd.PersistentFlags().BoolVar(&useRandomKey, "random", false, "encrypt with random key")

	credCmd.AddCommand(credEncryptCmd, credInjectCmd)
	rootCmd.AddCommand(credCmd, publishCmd, keyCmd, helloWorldWorkflowCmd)
}

func initDefaultValue() {
	configFilename = "config.yml"
}

// Config is configurations for NATS Streaming
type Config struct {
	NatsAddr  string `yaml:"nats_addr"`
	ClusterID string `yaml:"cluster_id"`
	ClientID  string `yaml:"client_id"`
	Subject   string `yaml:"subject"`
}

// PublishMsg publis a msg to NATS
func PublishMsg(cmd *cobra.Command, args []string) error {
	if configFilename == "" {
		configFilename = "config.yml"
	}
	if len(args) < 1 {
		return fmt.Errorf("[msg-file] missing")
	}
	msgFilename := args[0]

	conf := loadConfig(configFilename)
	msg := readMsgFile(msgFilename, validateBeforePublish)
	publish(conf, msg)
	return nil
}

// load config from yaml file
func loadConfig(filename string) Config {
	var conf Config

	dat, err := os.ReadFile(filename)
	if err != nil {
		log.Fatal(err)
	}

	err = yaml.Unmarshal(dat, &conf)
	if err != nil {
		log.Fatal(err)
	}

	return conf
}

// read msg from json file
func readMsgFile(filename string, validate bool) []byte {
	dat, err := os.ReadFile(filename)
	if err != nil {
		log.Fatal(err)
	}
	if !validate {
		return dat
	}

	var event cloudevents.Event
	err = json.Unmarshal(dat, &event)
	if err != nil {
		log.Fatal(err)
	}

	// generate event ID
	event.SetID(xid.New().String())

	dat, err = event.MarshalJSON()
	if err != nil {
		log.Fatal(err)
	}
	return dat
}

// publish msg to NATS Streaming
func publish(conf Config, msg []byte) {

	t := time.Now()
	clientID := fmt.Sprintf("%s-%d", conf.ClientID, t.Unix())

	sc, err := stan.Connect(
		conf.ClusterID,
		clientID,
		stan.NatsURL(conf.NatsAddr),
		stan.ConnectWait(5*time.Second),
	)
	if err != nil {
		log.Fatal(err)
	}

	// Simple Synchronous Publisher
	err = sc.Publish(conf.Subject, msg)
	if err != nil {
		log.Fatal(err)
	}

}

// EncryptCred encrypt credential json file and prints it out
func EncryptCred(cmd *cobra.Command, args []string) error {
	var key []byte
	var err error
	if len(args) < 1 {
		return fmt.Errorf("[cred-file] missing")
	}
	if !useRandomKey {
		if len(args) < 2 {
			return fmt.Errorf("[key] missing")
		}
		if args[1] == "" {
			return fmt.Errorf("[key] empty")
		}
		key, err = base64.StdEncoding.DecodeString(args[1])
		if err != nil {
			return fmt.Errorf("key is not valid base64, %v", err)
		}
	} else {
		key = utils.GenerateAES256Key()
	}
	credFilename := args[0]

	encoded := EncryptFile(credFilename, key)
	fmt.Println("key:")
	fmt.Println(base64.StdEncoding.EncodeToString(key))
	fmt.Println("credential:")
	fmt.Println(encoded)
	return nil
}

// InjectEncryptCred encrypt credential json file and inject it into a msg file
func InjectEncryptCred(cmd *cobra.Command, args []string) error {
	var key []byte
	var err error
	if len(args) < 1 {
		return fmt.Errorf("[msg-file] missing")
	} else if len(args) < 2 {
		return fmt.Errorf("[cred-file] missing")
	}
	if !useRandomKey {
		if len(args) < 3 {
			return fmt.Errorf("[key] missing")
		}
		if args[2] == "" {
			return fmt.Errorf("[key] empty")
		}
		key, err = base64.StdEncoding.DecodeString(args[2])
		if err != nil {
			return fmt.Errorf("key is not valid base64, %v", err)
		}
	} else {
		key = utils.GenerateAES256Key()
	}

	credFilename := args[0]
	msgFilename := args[1]

	encoded := EncryptFile(credFilename, key)
	fmt.Println("key:")
	fmt.Println(base64.StdEncoding.EncodeToString(key))

	err = injectCred(msgFilename, encoded)
	if err != nil {
		return err
	}

	return nil
}

func injectCred(msgFilename string, credBase64 string) error {
	dat, err := os.ReadFile(msgFilename)
	if err != nil {
		return err
	}

	var event cloudevents.Event
	var msgCreate types.WorkflowCreate

	// decode
	err = event.UnmarshalJSON(dat)
	if err != nil {
		return err
	}
	err = event.DataAs(&msgCreate)
	if err != nil {
		return err
	}
	var wfData types.TerraformParam
	err = mapstructure.Decode(msgCreate.WfDat, &wfData)
	if err != nil {
		return err
	}

	wfData.CloudCredentialBase64 = credBase64

	// encode
	err = mapstructure.Decode(wfData, &msgCreate.WfDat)
	if err != nil {
		return err
	}
	err = event.SetData(cloudevents.ApplicationJSON, msgCreate)
	if err != nil {
		return err
	}
	result, err := event.MarshalJSON()
	if err != nil {
		return err
	}
	err = os.WriteFile(msgFilename, result, os.ModeExclusive)
	if err != nil {
		return err
	}

	return nil
}

// EncryptFile reads the content of a file and return the encrypted version encoded in base64
func EncryptFile(filename string, key []byte) string {
	dat, err := os.ReadFile(filename)
	if err != nil {
		log.Fatal(err)
	}
	cipher, err := utils.AESEncrypt(key, dat)
	if err != nil {
		log.Fatal(err)
	}
	return base64.StdEncoding.EncodeToString(cipher)
}

// KeyGeneration generate a random AES-256 key encoded in base64
func KeyGeneration(cmd *cobra.Command, args []string) {
	key := utils.GenerateAES256Key()
	fmt.Println(base64.StdEncoding.EncodeToString(key))
}

func createHelloWorldWorkflow(cmd *cobra.Command, args []string) error {
	username := "testuser123"
	event := types.WorkflowCreate{
		EventID:          types.EventID(common.NewID("event").String()),
		TransactionID:    messaging2.NewTransactionID(),
		Provider:         types.Provider(common.NewID("provider").String()),
		Username:         &username,
		WorkflowFilename: "helloworld.yaml",
		WfDat:            nil,
	}
	ce, err := messaging2.CreateCloudEventWithAutoSource(event, event.EventType())
	if err != nil {
		return err
	}
	var conf messaging2.NatsStanMsgConfig
	err = envconfig.Process("", &conf)
	if err != nil {
		return err
	}
	conf.ClientID += "-cli"
	stanConn, err := conf.ConnectStan()
	if err != nil {
		return err
	}
	defer stanConn.Close()
	err = stanConn.Publish(ce)
	if err != nil {
		return err
	}
	return nil
}
