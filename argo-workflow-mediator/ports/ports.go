package ports

import (
	"context"
	"fmt"
	"sync"

	wfv1 "github.com/argoproj/argo-workflows/v3/pkg/apis/workflow/v1alpha1"
	cloudevents "github.com/cloudevents/sdk-go/v2"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/config"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/types"
	corev1 "k8s.io/api/core/v1"
)

// ArgoClient is a client interface for manipulating Argo Workflow
type ArgoClient interface {
	Init(conf config.ArgoConfig)
	Context() *ArgoContext
	CreateWorkflow(wfDef *wfv1.Workflow) (*wfv1.Workflow, error)
	TerminateWorkflow(wfName string) error
	DeleteWorkflow(wfName string) error
	ResubmitWorkflow(wfName string) (*wfv1.Workflow, error)
	WorkflowStatus(wfName string) (*wfv1.WorkflowStatus, error)
}

// ArgoContext is the context of which workflows exists in
type ArgoContext struct {
	ServerURL string
	Secure    bool
	SSLVerify bool
	Namespace string
	Token     string
}

// CreateArgoContextFromConfig creates a context from Config
func CreateArgoContextFromConfig(conf *config.ArgoConfig) *ArgoContext {

	if conf == nil {
		return nil
	}

	var url string
	if conf.Port == 443 {
		url = conf.Host
	} else {
		url = fmt.Sprintf("%s:%d", conf.Host, conf.Port)
	}
	return &ArgoContext{
		ServerURL: url,
		Secure:    conf.SSL,
		SSLVerify: conf.SSLVerify,
		Namespace: conf.Namespace,
		Token:     conf.Token,
	}
}

// K8SCredSecretStore stores cloud credential as K8S secrets
type K8SCredSecretStore interface {
	Init(config.K8SConfig) error
	StoreEncryptedCred(CloudCredMetadata, []byte, CredValidator) (string, error)
	Get(metadata CloudCredMetadata, result interface{}) error
	Delete(CloudCredMetadata) error
}

// CloudCredMetadata is the metadata for cloud credential.
type CloudCredMetadata struct {
	// the cloud provider
	types.Provider
	// owner of the credential
	Username string
	// ID of the credential
	CredID string
}

// CredValidator is a function that validates credential
type CredValidator func(cred map[string]string) error

// EventSrc is an adapter interface for incoming events
type EventSrc interface {
	SetHandlers(handlers EventHandlers)
	Start(ctx context.Context, wg *sync.WaitGroup) error
}

// EventPort is an interface for both incoming and outgoing events
type EventPort interface {
	EventSrc
}

// EventHandlers ...
type EventHandlers interface {
	WorkflowCreateHandler(event types.WorkflowCreate, sink OutgoingEvents)
	WorkflowTerminateHandler(event types.WorkflowTerminate, sink OutgoingEvents)
	WorkflowResubmitHandler(event types.WorkflowResubmit, sink OutgoingEvents)
}

// OutgoingEvents ...
type OutgoingEvents interface {
	WorkflowCreated(types.WorkflowCreated)
	WorkflowCreateFailed(types.WorkflowCreateFailed)
	WorkflowResubmitted(types.WorkflowResubmitted)
	WorkflowResubmitFailed(types.WorkflowResubmitFailed)
	WorkflowTerminated(types.WorkflowTerminated)
	WorkflowTerminateFailed(types.WorkflowTerminateFailed)
}

// MsgHandler ...
type MsgHandler func(*cloudevents.Event) error

// K8SSecretClient is client for K8S Secret
type K8SSecretClient interface {
	Init(config.K8SConfig) error
	Create(corev1.Secret) error
	Get(name string) (*corev1.Secret, error)
	Update(secret corev1.Secret) error
	Delete(name string) error
}

// TFState is Terraform State of a template
type TFState interface {
	ToStr() string
}

// TFBackendCredential is the credential to connect to a Terraform backend
type TFBackendCredential interface {
}

// TFStateKey is key/identifier to fetch Terraform state from backend
type TFStateKey struct {
	DeploymentID string
}

// String ...
func (k TFStateKey) String() string {
	return k.DeploymentID
}

// TFBackend is Terraform backend
type TFBackend interface {
	Init(config.TFBackendConfig) error
	LookupUser(username string) bool
	SetupUser(username string) error
	GetUserBackendCredential(username string) (TFBackendCredential, error)
	GetHCLBackendBlock(username, workspace string) (string, error)
	GetState(username string, key TFStateKey) (TFState, error)
	PushState(username string, key TFStateKey, state TFState) error
}

// WorkflowDefSrc is a source of Workflow Definition
type WorkflowDefSrc interface {
	Init(config.WorkflowConfig) error
	GetWorkflow(wfName string) *wfv1.Workflow
}
