package mock

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/config"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/ports"
)

// TFState is Terraform state
type TFState string

// ToStr return the TFState as string
func (tfs TFState) ToStr() string {
	return string(tfs)
}

// TFMockBackendCreden is the credential to the mock-up Terraform Backend
type TFMockBackendCreden struct {
	Password string
}

// TFMockBackend is a mock-up Terraform Backend
type TFMockBackend struct {
	creden map[string]TFMockBackendCreden
}

// Init initializes the backend
func (backend *TFMockBackend) Init(config.TFBackendConfig) error {
	backend.creden = make(map[string]TFMockBackendCreden)
	log.Printf("%p", &backend.creden)
	return nil
}

// LookupUser check if a user is setup in the backend
func (backend TFMockBackend) LookupUser(username string) bool {
	_, ok := backend.creden[username]
	return ok
}

// SetupUser setup a user in the backend.
// An credential entry is created for user.
func (backend *TFMockBackend) SetupUser(username string) error {
	if backend.LookupUser(username) {
		return nil
	}

	log.Printf("%p", &backend.creden)
	backend.creden[username] = TFMockBackendCreden{Password: "mock-password"}
	return nil
}

// GetUserBackendCredential returns the credential to the backend for user
func (backend TFMockBackend) GetUserBackendCredential(username string) (ports.TFBackendCredential, error) {
	creden, ok := backend.creden[username]
	if !ok {
		return nil, UserCredenlNotFound{Username: username}
	}
	return creden, nil
}

// GetHCLBackendBlock return an empty string
func (backend TFMockBackend) GetHCLBackendBlock(username, workspace string) (string, error) {
	return "", nil
}

// GetState returns a empty TFState
func (backend TFMockBackend) GetState(username string, key ports.TFStateKey) (ports.TFState, error) {
	return TFState(""), nil
}

// PushState does nothing
func (backend TFMockBackend) PushState(username string, key ports.TFStateKey, state ports.TFState) error {
	return nil
}
