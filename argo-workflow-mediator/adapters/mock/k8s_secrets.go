package mock

import (
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/config"
	corev1 "k8s.io/api/core/v1"
)

// K8SSecretClient is a mock up of ports.K8SSecretClient
type K8SSecretClient struct {
	secrets map[string]corev1.Secret
}

// NewK8SSecretClient creates a new mock K8SSecretClient
func NewK8SSecretClient() K8SSecretClient {
	cli := K8SSecretClient{}
	cli.secrets = make(map[string]corev1.Secret)
	return cli
}

// Init implements ports.K8SSecretClient, the config parameter is ignored
func (cli K8SSecretClient) Init(config.K8SConfig) error {
	return nil
}

// Create implements ports.K8SSecretClient
func (cli K8SSecretClient) Create(secret corev1.Secret) error {
	cli.secrets[secret.Name] = secret
	return nil
}

// Get implements ports.K8SSecretClient
func (cli K8SSecretClient) Get(name string) (*corev1.Secret, error) {
	secret, ok := cli.secrets[name]
	if !ok {
		return nil, K8SSecretNotFound{name}
	}
	return &secret, nil
}

// Update implements ports.K8SSecretClient
func (cli K8SSecretClient) Update(secret corev1.Secret) error {
	cli.secrets[secret.Name] = secret
	return nil
}

// Delete implements ports.K8SSecretClient
func (cli K8SSecretClient) Delete(name string) error {
	if _, ok := cli.secrets[name]; !ok {
		return K8SSecretNotFound{name}
	}
	delete(cli.secrets, name)
	return nil
}

// Count counts the number of secrets stored
func (cli K8SSecretClient) Count() int {
	return len(cli.secrets)
}

// List returns the list of all the secrets stored
func (cli K8SSecretClient) List() []string {
	names := make([]string, 0)
	for name := range cli.secrets {
		names = append(names, name)
	}
	return names
}
