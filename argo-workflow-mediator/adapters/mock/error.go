package mock

import (
	"fmt"
)

// UserCredenlNotFound indicates the credential for a user does not exist
type UserCredenlNotFound struct {
	Username string
}

func (err UserCredenlNotFound) Error() string {
	return fmt.Sprintf("user %s credential not found", err.Username)
}

// K8SSecretNotFound indicates the K8S secret with given name does not exist
type K8SSecretNotFound struct {
	SecretName string
}

func (err K8SSecretNotFound) Error() string {
	return fmt.Sprintf("secret %s not found", err.SecretName)
}
