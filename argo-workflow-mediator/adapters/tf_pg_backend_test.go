package adapters

import (
	"errors"
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/ports"
	"math/rand"
	"reflect"
	"strings"
	"testing"
	"unicode"
)

func Test_encodeUsername(t *testing.T) {
	for i := 0; i < 1000; i++ {
		for usernameLen := 1; usernameLen < 100; usernameLen++ {
			username := randomString(usernameLen)
			encoded := encodeUsername(username)
			for _, r := range encoded {
				if !assert.Truef(t, unicode.IsLetter(r) || unicode.IsNumber(r) || r == '_', "%s => %s", username, encoded) {
					return
				}
			}
			decoded, err := decodeUsername(encoded)
			if len(username) == 0 {
				if !assert.Error(t, err) {
					return
				}
				continue
			}
			if !assert.NoError(t, err) {
				return
			}
			assert.Equal(t, username, decoded)
		}
	}
}

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_-=+"

func randomString(length int) string {
	b := make([]byte, length)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
	return string(b)
}

type pgRows struct {
	strValue string
	rowCount int
}

func (p *pgRows) Next() bool {
	p.rowCount++
	return p.rowCount == 1
}

func (p pgRows) Scan(dest ...interface{}) error {
	if len(dest) != 1 {
		return errors.New("only takes 1 arg")
	}
	str, ok := dest[0].(*string)
	if !ok {
		return errors.New("not *string")
	}
	*str = p.strValue
	return nil
}

func (p pgRows) Err() error {
	return nil
}

func (p pgRows) Close() error {
	return nil
}

func TestTFPostgresBackend_SetupUser(t *testing.T) {
	var pg = &PostgresMock{}
	pgAdminCred := PostgresCreden{
		Host:     "host",
		Port:     1234,
		User:     "adminUser",
		Password: "password",
	}
	pgPassGen := func() string {
		return "generated_pg_password"
	}
	backend := TFPostgresBackend{
		admin:       pgAdminCred,
		credenDB:    "cred",
		credenTable: "credTable",
		tfStateDB:   "tfState",
		newPg:       func() Postgres { return pg },
		passwordGen: pgPassGen,
	}

	backendUsername := "test_user"

	pg.On("Connect", pgAdminCred, backend.credenDB).Return(nil)
	pg.On("CreateSQLUser", encodeUsername(backendUsername), pgPassGen()).Return(nil)
	stmt := &PgStmtMock{}
	stmt.On("DoQuery", encodeUsername(backendUsername), pgPassGen()).Return(nil, nil)
	stmt.On("Close").Return(nil)
	pg.On("Prep",
		fmt.Sprintf(`INSERT INTO %s (username, pg_password) VALUES ($1, $2)`, backend.credenTable),
	).Return(stmt, nil)

	pg.On("Connect", pgAdminCred, backend.tfStateDB).Return(nil)
	for _, query := range backend.schemaCreateQueries() {
		query = strings.ReplaceAll(query, "$1", backend.schemaName(encodeUsername(backendUsername)))
		query = strings.ReplaceAll(query, "$2", pgAdminCred.User)
		pg.On("Exec", query).Return(nil)
	}
	pg.On("GrantUsageOnSchema", backend.schemaName(encodeUsername(backendUsername)), encodeUsername(backendUsername)).Return(nil)

	err := backend.SetupUser(backendUsername)
	assert.NoError(t, err)

	pg.AssertExpectations(t)
	stmt.AssertExpectations(t)
}

func TestTFPostgresBackend_LookupUser(t *testing.T) {
	var pg = &PostgresMock{}
	pgAdminCred := PostgresCreden{
		Host:     "host",
		Port:     1234,
		User:     "adminUser",
		Password: "password",
	}
	pgPassGen := func() string {
		return "generated_pg_password"
	}
	backend := TFPostgresBackend{
		admin:       pgAdminCred,
		credenDB:    "cred",
		credenTable: "credTable",
		tfStateDB:   "tfState",
		newPg:       func() Postgres { return pg },
		passwordGen: pgPassGen,
	}
	username := "test_user"

	pg.On("Connect", pgAdminCred, backend.credenDB).Return(nil)
	stmt := &PgStmtMock{}
	stmt.On("DoQuery", encodeUsername(username)).Return(&pgRows{strValue: encodeUsername(username)}, nil)
	stmt.On("Close").Return(nil)
	pg.On("Prep",
		fmt.Sprintf("SELECT username FROM %s WHERE username=$1;", backend.credenTable),
	).Return(stmt, nil)

	userExists := backend.LookupUser(username)
	assert.True(t, userExists)

	pg.AssertExpectations(t)
	stmt.AssertExpectations(t)
}

func TestTFPostgresBackend_GetUserBackendCredential(t *testing.T) {
	var pg = &PostgresMock{}
	pgAdminCred := PostgresCreden{
		Host:     "host",
		Port:     1234,
		User:     "adminUser",
		Password: "password",
	}
	pgPassGen := func() string {
		return "generated_pg_password"
	}
	backend := TFPostgresBackend{
		admin:       pgAdminCred,
		credenDB:    "cred",
		credenTable: "credTable",
		tfStateDB:   "tfState",
		newPg:       func() Postgres { return pg },
		passwordGen: pgPassGen,
	}
	username := "test_user"
	password := "test_password"

	pg.On("Connect", pgAdminCred, backend.credenDB).Return(nil)
	stmt := &PgStmtMock{}
	stmt.On("DoQuery", encodeUsername(username)).Return(&pgRows{strValue: password}, nil)
	stmt.On("Close").Return(nil)
	pg.On("Prep",
		fmt.Sprintf("SELECT pg_password FROM %s WHERE username=$1", backend.credenTable),
	).Return(stmt, nil)

	backendCred, err := backend.GetUserBackendCredential(username)
	assert.NoError(t, err)
	assert.IsType(t, PostgresCreden{}, backendCred)
	cred := backendCred.(PostgresCreden)
	assert.Equal(t, encodeUsername(username), cred.User)
	decodedUsername, err := decodeUsername(cred.User)
	assert.NoError(t, err)
	assert.Equal(t, username, decodedUsername)
	assert.Equal(t, password, cred.Password)
	assert.Equal(t, pgAdminCred.Host, cred.Host)
	assert.Equal(t, pgAdminCred.Port, cred.Port)

	pg.AssertExpectations(t)
	stmt.AssertExpectations(t)
}

func TestTFPostgresBackend_GetHCLBackendBlock(t *testing.T) {
	var pg = &PostgresMock{}
	pgAdminCred := PostgresCreden{
		Host:     "host",
		Port:     1234,
		User:     "adminUser",
		Password: "password",
	}
	pgPassGen := func() string {
		return "generated_pg_password"
	}
	backend := TFPostgresBackend{
		admin:       pgAdminCred,
		credenDB:    "cred",
		credenTable: "credTable",
		tfStateDB:   "tfState",
		newPg:       func() Postgres { return pg },
		passwordGen: pgPassGen,
	}
	username := "test_user"
	password := "test_password"

	pg.On("Connect", pgAdminCred, backend.credenDB).Return(nil)
	stmt := &PgStmtMock{}
	stmt.On("DoQuery", encodeUsername(username)).Return(&pgRows{strValue: password}, nil)
	stmt.On("Close").Return(nil)
	pg.On("Prep",
		fmt.Sprintf("SELECT pg_password FROM %s WHERE username=$1", backend.credenTable),
	).Return(stmt, nil)

	block, err := backend.GetHCLBackendBlock(username, "?????")
	assert.NoError(t, err)

	backendBlock := fmt.Sprintf(`
backend "pg" {
  conn_str = "postgres://%s:test_password@host:1234/tfState?sslmode=disable"
  schema_name = "%s"
  skip_schema_creation = true
  skip_table_creation = true
  skip_index_creation = true
}`, encodeUsername(username), backend.schemaName(encodeUsername(username)))
	assert.Equal(t, backendBlock, block)

	pg.AssertExpectations(t)
	stmt.AssertExpectations(t)
}

func TestTFPostgresBackend_GetState(t *testing.T) {
	type fields struct {
		admin       PostgresCreden
		credenDB    string
		credenTable string
		tfStateDB   string
		newPg       func() Postgres
		passwordGen func() string
	}
	type args struct {
		username string
		key      ports.TFStateKey
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    ports.TFState
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			backend := TFPostgresBackend{
				admin:       tt.fields.admin,
				credenDB:    tt.fields.credenDB,
				credenTable: tt.fields.credenTable,
				tfStateDB:   tt.fields.tfStateDB,
				newPg:       tt.fields.newPg,
				passwordGen: tt.fields.passwordGen,
			}
			got, err := backend.GetState(tt.args.username, tt.args.key)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetState() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetState() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTFPostgresBackend_PushState(t *testing.T) {
	type fields struct {
		admin       PostgresCreden
		credenDB    string
		credenTable string
		tfStateDB   string
		newPg       func() Postgres
		passwordGen func() string
	}
	type args struct {
		username string
		key      ports.TFStateKey
		state    ports.TFState
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			backend := TFPostgresBackend{
				admin:       tt.fields.admin,
				credenDB:    tt.fields.credenDB,
				credenTable: tt.fields.credenTable,
				tfStateDB:   tt.fields.tfStateDB,
				newPg:       tt.fields.newPg,
				passwordGen: tt.fields.passwordGen,
			}
			if err := backend.PushState(tt.args.username, tt.args.key, tt.args.state); (err != nil) != tt.wantErr {
				t.Errorf("PushState() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
