package adapters

import (
	"context"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/config"
	coreV1 "k8s.io/api/core/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	coreV1Types "k8s.io/client-go/kubernetes/typed/core/v1"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	"time"
)

// K8SSecretClient is a client for manipulate K8S secrets. Implements ports.K8SSecretClient.
type K8SSecretClient struct {
	// if empty then uses incluster config
	secretsClient coreV1Types.SecretInterface
}

// Init initialize the client
func (cli *K8SSecretClient) Init(conf config.K8SConfig) error {
	var err error
	if conf.Kubeconfig == nil {
		err = cli.inClusterInit(conf.Namespace)
	} else {
		err = cli.initFromConfigStr(conf.Namespace, conf.Kubeconfig)
	}

	if err != nil {
		log.Error(err)
		return err
	}
	return nil
}

// init from kubeconfig string
func (cli *K8SSecretClient) initFromConfigStr(namespace string, kubeconfig []byte) error {
	config, err := clientcmd.RESTConfigFromKubeConfig(kubeconfig)
	if err != nil {
		return err
	}
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		return err
	}
	cli.secretsClient = clientset.CoreV1().Secrets(namespace)
	return nil
}

// init from in-cluster config
func (cli *K8SSecretClient) inClusterInit(namespace string) error {
	config, err := rest.InClusterConfig()
	if err != nil {
		return err
	}
	// creates the clientset
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		return err
	}
	cli.secretsClient = clientset.CoreV1().Secrets(namespace)
	return nil
}

// Create creates a new secret
func (cli K8SSecretClient) Create(secretSpec coreV1.Secret) error {
	logger := log.WithFields(log.Fields{
		"package":    "adapters",
		"function":   "K8SSecretClient.Create",
		"secretName": secretSpec.Name,
	})
	_, err := cli.secretsClient.Create(cli.newCtx(), &secretSpec, metaV1.CreateOptions{})
	if err != nil {
		logger.WithError(err).Error("fail to create k8s secret")
		return err
	}
	logger.Info("k8s secret created")

	return nil
}

// Get fetch the secret with the given name
func (cli K8SSecretClient) Get(name string) (*coreV1.Secret, error) {
	logger := log.WithFields(log.Fields{
		"package":    "adapters",
		"function":   "K8SSecretClient.Get",
		"secretName": name,
	})
	secret, err := cli.secretsClient.Get(cli.newCtx(), name, metaV1.GetOptions{})
	if err != nil {
		logger.WithError(err).Error("fail to get secret")
		return nil, err
	}
	return secret, nil
}

// Update updates a secret
func (cli K8SSecretClient) Update(secret coreV1.Secret) error {
	logger := log.WithFields(log.Fields{
		"package":    "adapters",
		"function":   "K8SSecretClient.Create",
		"secretName": secret.Name,
	})
	_, err := cli.secretsClient.Update(cli.newCtx(), &secret, metaV1.UpdateOptions{})
	if err != nil {
		logger.WithError(err).Error("fail to update secret")
		return err
	}
	logger.Info("secret updated")
	return nil
}

// Delete deletes the secret with given name
func (cli K8SSecretClient) Delete(name string) error {
	logger := log.WithFields(log.Fields{
		"package":    "adapters",
		"function":   "K8SSecretClient.Create",
		"secretName": name,
	})
	err := cli.secretsClient.Delete(cli.newCtx(), name, metaV1.DeleteOptions{})
	if err != nil {
		logger.WithError(err).Error("fail to delete secret")
		return err
	}
	logger.Info("secret deleted")
	return nil
}

func (cli K8SSecretClient) newCtx() context.Context {
	// no need to return the cancel func, this context does not propagate to outside of K8SSecretClient
	ctx, _ := context.WithTimeout(context.Background(), time.Second*10)
	return ctx
}
