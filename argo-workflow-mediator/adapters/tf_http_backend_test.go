package adapters

import (
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func Test_parseJWTTokenLifetimeConfig(t *testing.T) {
	tests := []struct {
		name  string
		input string
		want  time.Duration
	}{
		{
			name:  "empty",
			input: "",
			want:  defaultJWTTokenLifetime,
		},
		{
			name:  "no unit",
			input: "123",
			want:  defaultJWTTokenLifetime,
		},
		{
			name:  "not number",
			input: "jfsdhfkjas",
			want:  defaultJWTTokenLifetime,
		},
		{
			name:  "1sec",
			input: "1s",
			want:  1 * time.Second,
		},
		{
			name:  "123sec",
			input: "123s",
			want:  123 * time.Second,
		},
		{
			name:  "min",
			input: "123m",
			want:  123 * time.Minute,
		},
		{
			name:  "hour",
			input: "123h",
			want:  123 * time.Hour,
		},
		{
			name:  "day",
			input: "123d",
			want:  123 * time.Hour * 24,
		},
		{
			name:  "float",
			input: "223.456h",
			want:  defaultJWTTokenLifetime,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equalf(t, tt.want, parseJWTTokenLifetimeConfig(tt.input), "parseDuration(%v)", tt.input)
		})
	}
}
