package adapters

import (
	"context"
	"encoding/json"
	cloudevents "github.com/cloudevents/sdk-go/v2"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/ports"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/types"
	"sync"
)

// StanEventAdapter is an adapter for NATS Streaming, and implements ports.EvenPort
type StanEventAdapter struct {
	eventConn messaging2.EventConnection
	handlers  ports.EventHandlers
}

var _ ports.EventSrc = (*StanEventAdapter)(nil)

// NewStanEventAdapter creates a new StanEventAdapter.
func NewStanEventAdapter(eventConn messaging2.EventConnection) *StanEventAdapter {
	return &StanEventAdapter{
		eventConn: eventConn,
		handlers:  nil,
	}
}

// SetHandlers ...
func (adapter *StanEventAdapter) SetHandlers(handlers ports.EventHandlers) {
	adapter.handlers = handlers
}

// Start implements ports.EventSrc
func (adapter *StanEventAdapter) Start(ctx context.Context, wg *sync.WaitGroup) error {
	err := adapter.eventConn.Listen(ctx, map[common.EventType]messaging2.EventHandlerFunc{
		types.WorkflowCreateRequestedEvent:    handlerWrapper(adapter.handlers.WorkflowCreateHandler),
		types.WorkflowTerminateRequestedEvent: handlerWrapper(adapter.handlers.WorkflowTerminateHandler),
		types.WorkflowResubmitRequestedEvent:  handlerWrapper(adapter.handlers.WorkflowResubmitHandler),
	}, wg)
	if err != nil {
		return err
	}
	return nil
}

func handlerWrapper[T any](handler func(T, ports.OutgoingEvents)) messaging2.EventHandlerFunc {
	return func(ctx context.Context, event cloudevents.Event, writer messaging2.EventResponseWriter) error {
		var request T
		err := json.Unmarshal(event.Data(), &request)
		if err != nil {
			return err
		}
		handler(request, eventOut{writer: writer})
		return nil
	}
}

// eventOut is for outgoing events
type eventOut struct {
	writer messaging2.EventResponseWriter
}

var _ ports.OutgoingEvents = (*eventOut)(nil)

func (sink eventOut) WorkflowCreated(created types.WorkflowCreated) {
	sink.publish(created)
}

func (sink eventOut) WorkflowCreateFailed(failed types.WorkflowCreateFailed) {
	sink.publish(failed)
}

func (sink eventOut) WorkflowResubmitted(resubmitted types.WorkflowResubmitted) {
	sink.publish(resubmitted)
}

func (sink eventOut) WorkflowResubmitFailed(failed types.WorkflowResubmitFailed) {
	sink.publish(failed)
}

func (sink eventOut) WorkflowTerminated(terminated types.WorkflowTerminated) {
	sink.publish(terminated)
}

func (sink eventOut) WorkflowTerminateFailed(failed types.WorkflowTerminateFailed) {
	sink.publish(failed)
}

func (sink eventOut) publish(event types.Event) {
	logger := log.WithFields(log.Fields{
		"package":   "adapters",
		"function":  "eventOut.Publish",
		"eventType": event.EventType(),
		"tid":       event.Transaction(),
	})
	ce, err := messaging2.CreateCloudEventWithAutoSource(event, event.EventType())
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return
	}
	err = sink.writer.Write(ce)
	if err != nil {
		logger.WithError(err).Error("fail to publish")
		return
	}
	logger.Info("event published")
}

// EventUnmarshal is of type EventUnmarshaler, which unmarshals cloudevents
// into apprioate domain events. Returns nil if there is no corrsponding
// domain event for the cloudevent.
func EventUnmarshal(logger *log.Logger, ce *cloudevents.Event) types.Event {
	eventType := types.EventType(ce.Type())

	switch eventType {
	case types.WorkflowCreateRequestedEvent:
		var event types.WorkflowCreate
		err := json.Unmarshal(ce.Data(), &event)
		if err != nil {
			logger.Error(err)
			return nil
		}
		event.EventID = types.EventID(ce.ID())
		event.TransactionID = messaging.GetTransactionID(ce)
		return event

	case types.WorkflowTerminateRequestedEvent:
		var event types.WorkflowTerminate
		err := json.Unmarshal(ce.Data(), &event)
		if err != nil {
			logger.Error(err)
			return nil
		}
		event.EventID = types.EventID(ce.ID())
		event.TransactionID = messaging.GetTransactionID(ce)
		return event

	case types.WorkflowResubmitRequestedEvent:
		var event types.WorkflowResubmit
		err := json.Unmarshal(ce.Data(), &event)
		if err != nil {
			logger.Error(err)
			return nil
		}
		event.EventID = types.EventID(ce.ID())
		event.TransactionID = messaging.GetTransactionID(ce)
		return event

	default:
		logger.WithField("event_type", eventType).Trace("unsupported event type")
	}
	return nil
}
