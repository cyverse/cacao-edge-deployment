// Package adapters ...
// Postgresql backend for Terraform.
// Each user will have their own credential to Postgres for isolation.
// The user credentials will be stored in another Postgres database.
package adapters

import (
	"encoding/base32"
	"errors"
	"fmt"
	"strings"

	_ "github.com/lib/pq" // postgres driver
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/config"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/ports"
)

// TFState is Terraform state
type TFState string

// ToStr return the TFState as string
func (tfs TFState) ToStr() string {
	return string(tfs)
}

// TFPostgresBackend is Terraform Postgres(pg) Backend.
// usernames are base32 encoded to avoid undesired characters in table/role name. All published method should encode
// the username in the top of the method.
//
// Note this has a implicit length requirement on the username, since postgres has a max length on table name.
type TFPostgresBackend struct {
	admin PostgresCreden
	// name of the DB that stores user pg credential.
	// the DB will be on the same instance of postgres as the DB that stores TF states,
	// so host and port will be same as the admin Postgres credential.
	credenDB string
	// the table in credenDB that stores user's Postgres credential
	credenTable string
	// name of the DB that stores Terraform states
	tfStateDB string
	newPg     func() Postgres
	// generate random password
	passwordGen func() string
}

// NewTFPostgresBackend creates a TFPostgresBackend
func NewTFPostgresBackend() ports.TFBackend {
	return &TFPostgresBackend{newPg: NewPostgres, passwordGen: generatePostgresPassword}
}

// Init initializes the backend
func (backend *TFPostgresBackend) Init(conf config.TFBackendConfig) error {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "TFPostgresBackend.Init",
	})
	backend.admin = PostgresCreden{Host: conf.Host, Port: conf.Port, User: conf.AdminUser, Password: conf.AdminPassword}
	backend.credenDB = conf.UserCredentialDB
	backend.credenTable = conf.UserCredentialTable
	backend.tfStateDB = conf.TFStateDB

	adminConn := backend.newPg()
	err := adminConn.Connect(backend.admin, backend.credenDB)
	if err != nil {
		logger.WithError(err).Error("fail to connect to postgres")
		return err
	}

	// Create credential table if not exist
	if !adminConn.FindTable(backend.credenTable) {
		err = backend.createUserCredenTable(adminConn, backend.credenTable)
		if err != nil {
			logger.WithError(err).Error("fail to create credential table")
			return err
		}
		log.Infof("Create table '%s'\n", backend.credenTable)
	}

	// Create Terraform State DB if not exist
	if !adminConn.FindDB(backend.tfStateDB) {
		err = adminConn.CreateDB(backend.tfStateDB, backend.admin.User)
		if err != nil {
			logger.WithError(err).Error("fail tf state DB")
			return err
		}
		log.Infof("Create database '%s'\n", backend.tfStateDB)
	}

	return nil
}

func (backend TFPostgresBackend) createUserCredenTable(db Postgres, tableName string) error {

	query := fmt.Sprintf(`CREATE TABLE %s (
    username varchar(70) PRIMARY KEY,
    pg_password varchar(100)
);`, tableName)

	err := db.Exec(query)
	if err != nil {
		return err
	}
	return nil
}

// LookupUser looks up if a user has been setup in the backend.
// Username is encoded.
func (backend TFPostgresBackend) LookupUser(username string) bool {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "TFPostgresBackend.LookupUser",
		"username": username,
	})
	if len(username) > 36 {
		// postgres has a length limit on table name
		logger.Warn("username too long")
	}
	// encode username
	username = encodeUsername(username)

	adminConn := backend.newPg()
	err := adminConn.Connect(backend.admin, backend.credenDB)
	if err != nil {
		logger.WithError(err).Error("fail to connect to postgres")
		return false
	}

	query := fmt.Sprintf("SELECT username FROM %s WHERE username=$1;", backend.credenTable)
	stmt, err := adminConn.Prep(query)
	if err != nil {
		logger.WithError(err).Error("fail to prep stmt")
		return false
	}
	defer stmt.Close()
	rows, err := stmt.DoQuery(username)
	if err != nil {
		logger.WithError(err).Error("look up query failed")
		return false
	}

	usernames := make([]string, 0)
	for rows.Next() {
		var name string
		if err := rows.Scan(&name); err != nil {
			logger.WithError(err).Error("fail to scan row")
			return false
		}
		usernames = append(usernames, name)
		logger.Trace("found user in tf backend")
	}
	// Check for errors from iterating over rows.
	if err := rows.Err(); err != nil {
		logger.WithError(err).Error("err when iterating over rows")
		return false
	}

	if len(usernames) > 0 {
		logger.WithField("username", username).Debug("user found in tf pg backend")
		return true
	}
	logger.Debug("no user matched in tf pg backend")
	return false
}

func (backend TFPostgresBackend) saveUserCreden(adminConn Postgres, username string, password string) error {
	query := `INSERT INTO %s (username, pg_password) VALUES ($1, $2)`
	query = fmt.Sprintf(query, backend.credenTable)

	stmt, err := adminConn.Prep(query)
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.DoQuery(username, password)
	if err != nil {
		return err
	}

	return nil
}

// SetupUser setup a user in the backend.
// A postgresql role is created for the user.
// The credential of the pg role is stored in a table.
// A schema(which includes a table) is created in a separate DB for the user
// to store TF states.
// Username is encoded.
func (backend TFPostgresBackend) SetupUser(username string) error {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "TFPostgresBackend.SetupUser",
		"username": username,
	})
	if len(username) > 36 {
		// postgres has a length limit (63) on table name, with the prefix and encoding on username, the limit is 36 on the original username.
		logger.Error("username too long")
		return errors.New("username too long")
	}
	// encode username
	username = encodeUsername(username)

	adminConn := backend.newPg()
	err := adminConn.Connect(backend.admin, backend.credenDB)
	if err != nil {
		logger.WithError(err).Error("fail to connect to postgres")
		return err
	}

	password := backend.passwordGen()

	err = adminConn.CreateSQLUser(username, password)
	if err != nil {
		logger.WithError(err).Error("fail to create sql user")
		return err
	}

	err = backend.saveUserCreden(adminConn, username, password)
	if err != nil {
		logger.WithError(err).Error("err from backend.saveUserCreden()")
		return err
	}

	err = backend.setupUserSchema(username)
	if err != nil {
		logger.WithError(err).Error("err from backend.setupUserSchema()")
		return err
	}

	return nil
}

func (backend TFPostgresBackend) schemaName(username string) string {
	return username + "_tf_state"
}

func (backend TFPostgresBackend) setupUserSchema(username string) error {

	adminConn := backend.newPg()
	err := adminConn.Connect(backend.admin, backend.tfStateDB)
	if err != nil {
		return err
	}

	err = backend.createTFStateSchema(adminConn, backend.schemaName(username), backend.admin.User)
	if err != nil {
		return err
	}

	err = adminConn.GrantUsageOnSchema(backend.schemaName(username), username)
	if err != nil {
		return err
	}
	return err
}

// GetUserBackendCredential returns the credential of user's Postgresql role.
// Username is encoded.
func (backend TFPostgresBackend) GetUserBackendCredential(username string) (ports.TFBackendCredential, error) {
	// encode username
	username = encodeUsername(username)
	return backend.getUserBackendCredential(username)
}

// GetUserBackendCredential returns the credential of user's Postgresql role
func (backend TFPostgresBackend) getUserBackendCredential(username string) (ports.TFBackendCredential, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "TFPostgresBackend.getUserBackendCredential",
		"username": username,
	})
	adminConn := backend.newPg()
	err := adminConn.Connect(backend.admin, backend.credenDB)
	if err != nil {
		logger.WithError(err).Error("fail to connect to postgres")
		return nil, err
	}

	query := fmt.Sprintf("SELECT pg_password FROM %s WHERE username=$1", backend.credenTable)
	stmt, err := adminConn.Prep(query)
	if err != nil {
		logger.WithError(err).Error("fail to prep stmt")
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.DoQuery(username)
	if err != nil {
		logger.WithError(err).Error("query to get backend cred failed")
		return nil, err
	}

	credentials := make([]string, 0)
	for rows.Next() {
		var name string
		if err := rows.Scan(&name); err != nil {
			logger.WithError(err).Error("fail to scan row")
			return nil, err
		}
		credentials = append(credentials, name)
	}
	// Check for errors from iterating over rows.
	if err := rows.Err(); err != nil {
		logger.WithError(err).Error("err when iterating over rows")
		return nil, err
	}

	if len(credentials) > 0 {
		userCreden := PostgresCreden{Host: backend.admin.Host, Port: backend.admin.Port}
		userCreden.User = username
		userCreden.Password = credentials[0]
		return userCreden, nil
	}

	return nil, UserCredenlNotFound{Username: username}
}

// GetHCLBackendBlock returns the "backend" block in HCL for user.
// Username is encoded.
//
// e.g.
//
//	backend "pg" {
//	  conn_str = "postgres://username:password@postgresql_host:5432/database_name"
//	}
func (backend TFPostgresBackend) GetHCLBackendBlock(username, workspace string) (string, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "TFPostgresBackend.GetHCLBackendBlock",
		"username": username,
	})
	// encode username
	username = encodeUsername(username)

	creden, err := backend.getUserBackendCredential(username)
	if err != nil {
		logger.WithError(err).Error("fail to get backend cred")
		return "", err
	}

	pgCreden, ok := creden.(PostgresCreden)
	if !ok {
		logger.Error("PostgresCreden should be the only implementation used")
		panic("BUG")
	}

	return pgCreden.getHCLBackendBlock(backend.tfStateDB, backend.schemaName(username)), nil
}

// GetState fetches the TFState of a specific template for the specified user
// and return the TFState.
// Username is encoded.
func (backend TFPostgresBackend) GetState(username string, key ports.TFStateKey) (ports.TFState, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "TFPostgresBackend.GetState",
		"username": username,
		"key":      key,
	})
	// encode username
	username = encodeUsername(username)

	adminConn := backend.newPg()
	err := adminConn.Connect(backend.admin, backend.tfStateDB)
	if err != nil {
		logger.WithError(err).Error("fail to connect to postgres")
		return nil, err
	}

	query := fmt.Sprintf("SELECT data FROM %s.states WHERE name=$1", backend.schemaName(username))
	stmt, err := adminConn.Prep(query)
	if err != nil {
		logger.WithError(err).Error("fail to prep stmt")
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.DoQuery(key.String())
	if err != nil {
		logger.WithError(err).Error("query failed to get state")
		return nil, err
	}

	tfStates := make([]string, 0)
	for rows.Next() {
		var tfState string
		if err := rows.Scan(&tfState); err != nil {
			logger.WithError(err).Error("fail to scan row")
			return nil, err
		}
		tfStates = append(tfStates, tfState)
	}
	// Check for errors from iterating over rows.
	if err := rows.Err(); err != nil {
		logger.WithError(err).Error("err when iterating over rows")
		return nil, err
	}

	if len(tfStates) > 0 {
		return TFState(tfStates[0]), nil
	}

	return nil, UserCredenlNotFound{Username: username}
}

// PushState pushes the TFState to the backend for a user's template
// Username is encoded.
func (backend TFPostgresBackend) PushState(username string, key ports.TFStateKey, state ports.TFState) error {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "TFPostgresBackend.PushState",
		"username": username,
		"key":      key,
	})
	// encode username
	username = encodeUsername(username)

	adminConn := backend.newPg()
	err := adminConn.Connect(backend.admin, backend.tfStateDB)
	if err != nil {
		logger.WithError(err).Error("fail to connect to postgres")
		return err
	}

	query := fmt.Sprintf("UPDATE %s.states SET data = $1 WHERE name = $2;", backend.schemaName(username))
	stmt, err := adminConn.Prep(query)
	if err != nil {
		logger.WithError(err).Error("fail to prep stmt")
		return err
	}
	defer stmt.Close()

	_, err = stmt.Exec(state.ToStr(), key.String())
	if err != nil {
		logger.WithError(err).Error("fail to exec query to push state")
		return err
	}

	return nil
}

func (backend TFPostgresBackend) createTFStateSchema(db Postgres, schemaName string, owner string) error {
	for _, query := range backend.schemaCreateQueries() {

		log.Warn("Use simple string replacment in SQL Query, be cautious regarding SQL injection")
		query = strings.ReplaceAll(query, "$1", schemaName)
		query = strings.ReplaceAll(query, "$2", owner)
		log.Println(query)

		err := db.Exec(query)
		if err != nil {
			return err
		}
	}
	return nil
}

func (backend TFPostgresBackend) schemaCreateQueries() [12]string {
	var queries = [...]string{
		"CREATE SCHEMA $1;",
		"ALTER SCHEMA $1 OWNER TO $2;",
		"SET default_tablespace = '';",
		"SET default_table_access_method = heap;",
		`CREATE TABLE $1.states (
    id integer NOT NULL,
    name text,
    data text
);`,
		"ALTER TABLE $1.states OWNER TO $2;",
		`CREATE SEQUENCE $1.states_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;`,
		"ALTER TABLE $1.states_id_seq OWNER TO $2;",
		"ALTER SEQUENCE $1.states_id_seq OWNED BY $1.states.id;",
		"ALTER TABLE ONLY $1.states ALTER COLUMN id SET DEFAULT nextval('$1.states_id_seq'::regclass);",
		"ALTER TABLE ONLY $1.states ADD CONSTRAINT states_pkey PRIMARY KEY (id);",

		"CREATE UNIQUE INDEX states_by_name ON $1.states USING btree (name);",
	}

	return queries
}

// use '_' as padding rather than the default '=', this way the encoded username only consists of alphanumeric char
// and '_'
var usernameEncoding = base32.HexEncoding.WithPadding('_')

// Note: base32-hex encoding uses upper case letters, but postgresql does not deal with mix cases username very well,
// so convert to lower case with ToLower().
func encodeUsername(username string) string {
	// add a prefix to avoid start with number
	return "usr_" + strings.ToLower(usernameEncoding.EncodeToString([]byte(username)))
}

func decodeUsername(encoded string) (string, error) {
	if len(encoded) < 5 {
		return "", errors.New("length < 5")
	}
	decoded, err := usernameEncoding.DecodeString(strings.ToUpper(encoded[4:]))
	if err != nil {
		return "", err
	}
	return string(decoded), nil
}
