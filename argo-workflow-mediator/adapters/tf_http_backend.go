package adapters

import (
	"errors"
	"strconv"
	"strings"
	"text/template"
	"time"

	jwt "github.com/golang-jwt/jwt/v4"
	"github.com/rs/xid"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/config"
	"gitlab.com/cyverse/cacao-edge-deployment/argo-workflow-mediator/ports"
)

const defaultJWTTokenLifetime = time.Hour

// TFHTTPBackend is Terraform HTTP backend.
type TFHTTPBackend struct {
	tokenGenerator       JWTTokenGenerator
	backendBlockTemplate *template.Template
}

// NewTFHTTPBackend ...
func NewTFHTTPBackend() *TFHTTPBackend {
	return &TFHTTPBackend{}
}

// Init ...
func (backend *TFHTTPBackend) Init(config config.TFBackendConfig) error {
	if len(config.HTTPJWTSecretKey) < 32 {
		return errors.New("jwt secret key length < 32")
	}
	backend.tokenGenerator = JWTTokenGenerator{
		keySrc: func() []byte {
			return []byte(config.HTTPJWTSecretKey)
		},
		signingMethod: jwt.SigningMethodHS256,
		lifetime:      parseJWTTokenLifetimeConfig(config.HTTPJWTLifetime),
	}
	log.WithField("tokenLifetimeSec", backend.tokenGenerator.lifetime/time.Second).Info("token lifetime set for terraform http backend")

	backend.initBackendBlockTemplate()
	return nil
}

func parseJWTTokenLifetimeConfig(s string) time.Duration {
	if len(s) < 2 {
		return defaultJWTTokenLifetime
	}
	parseInt, err := strconv.ParseUint(s[:len(s)-1], 10, 32)
	if err != nil {
		return defaultJWTTokenLifetime
	}
	if parseInt <= 0 {
		return defaultJWTTokenLifetime
	}
	switch s[len(s)-1] {
	case 's':
		return time.Duration(parseInt) * time.Second
	case 'm':
		return time.Duration(parseInt) * time.Minute
	case 'h':
		return time.Duration(parseInt) * time.Hour
	case 'd':
		return time.Duration(parseInt) * time.Hour * 24
	default:
		return defaultJWTTokenLifetime
	}
}

// LookupUser ...
func (backend TFHTTPBackend) LookupUser(username string) bool {
	// http backend does not directly store user info, so all users already has access to the backend.
	return true
}

// SetupUser ...
func (backend TFHTTPBackend) SetupUser(username string) error {
	// nothing to set up
	return nil
}

// GetUserBackendCredential ...
func (backend TFHTTPBackend) GetUserBackendCredential(username string) (ports.TFBackendCredential, error) {
	token, err := backend.tokenGenerator.Generate(username)
	if err != nil {
		return nil, err
	}
	return token, nil
}

// GetHCLBackendBlock ...
func (backend TFHTTPBackend) GetHCLBackendBlock(username, workspace string) (string, error) {
	token, err := backend.tokenGenerator.Generate(username)
	if err != nil {
		return "", err
	}
	return backend.generateBackendBlock(username, workspace, token), nil
}

// GetState ...
func (backend TFHTTPBackend) GetState(username string, key ports.TFStateKey) (ports.TFState, error) {
	panic("implement me")
}

// PushState ...
func (backend TFHTTPBackend) PushState(username string, key ports.TFStateKey, state ports.TFState) error {
	panic("implement me")
}

// initialize a template for the HCL Terraform backend block, the template is reused for multiple backend-block generations.
func (backend *TFHTTPBackend) initBackendBlockTemplate() {
	backendBlock := `
backend "http" {
    address = "{{.BaseURL}}/state/{{.Username}}/{{.Workspace}}"
    lock_address = "{{.BaseURL}}/state/{{.Username}}/{{.Workspace}}"
    unlock_address = "{{.BaseURL}}/state/{{.Username}}/{{.Workspace}}"
    username = "{{.Username}}"
    password = "{{.Token}}"
  }
`
	backendBlockTemplate := template.New("backend_block")
	backendBlockTemplate, err := backendBlockTemplate.Parse(backendBlock)
	if err != nil {
		// if error then bug in template string
		log.WithError(err).Fatal("fail to init Terraform Backend Block template")
	}
	backend.backendBlockTemplate = backendBlockTemplate
}

func (backend TFHTTPBackend) generateBackendBlock(username, tfWorkspace, token string) string {
	var templateData = struct {
		BaseURL   string
		Username  string
		Workspace string
		Token     string
	}{
		BaseURL:   "http://terraform-http-backend.default.svc", // FIXME
		Username:  username,
		Workspace: tfWorkspace,
		Token:     token,
	}
	var strBuilder strings.Builder
	err := backend.backendBlockTemplate.Execute(&strBuilder, templateData)
	if err != nil {
		return ""
	}
	return strBuilder.String()
}

// JWTTokenGenerator ...
type JWTTokenGenerator struct {
	keySrc        func() []byte
	signingMethod jwt.SigningMethod
	lifetime      time.Duration
}

// Generate ...
func (g JWTTokenGenerator) Generate(username string) (string, error) {
	timeNow := time.Now()
	token := jwt.NewWithClaims(g.signingMethod, jwt.RegisteredClaims{
		Audience:  jwt.ClaimStrings{username},
		ExpiresAt: jwt.NewNumericDate(timeNow.Add(g.lifetime)),
		ID:        xid.NewWithTime(timeNow).String(),
		IssuedAt:  jwt.NewNumericDate(timeNow),
		Issuer:    "tf-http-backend",
		NotBefore: jwt.NewNumericDate(timeNow),
		Subject:   "tf-backend",
	})
	tokenString, err := token.SignedString(g.keySrc())
	if err != nil {
		return "", errors.New("jwt token generation failed")
	}
	return tokenString, nil
}
