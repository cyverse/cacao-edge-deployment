package adapters

import (
	wfv1 "github.com/argoproj/argo-workflows/v3/pkg/apis/workflow/v1alpha1"
)

// WorkflowDefInMem is a in-memory Source of Workflow Definition
type WorkflowDefInMem struct {
	defs map[string]wfv1.Workflow
}

// NewWorkflowDefInMem ...
func NewWorkflowDefInMem() *WorkflowDefInMem {
	return nil
}

// Register a workflow definition under the given name
func (src *WorkflowDefInMem) Register(name string, wfDef *wfv1.Workflow) {
	if wfDef == nil {
		panic("wf definition is nil")
	}
	src.defs[name] = *wfDef
}

// GetWorkflow looks up workflow definition, and return the definition if
// found. nil is returned if not found.
func (src WorkflowDefInMem) GetWorkflow(wfName string) *wfv1.Workflow {
	wf, ok := src.defs[wfName]
	if !ok {
		return nil
	}
	return wf.DeepCopy()
}
