# workflow-event-emitter

This is a command line utility to emit workflow complete events. This will be used in the exit handler of the workflow to publish events indicate the completeness and status of the workflow.

## Cmd Line Usage
```bash
workflow-event-emitter pub [workflow-name] [workflow-status] \
    --wf-output [workflow-output-filename] \
    --provider [awm-provider] \
    --transaction [event-transaction-id]
```
| flag | description |
| --- | --- |
| --wf-output | workflow output filename, need to be a JSON file, can also be passed via env var. |
| --provider | AWM provider that the workflow is executed on |
| --transaction | Transaction ID of the emitted event |

## Environment Variable

### NATS Streaming
| name | description |
| --- | --- |
| NATS_URL | url to NATS server |
| NATS_SUBJECT | NATS Streaming channel to publish msg to |
| NATS_CLIENT_ID | client id to publish to NATS Streaming |
| NATS_CLUSTER_ID | id of the NATS Streaming cluster |

### Workflow specific
| name | description |
| --- | --- |
| WF_OUTPUT | pass the workflow output via env var, must be JSON string (JSON object) or empty, this takes priority over cmd line flag |
| METADATA | additional metadata to pass along in the emitted event, must be JSON string (JSON object) or empty string |

## Events Emitted
| event name | - |
| --- | --- |
| `WorkflowSucceeded` | emitted when workflow has succeeded |
| `WorkflowFailed` | emitted when workflow is failed or errored |
