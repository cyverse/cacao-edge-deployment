package types

import (
	wfv1 "github.com/argoproj/argo-workflows/v3/pkg/apis/workflow/v1alpha1"
	cloudevents "github.com/cloudevents/sdk-go/v2"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
)

// WorkflowSucceeded is an event emitted when workflow succeeded
type WorkflowSucceeded struct {
	Transaction common.TransactionID `json:"-"`

	// provider that the workflow operates on
	Provider Provider `json:"provider,omitempty"`
	// name of the workflow
	WorkflowName string `json:"workflow_name"`
	// status of the Workflow
	Status wfv1.NodePhase `json:"status"`
	// assume workflow output is a json object
	WfOutputs map[string]interface{} `json:"workflow_outputs"`
	// additional metadata to pass along
	Metadata map[string]interface{} `json:"metadata"`
}

// EventType ...
func (event WorkflowSucceeded) EventType() EventType {
	return WorkflowSucceededEvent
}

// TransactionID ...
func (event WorkflowSucceeded) TransactionID() common.TransactionID {
	return event.Transaction
}

// ToCloudEvent convert the event to cloudevent
func (event WorkflowSucceeded) ToCloudEvent() (*cloudevents.Event, error) {
	ce, err := messaging2.CreateCloudEventWithTransactionID(event, WorkflowSucceededEvent, event.WorkflowName, event.Transaction)
	if err != nil {
		return nil, err
	}
	return &ce, err
}

// WorkflowFailed is an event emitted when workflow failed or errored
type WorkflowFailed struct {
	Transaction common.TransactionID `json:"-"`

	// provider that the workflow operates on
	Provider Provider `json:"provider,omitempty"`
	// name of the workflow
	WorkflowName string `json:"workflow_name"`
	// status of the Workflow
	Status wfv1.NodePhase `json:"status"`
	// status of the failed node
	NodeStatus map[string]wfv1.NodePhase `json:"node_status"` // TODO this is currently not used
	// assume workflow output is a json object
	WfOutputs map[string]interface{} `json:"workflow_outputs"`
	// additional metadata to pass along
	Metadata map[string]interface{} `json:"metadata"`
}

// EventType ...
func (event WorkflowFailed) EventType() EventType {
	return WorkflowFailedEvent
}

// TransactionID ...
func (event WorkflowFailed) TransactionID() common.TransactionID {
	return event.Transaction
}

// ToCloudEvent convert the event to cloudevent
func (event WorkflowFailed) ToCloudEvent() (*cloudevents.Event, error) {
	ce, err := messaging2.CreateCloudEventWithTransactionID(event, WorkflowFailedEvent, event.WorkflowName, event.Transaction)
	if err != nil {
		return nil, err
	}
	return &ce, err
}
