package domain

import (
	"testing"

	wfv1 "github.com/argoproj/argo-workflows/v3/pkg/apis/workflow/v1alpha1"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao-edge-deployment/workflow-event-emitter/adapters"
	"gitlab.com/cyverse/cacao-edge-deployment/workflow-event-emitter/ports"
	"gitlab.com/cyverse/cacao-edge-deployment/workflow-event-emitter/types"
)

func TestValidateEnvConfig(t *testing.T) {
	type args struct {
		envConf types.EnvConfig
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{"normal", args{types.EnvConfig{
			StanConfig: types.StanConfig{URL: "example.com:4222", Subject: "subject", ClientID: "client-123", ClusterID: "cluster-id"},
			Metadata:   "",
		}}, false},
		{"missing URL", args{types.EnvConfig{
			StanConfig: types.StanConfig{URL: "", Subject: "subject", ClientID: "client-123", ClusterID: "cluster-id"}}}, true},
		{"missing subject", args{types.EnvConfig{
			StanConfig: types.StanConfig{URL: "example.com:4222", Subject: "", ClientID: "client-123", ClusterID: "cluster-id"},
		}}, true},
		{"missing client id", args{types.EnvConfig{
			StanConfig: types.StanConfig{URL: "example.com:4222", Subject: "subject", ClientID: "", ClusterID: "cluster-id"},
		}}, true},
		{"missing cluster id", args{types.EnvConfig{
			StanConfig: types.StanConfig{URL: "example.com:4222", Subject: "subject", ClientID: "client-123", ClusterID: ""},
			Metadata:   "",
		}}, true},
		{"non-json metadata, no error", args{types.EnvConfig{
			StanConfig: types.StanConfig{URL: "example.com:4222", Subject: "subject", ClientID: "client-123", ClusterID: "cluster-id"},
			Metadata:   "{{{{{{{{{",
		}}, false}, // function does not validate metadata string, so no error
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := validateEnvConfig(tt.args.envConf); (err != nil) != tt.wantErr {
				t.Errorf("validateEnvConfig() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestWorkflowCompletionEventFactory_Create(t *testing.T) {
	type fields struct {
		WorkflowName       string
		WorkflowStatus     string
		WorkflowOutputFile ports.JSONSrc
		Provider           string
		MetadataString     string
		metadata           map[string]interface{}
	}
	tests := []struct {
		name    string
		fields  fields
		want    types.Event
		wantErr bool
	}{
		{
			name: "wf succeeded",
			fields: fields{
				WorkflowName:       "wf-123",
				WorkflowStatus:     string(wfv1.NodeSucceeded),
				WorkflowOutputFile: nil,
				Provider:           "provider-123",
				MetadataString:     "",
				metadata:           nil,
			},
			want: types.WorkflowSucceeded{
				Provider:     "provider-123",
				WorkflowName: "wf-123",
				Status:       wfv1.NodeSucceeded,
				WfOutputs:    nil,
				Metadata:     nil,
			},
			wantErr: false,
		},
		{
			name: "wf succeeded w/ output",
			fields: fields{
				WorkflowName:   "wf-123",
				WorkflowStatus: string(wfv1.NodeSucceeded),
				WorkflowOutputFile: adapters.MockJSONFile{
					JSONObj: map[string]interface{}{
						"foo": "bar",
					},
				},
				Provider:       "provider-123",
				MetadataString: "",
				metadata:       nil,
			},
			want: types.WorkflowSucceeded{
				Provider:     "provider-123",
				WorkflowName: "wf-123",
				Status:       wfv1.NodeSucceeded,
				WfOutputs: map[string]interface{}{
					"foo": "bar",
				},
				Metadata: nil,
			},
			wantErr: false,
		},
		{
			name: "wf succeeded w/ metadata",
			fields: fields{
				WorkflowName:   "wf-123",
				WorkflowStatus: string(wfv1.NodeSucceeded),
				WorkflowOutputFile: adapters.MockJSONFile{
					JSONObj: map[string]interface{}{
						"foo": "bar",
					},
				},
				Provider:       "provider-123",
				MetadataString: `{"key1": "value1"}`,
				metadata:       nil,
			},
			want: types.WorkflowSucceeded{
				Provider:     "provider-123",
				WorkflowName: "wf-123",
				Status:       wfv1.NodeSucceeded,
				WfOutputs: map[string]interface{}{
					"foo": "bar",
				},
				Metadata: map[string]interface{}{
					"key1": "value1",
				},
			},
			wantErr: false,
		},
		{
			name: "wf failed",
			fields: fields{
				WorkflowName:       "wf-123",
				WorkflowStatus:     string(wfv1.NodeFailed),
				WorkflowOutputFile: nil,
				Provider:           "provider-123",
				MetadataString:     "",
				metadata:           nil,
			},
			want: types.WorkflowFailed{
				Provider:     "provider-123",
				WorkflowName: "wf-123",
				Status:       wfv1.NodeFailed,
				Metadata:     nil,
			},
			wantErr: false,
		},
		{
			// output is not carried in types.WorkflowFailed
			name: "wf failed w/ output",
			fields: fields{
				WorkflowName:   "wf-123",
				WorkflowStatus: string(wfv1.NodeFailed),
				WorkflowOutputFile: adapters.MockJSONFile{
					JSONObj: map[string]interface{}{
						"foo": "bar",
					},
				},
				Provider:       "provider-123",
				MetadataString: "",
				metadata:       nil,
			},
			want: types.WorkflowFailed{
				Provider:     "provider-123",
				WorkflowName: "wf-123",
				Status:       wfv1.NodeFailed,
				WfOutputs: map[string]interface{}{
					"foo": "bar",
				},
			},
			wantErr: false,
		},

		{
			name: "wf failed w/ metadata",
			fields: fields{
				WorkflowName:       "wf-123",
				WorkflowStatus:     string(wfv1.NodeFailed),
				WorkflowOutputFile: nil,
				Provider:           "provider-123",
				MetadataString:     `{"key1": "value1"}`,
				metadata:           nil,
			},
			want: types.WorkflowFailed{
				Provider:     "provider-123",
				WorkflowName: "wf-123",
				Status:       wfv1.NodeFailed,
				Metadata: map[string]interface{}{
					"key1": "value1",
				},
			},
			wantErr: false,
		},
		{
			name: "wf errored w/ metadata",
			fields: fields{
				WorkflowName:       "wf-123",
				WorkflowStatus:     string(wfv1.NodeError),
				WorkflowOutputFile: nil,
				Provider:           "provider-123",
				MetadataString:     `{"key1": "value1"}`,
				metadata:           nil,
			},
			want: types.WorkflowFailed{
				Provider:     "provider-123",
				WorkflowName: "wf-123",
				Status:       wfv1.NodeError,
				Metadata: map[string]interface{}{
					"key1": "value1",
				},
			},
			wantErr: false,
		},
		{
			name: "wf unknown status",
			fields: fields{
				WorkflowName:       "wf-123",
				WorkflowStatus:     "FooBar",
				WorkflowOutputFile: nil,
				Provider:           "provider-123",
				MetadataString:     "",
				metadata:           nil,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "wf illegal status",
			fields: fields{
				WorkflowName:       "wf-123",
				WorkflowStatus:     string(wfv1.NodeRunning),
				WorkflowOutputFile: nil,
				Provider:           "provider-123",
				MetadataString:     "",
				metadata:           nil,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "empty workflow name",
			fields: fields{
				WorkflowName:       "",
				WorkflowStatus:     string(wfv1.NodeSucceeded),
				WorkflowOutputFile: nil,
				Provider:           "provider-123",
				MetadataString:     "",
				metadata:           nil,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "empty workflow status",
			fields: fields{
				WorkflowName:       "wf-123",
				WorkflowStatus:     "",
				WorkflowOutputFile: nil,
				Provider:           "provider-123",
				MetadataString:     "",
				metadata:           nil,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "empty provider",
			fields: fields{
				WorkflowName:       "",
				WorkflowStatus:     string(wfv1.NodeSucceeded),
				WorkflowOutputFile: nil,
				Provider:           "",
				MetadataString:     "",
				metadata:           nil,
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			fac := WorkflowCompletionEventFactory{
				WorkflowName:       tt.fields.WorkflowName,
				WorkflowStatus:     tt.fields.WorkflowStatus,
				WorkflowOutputFile: tt.fields.WorkflowOutputFile,
				Provider:           tt.fields.Provider,
				MetadataString:     tt.fields.MetadataString,
				metadata:           tt.fields.metadata,
			}
			got, err := fac.Create()
			if tt.wantErr {
				if !assert.Error(t, err) {
					return
				}
			} else {
				if !assert.NoError(t, err) {
					return
				}
			}
			assert.Equal(t, tt.want, got)
		})
	}
}

func TestPublishEvent(t *testing.T) {
	envConf := types.EnvConfig{
		StanConfig: types.StanConfig{URL: "example.com:4222", Subject: "subject", ClientID: "client-123", ClusterID: "cluster-id"},
		Metadata:   "",
	}
	wfName := "wf-123"
	t.Run("succeeded", func(t *testing.T) {
		mockSink := adapters.MockEventSink{}
		wfOutputs := map[string]interface{}{"foo": "bar"}
		event := types.WorkflowSucceeded{
			Provider:     types.Provider(common.NewID("provider")),
			WorkflowName: wfName,
			Status:       wfv1.NodeSucceeded,
			WfOutputs:    wfOutputs,
			Metadata:     nil,
		}
		err := PublishEvent(&mockSink, envConf, event)
		assert.NoError(t, err)
		if !assert.Len(t, mockSink.ListEvents(), 1) {
			return
		}
		received := mockSink.ListEvents()[0]
		assert.Equal(t, types.WorkflowSucceededEvent, received.EventType())

		ce, err := received.ToCloudEvent()
		if !assert.NoError(t, err) {
			return
		}
		var wfEvent types.WorkflowSucceeded
		err = ce.DataAs(&wfEvent)
		if !assert.NoError(t, err) {
			return
		}
		assert.Equal(t, wfName, wfEvent.WorkflowName)
		if !assert.NotNil(t, wfEvent.WfOutputs) {
			return
		}
		assert.Equal(t, wfOutputs, wfEvent.WfOutputs)
	})
	t.Run("succeeded w/ metadata", func(t *testing.T) {
		mockSink := adapters.MockEventSink{}
		wfOutputs := map[string]interface{}{"foo": "bar"}
		metadata := map[string]interface{}{"key1": "value1"}
		event := types.WorkflowSucceeded{
			Provider:     types.Provider(common.NewID("provider")),
			WorkflowName: wfName,
			Status:       wfv1.NodeSucceeded,
			WfOutputs:    wfOutputs,
			Metadata:     metadata,
		}
		err := PublishEvent(&mockSink, envConf, event)
		assert.NoError(t, err)
		if !assert.Len(t, mockSink.ListEvents(), 1) {
			return
		}
		received := mockSink.ListEvents()[0]
		assert.Equal(t, types.WorkflowSucceededEvent, received.EventType())

		ce, err := received.ToCloudEvent()
		if !assert.NoError(t, err) {
			return
		}
		var wfEvent types.WorkflowSucceeded
		err = ce.DataAs(&wfEvent)
		if !assert.NoError(t, err) {
			return
		}
		assert.Equal(t, wfName, wfEvent.WorkflowName)
		if !assert.NotNil(t, wfEvent.WfOutputs) {
			return
		}
		assert.Equal(t, wfOutputs, wfEvent.WfOutputs, "workflow output not equal")
		assert.Equal(t, metadata, wfEvent.Metadata, "metadata not equal")
	})
	t.Run("succeeded w/ metadata w/ tid", func(t *testing.T) {
		mockSink := adapters.MockEventSink{}
		wfOutputs := map[string]interface{}{"foo": "bar"}
		metadata := map[string]interface{}{"key1": "value1"}
		event := types.WorkflowSucceeded{
			Transaction:  messaging2.NewTransactionID(),
			Provider:     types.Provider(common.NewID("provider")),
			WorkflowName: wfName,
			Status:       wfv1.NodeSucceeded,
			WfOutputs:    wfOutputs,
			Metadata:     metadata,
		}
		err := PublishEvent(&mockSink, envConf, event)
		assert.NoError(t, err)
		if !assert.Len(t, mockSink.ListEvents(), 1) {
			return
		}
		received := mockSink.ListEvents()[0]
		assert.Equal(t, types.WorkflowSucceededEvent, received.EventType())

		ce, err := received.ToCloudEvent()
		if !assert.NoError(t, err) {
			return
		}
		assert.Equal(t, event.Transaction, messaging2.GetTransactionID(ce))
		var wfEvent types.WorkflowSucceeded
		err = ce.DataAs(&wfEvent)
		if !assert.NoError(t, err) {
			return
		}
		assert.Equal(t, wfName, wfEvent.WorkflowName)
		if !assert.NotNil(t, wfEvent.WfOutputs) {
			return
		}
		assert.Equal(t, wfOutputs, wfEvent.WfOutputs, "workflow output not equal")
		assert.Equal(t, metadata, wfEvent.Metadata, "metadata not equal")
	})
	t.Run("failed", func(t *testing.T) {
		mockSink := adapters.MockEventSink{}
		event := types.WorkflowFailed{
			Provider:     types.Provider(common.NewID("provider")),
			WorkflowName: wfName,
			Status:       wfv1.NodeFailed,
			Metadata:     nil,
		}
		err := PublishEvent(&mockSink, envConf, event)
		assert.NoError(t, err)
		if !assert.Len(t, mockSink.ListEvents(), 1) {
			return
		}
		received := mockSink.ListEvents()[0]
		assert.Equal(t, types.WorkflowFailedEvent, received.EventType())

		ce, err := received.ToCloudEvent()
		if !assert.NoError(t, err) {
			return
		}
		var wfEvent types.WorkflowFailed
		err = ce.DataAs(&wfEvent)
		if !assert.NoError(t, err) {
			return
		}
		assert.Equal(t, wfName, wfEvent.WorkflowName)
	})
	t.Run("error", func(t *testing.T) {
		mockSink := adapters.MockEventSink{}
		event := types.WorkflowFailed{
			Provider:     types.Provider(common.NewID("provider")),
			WorkflowName: wfName,
			Status:       wfv1.NodeError,
			Metadata:     nil,
		}
		err := PublishEvent(&mockSink, envConf, event)
		assert.NoError(t, err)
		if !assert.Len(t, mockSink.ListEvents(), 1) {
			return
		}
		received := mockSink.ListEvents()[0]
		assert.Equal(t, types.WorkflowFailedEvent, received.EventType())
	})
	t.Run("unknown status, no error", func(t *testing.T) {
		mockSink := adapters.MockEventSink{}
		event := types.WorkflowSucceeded{
			Provider:     types.Provider(common.NewID("provider")),
			WorkflowName: wfName,
			Status:       "Foobar",
			WfOutputs:    nil,
			Metadata:     nil,
		}
		err := PublishEvent(&mockSink, envConf, event)
		// function does not validate wf status, so no error
		assert.NoError(t, err)
		// event will be published as is
		assert.Len(t, mockSink.ListEvents(), 1)
	})
	t.Run("illegal status, no error", func(t *testing.T) {
		mockSink := adapters.MockEventSink{}
		event := types.WorkflowSucceeded{
			Provider:     types.Provider(common.NewID("provider")),
			WorkflowName: wfName,
			Status:       wfv1.NodeRunning,
			WfOutputs:    nil,
			Metadata:     nil,
		}
		err := PublishEvent(&mockSink, envConf, event)
		// function does not validate wf status, so no error
		assert.NoError(t, err)
		// event will be published as is
		assert.Len(t, mockSink.ListEvents(), 1)
	})
}
