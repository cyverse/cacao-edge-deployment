package domain

import (
	"encoding/json"
	"errors"
	"fmt"

	wfv1 "github.com/argoproj/argo-workflows/v3/pkg/apis/workflow/v1alpha1"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-edge-deployment/workflow-event-emitter/ports"
	"gitlab.com/cyverse/cacao-edge-deployment/workflow-event-emitter/types"
)

// WorkflowCompletionEventFactory ...
type WorkflowCompletionEventFactory struct {
	TransactionID      string // can be empty
	WorkflowName       string
	WorkflowStatus     string
	WorkflowOutputFile ports.JSONSrc // optional, can be nil
	Provider           string
	MetadataString     string
	metadata           map[string]interface{}
}

func (fac *WorkflowCompletionEventFactory) validate() error {
	if fac.WorkflowName == "" {
		return errors.New("workflow name cannot be empty")
	}
	if fac.WorkflowStatus == "" {
		return errors.New("workflow name cannot be empty")
	}
	if fac.Provider == "" {
		return errors.New("workflow name cannot be empty")
	}
	switch wfv1.NodePhase(fac.WorkflowStatus) {
	default:
		return fmt.Errorf("unknown status %s", fac.WorkflowStatus)
	// Non-completion status
	case wfv1.NodePending:
		fallthrough
	case wfv1.NodeRunning:
		fallthrough
	case wfv1.NodeSkipped:
		fallthrough
	case wfv1.NodeOmitted:
		return fmt.Errorf("illegal status %s", fac.WorkflowStatus)

	// Completion status
	case wfv1.NodeSucceeded:
		fallthrough
	case wfv1.NodeFailed:
		fallthrough
	case wfv1.NodeError:
		break
	}
	return nil
}

func (fac *WorkflowCompletionEventFactory) parseMetadata() error {
	if fac.MetadataString == "" {
		fac.metadata = nil
		return nil
	}
	err := json.Unmarshal([]byte(fac.MetadataString), &fac.metadata)
	if err != nil {
		return err
	}
	return nil
}

// Create creates a workflow completion event.
func (fac *WorkflowCompletionEventFactory) Create() (types.Event, error) {
	if err := fac.validate(); err != nil {
		return nil, err
	}
	if err := fac.parseMetadata(); err != nil {
		return nil, err
	}

	switch wfv1.NodePhase(fac.WorkflowStatus) {
	default:
		return nil, fmt.Errorf("unknown status %s", fac.WorkflowStatus)

	// Non-completion status
	case wfv1.NodePending:
		fallthrough
	case wfv1.NodeRunning:
		fallthrough
	case wfv1.NodeSkipped:
		fallthrough
	case wfv1.NodeOmitted:
		return nil, fmt.Errorf("illegal status %s", fac.WorkflowStatus)

	// Completion status
	case wfv1.NodeSucceeded:
		return fac.newWorkflowSucceedEvent()
	case wfv1.NodeFailed:
		return fac.newWorkflowFailedEvent()
	case wfv1.NodeError:
		return fac.newWorkflowFailedEvent()
	}
}

func (fac *WorkflowCompletionEventFactory) newWorkflowSucceedEvent() (types.Event, error) {
	wfOutputJSON, err := fac.parseWorkflowOutput()
	if err != nil {
		return nil, err
	}
	return types.WorkflowSucceeded{
		Transaction:  common.TransactionID(fac.TransactionID),
		Provider:     types.Provider(fac.Provider),
		WorkflowName: fac.WorkflowName,
		Status:       wfv1.NodeSucceeded,
		WfOutputs:    wfOutputJSON,
		Metadata:     fac.metadata,
	}, nil
}

func (fac *WorkflowCompletionEventFactory) newWorkflowFailedEvent() (types.Event, error) {
	wfOutputJSON, err := fac.parseWorkflowOutput()
	if err != nil {
		return nil, err
	}
	return types.WorkflowFailed{
		Transaction:  common.TransactionID(fac.TransactionID),
		Provider:     types.Provider(fac.Provider),
		WorkflowName: fac.WorkflowName,
		Status:       wfv1.NodePhase(fac.WorkflowStatus),
		NodeStatus:   nil, // TODO include node status of the failed node
		WfOutputs:    wfOutputJSON,
		Metadata:     fac.metadata,
	}, nil
}

func (fac *WorkflowCompletionEventFactory) parseWorkflowOutput() (map[string]interface{}, error) {
	if fac.WorkflowOutputFile == nil {
		return nil, nil
	} else {
		wfOutputJSON, err := fac.WorkflowOutputFile.Read()
		if err != nil {
			log.WithError(err).Error("fail to read from workflow output")
			return nil, err
		}
		return wfOutputJSON, nil
	}
}

// PublishEvent publish a msg to NATS
func PublishEvent(sink ports.StanEventSink, envConf types.EnvConfig, event types.Event) error {
	err := validateEnvConfig(envConf)
	if err != nil {
		return err
	}

	err = sink.Connect(envConf.StanConfig)
	if err != nil {
		return err
	}
	defer func(sink ports.StanEventSink) {
		if err := sink.Close(); err != nil {
			log.WithError(err).Error("fail to close event sink")
		}
	}(sink)

	err = sink.PublishEvent(event)
	if err != nil {
		return err
	}

	return nil
}

// validateEnvConfig validate EnvConfig
func validateEnvConfig(envConf types.EnvConfig) error {
	if envConf.URL == "" {
		return errors.New("URL missing from env var")
	}
	if envConf.Subject == "" {
		return errors.New("subject missing from env var")
	}
	if envConf.ClusterID == "" {
		return errors.New("cluster id missing from env var")
	}
	if envConf.ClientID == "" {
		return errors.New("client id missing from env var")
	}
	return nil
}
