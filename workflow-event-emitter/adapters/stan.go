package adapters

import (
	"gitlab.com/cyverse/cacao-common/messaging2"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-edge-deployment/workflow-event-emitter/ports"
	"gitlab.com/cyverse/cacao-edge-deployment/workflow-event-emitter/types"
)

// NewStanEventSink creates a new StanEventSink
func NewStanEventSink() ports.StanEventSink {
	return &stanEventSink{}
}

// implements StanEventSink
type stanEventSink struct {
	subject string
	conn    *messaging2.StanConnection
}

// Connect connects to NATS Streaming
func (sink *stanEventSink) Connect(conf types.StanConfig) error {

	err := retry(10, 5*time.Second, func() error {
		conn, err := messaging2.NewStanConnectionFromConfigWithoutEventSource(messaging2.NatsStanMsgConfig{
			NatsConfig: messaging2.NatsConfig{
				URL:      conf.URL,
				ClientID: conf.ClientID,
			},
			StanConfig: messaging2.StanConfig{
				ClusterID: conf.ClusterID,
			},
		})
		if err != nil {
			log.WithError(err).Error("fail to connect")
			return err
		}
		sink.conn = &conn
		return nil
	})
	if err != nil {
		return err
	}
	log.WithFields(map[string]interface{}{
		"url":     conf.URL,
		"cluster": conf.ClusterID,
	}).Info("connection established")

	sink.subject = conf.Subject
	return nil
}

// PublishEvent publish event to NATS Streaming
func (sink *stanEventSink) PublishEvent(event types.Event) error {
	ce, err := event.ToCloudEvent()
	if err != nil {
		return err
	}

	err = retry(10, 5*time.Second, func() error {
		err := sink.conn.Publish(*ce)
		if err != nil {
			log.WithError(err).Error("fail to publish event")
			return err
		}
		return nil
	})
	if err != nil {
		log.WithError(err).Error("fail to publish event, no more retry")
		return err
	}
	log.WithFields(map[string]interface{}{
		"subject":    sink.subject,
		"event_type": ce.Type(),
	}).Info("published event")

	return nil
}

// Close closes the NATS connection
func (sink *stanEventSink) Close() error {
	return sink.conn.Close()
}

func retry(attempts int, sleep time.Duration, fn func() error) error {
	if err := fn(); err != nil {
		if attempts--; attempts > 0 {
			time.Sleep(sleep)
			return retry(attempts, 2*sleep, fn)
		}
		return err
	}
	return nil
}
