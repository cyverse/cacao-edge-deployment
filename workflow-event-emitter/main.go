// Command Line Utility Publish a message to NATS Streaming
package main

import (
	"errors"
	"github.com/kelseyhightower/envconfig"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/cyverse/cacao-edge-deployment/workflow-event-emitter/adapters"
	"gitlab.com/cyverse/cacao-edge-deployment/workflow-event-emitter/domain"
	"gitlab.com/cyverse/cacao-edge-deployment/workflow-event-emitter/ports"
	"gitlab.com/cyverse/cacao-edge-deployment/workflow-event-emitter/types"
)

var (
	envConf types.EnvConfig

	rootCmd = &cobra.Command{
		Use:   "workflow-event-emitter",
		Short: "CLI utility to publish event upon workflow completion",
		Long:  `CLI utility to publish cloudevent to NATS Streaming upon workflow completion.`,
	}

	publishCmd = &cobra.Command{
		Use:   "pub [workflow-name] [workflow-status]",
		Short: "Publish to NATS Streaming",
		Long: `Read message from a json file and publish to NATS Streaming.
		\t[workflow-name] name of the workflow that has completed.
		\t[workflow-status] status of the workflow.
		`,
		RunE: PublishEvent,
		Args: publishCmdParseArgs,
	}
	publishCmdParam = struct {
		TransactionID      string
		WorkflowName       string
		WorkflowStatus     string
		WorkflowOutputFile string
		Provider           string
	}{}
)

func init() {
	initCmd()

	err := envconfig.Process("", &envConf)
	if err != nil {
		log.Fatal(err)
	}
}

func main() {
	if err := rootCmd.Execute(); err != nil {
		log.Fatal(err)
	}
}

func initCmd() {
	publishCmd.Flags().StringVarP(
		&publishCmdParam.WorkflowOutputFile,
		"wf-output",
		"o",
		"",
		"workflow output filename, need to be a JSON file",
	)
	publishCmd.Flags().StringVarP(
		&publishCmdParam.Provider,
		"provider",
		"p",
		"",
		"AWM provider that the workflow is executed on",
	)
	publishCmd.Flags().StringVarP(
		&publishCmdParam.TransactionID,
		"transaction",
		"t",
		"",
		"Transaction ID of the emitted event",
	)
	err := publishCmd.MarkFlagRequired("provider")
	if err != nil {
		log.Fatal(err)
	}
	rootCmd.AddCommand(publishCmd)
}

func publishCmdParseArgs(cmd *cobra.Command, args []string) error {
	if err := cobra.ExactArgs(2)(cmd, args); err != nil {
		return err
	}
	publishCmdParam.WorkflowName = args[0]
	publishCmdParam.WorkflowStatus = args[1]
	if publishCmdParam.WorkflowName == "" {
		return errors.New("workflow name cannot be empty")
	}
	if publishCmdParam.WorkflowStatus == "" {
		return errors.New("workflow status cannot be empty")
	}
	return nil
}

// PublishEvent publish a msg to NATS
func PublishEvent(cmd *cobra.Command, args []string) error {
	logger := log.WithFields(map[string]interface{}{
		"workflow_name":   publishCmdParam.WorkflowName,
		"workflow_status": publishCmdParam.WorkflowStatus,
		"output_filename": publishCmdParam.WorkflowOutputFile,
		"provider":        publishCmdParam.Provider,
	})
	logger.Info()
	var wfOutputFile ports.JSONSrc
	if envConf.WorkflowOutput != "" {
		// env var takes priority over cmd line flag
		wfOutputFile = adapters.NewJSONSrcFromString(envConf.WorkflowOutput)
	} else if publishCmdParam.WorkflowOutputFile != "" {
		wfOutputFile = adapters.NewJSONFile(publishCmdParam.WorkflowOutputFile)
	} else {
		wfOutputFile = nil
	}

	factory := domain.WorkflowCompletionEventFactory{
		TransactionID:      publishCmdParam.TransactionID,
		WorkflowName:       publishCmdParam.WorkflowName,
		WorkflowStatus:     publishCmdParam.WorkflowStatus,
		WorkflowOutputFile: wfOutputFile,
		Provider:           publishCmdParam.Provider,
		MetadataString:     envConf.Metadata,
	}
	event, err := factory.Create()
	if err != nil {
		logger.Error(err)
		return err
	}
	err = domain.PublishEvent(adapters.NewStanEventSink(), envConf, event)
	if err != nil {
		logger.Error(err)
		return err
	}

	return nil
}
