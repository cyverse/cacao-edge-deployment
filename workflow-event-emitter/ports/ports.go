package ports

import (
	"gitlab.com/cyverse/cacao-edge-deployment/workflow-event-emitter/types"
)

// StanEventSink is the a sink to publish events into
type StanEventSink interface {
	Connect(conf types.StanConfig) error
	PublishEvent(event types.Event) error
	Close() error
}

// JSONSrc is a source for JSON object
type JSONSrc interface {
	Read() (map[string]interface{}, error)
}
